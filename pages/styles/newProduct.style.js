import css from 'styled-jsx/css';

const styles = css`
  .hidden {
    display: none;
  }
  .visible {
    width: 100%;
  }
  .header {
    @mixin flexBoxCenter {
      width: 100%;
      margin: 40px;

      h1 {
        font-size: 40px;
        color: #777;
        font-weight: 900;
      }
    }
  }

  .tabs {
    @mixin flexFromStart {
      width: 100%;
      background: #eee;
      padding: 0px 15px;

      .tab {
        border: 3px solid transparent;
        padding: 15px;
        margin-right: 10px;
        font-size: 16px;

        &.active {
          border-bottom: 3px solid #ef5600;
          color: #ef5600;
          background: #fff;
          font-weight: 700;
        }
      }
    }
  }
  .tabs-content {
    width: 100%;
    margin-top: 10px;
  }
`;

export default styles;

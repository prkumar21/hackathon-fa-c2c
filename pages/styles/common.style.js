import css from 'styled-jsx/css';

const styles = css`
  .bg_wrap {
    width: 100%;
    height: 100vh;
    background-image: linear-gradient(
        to right,
        rgba(0, 0, 0, 0.5),
        rgba(0, 0, 0, 0.3)
      ),
      url('../../static/register_bg.jpg');
    background-repeat: no-repeat;
    background-attachment: fixed;
  }
`;

export default styles;

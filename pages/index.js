import React, { useEffect } from 'react';
import mainStyle from 'static/styles/main.style';
import Router, { useRouter } from 'next/router';
import compose from 'utils/compose';
import { getApplicationContext, auth } from '@/ssrUtils';

export default function Home() {
  const router = useRouter();
  useEffect(() => {
    router.prefetch('/dashboard');
  }, []);
  return (
    <div className="container">
      <h2>Anybody Can</h2>
      <h1>Sell Anything</h1>

      <button type="button" onClick={() => Router.push('/seller-login')}>
        Become a Seller
      </button>
      <style jsx>{mainStyle}</style>
    </div>
  );
}

export const getServerSideProps = compose(auth, getApplicationContext);

import React from 'react';
import { MuiThemeProvider, createTheme } from '@material-ui/core/styles';
import RegisterContainer from 'containers/Register/Register';
import compose from 'utils/compose';
import { getApplicationContext, auth } from '@/ssrUtils';
import styles from './styles/common.style';

const theme = createTheme({
  palette: {
    primary: {
      main: '#ff6200'
    }
  }
});

const Register = (props) => {
  return (
    <MuiThemeProvider theme={theme}>
      <div className="bg_wrap">
        <RegisterContainer {...props} />
        <style jsx>{styles}</style>
      </div>
    </MuiThemeProvider>
  );
};

export const getServerSideProps = compose(auth, getApplicationContext);

export default Register;

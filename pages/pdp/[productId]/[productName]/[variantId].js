import React from 'react';
import ProductListingPage from '../../../../containers/ProductDisplayPage/ProductDisplayPage';
import compose from 'utils/compose';
import { getProduct } from '@/ssrUtils';

export default function PDP(props) {
    return <ProductListingPage {...props} />;
}

export const getServerSideProps = compose(getProduct);
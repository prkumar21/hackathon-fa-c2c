import React from 'react';
import LoginContainer from 'containers/Login/Login';
import compose from 'utils/compose';
import { getApplicationContext, auth } from '@/ssrUtils';
import styles from './styles/common.style';

const Login = (props) => {
  return (
    <div className="bg_wrap">
      <LoginContainer {...props} />
      <style jsx>{styles}</style>
    </div>
  );
};

export const getServerSideProps = compose(auth, getApplicationContext);

export default Login;

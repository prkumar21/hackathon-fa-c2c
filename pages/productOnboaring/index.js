import React from 'react';
import { MuiThemeProvider, createTheme } from '@material-ui/core/styles';
import ProductOnboarding from 'containers/ProductOnboarding/ProductOnboarding';

const theme = createTheme({
  palette: {
    primary: {
      main: '#ff6200'
    }
  }
});

const Login = (props) => {
  return (
    <MuiThemeProvider theme={theme}>
      <ProductOnboarding />
    </MuiThemeProvider>
  );
};

// export const getServerSideProps = compose(auth, getApplicationContext);

export default Login;

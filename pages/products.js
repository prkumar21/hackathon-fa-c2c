import React from 'react';
import ProductListingPage from 'containers/ProductListingPage/ProductListingPage';
import { getProductList } from '@/ssrUtils';
import compose from 'utils/compose';

export default function DashBoard(props) {
  return <ProductListingPage {...props} />;
}

export const getStaticProps = compose(getProductList);

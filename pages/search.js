import React from 'react';
import SearchLanding from 'containers/SearchContainer';
import { search } from '@/ssrUtils';
import compose from 'utils/compose';

export default function Search(props) {
  return <SearchLanding {...props} />;
}

export const getServerSideProps = compose(search);

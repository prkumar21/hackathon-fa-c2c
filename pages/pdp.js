import React from 'react';
import ProductListingPage from 'containers/ProductDisplayPage/ProductDisplayPage';
import compose from 'utils/compose';
import { getProduct } from '@/ssrUtils';

export default function PDP(props) {
  return (
    <React.Fragment>
      <img src="public/header-img.png" />
      <ProductListingPage {...props} />
    </React.Fragment>
  );
}

export const getServerSideProps = compose(getProduct);

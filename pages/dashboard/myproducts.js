import React from 'react';
import SearchLanding from 'containers/SearchContainer';
import { sellerProducts } from '@/ssrUtils';
import compose from 'utils/compose';

export default function Search(props) {
  return <SearchLanding {...props} type="seller" />;
}

export const getServerSideProps = compose(sellerProducts);

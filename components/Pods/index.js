import React, { useState, useEffect } from 'react';
import PodImage from '@/ui/Avatar';
import podStyle from './pod.style';
import ColorSwatch from 'components/ColorSwatch';
import Button from '@/ui/Button';
import Router from 'next/router';
const PodCard = ({
  badge,
  brandName,
  productName = '',
  description,
  media,
  promoted,
  variants,
  skuID,
  productId,
  price,
  isAssured,
  type
}) => {
  const [swatches, setSwatches] = useState([]);

  const fetchSwatches = (vrnts) => {
    const swatchList = [];
    vrnts.map(({ options }) => {
      options.map(({ value }) => {
        swatchList.push(`#${value}`);
      });
    });

    return swatchList;
  };

  useEffect(() => {
    const list = fetchSwatches(variants);
    setSwatches(list);
  }, [variants]);

  return (
    <div className="product-card">
      {badge && <div className="badge">{badge}</div>}
      {promoted && <div className="promoted">Ad</div>}

      <div className="product-tumb">
        <PodImage url={media} width="280" height="280" name={productName} />
      </div>

      <div className="product-details">
        <span className="product-catagory">{brandName}</span>
        <div className="product-assured">
          <h4>
            <span href="" title={productName}>
              {productName}
            </span>
          </h4>
          {isAssured && <div>F Assured</div>}
        </div>
        <ColorSwatch swatches={swatches} />
        {/* <p className="product-description">{description}</p> */}
        <div className="product-bottom-details">
          <div className="product-price">
            {/* <small>$96.00</small> */}
            <span>${price}</span>
          </div>
          <div className="product-links">
            <Button
              label={'View Details'}
              strech="full"
              onClick={() => {
                if (type) {
                  Router.push(
                    `/pdp/${productId}/${productName}/${skuID}?isSeller=true`
                  );
                } else {
                  Router.push(`/pdp/${productId}/${productName}/${skuID}`);
                }
              }}
              key={`btn-${productName}`}
            />
          </div>
        </div>
      </div>
      <style jsx>{podStyle}</style>
    </div>
  );
};

export default PodCard;

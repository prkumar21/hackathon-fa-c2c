import css from 'styled-jsx/css';

const swatchStyles = css`
  .swatch-list {
    display: flex;
    flex-wrap: wrap;

    li {
      margin-right: 2px;
      padding: 1px;
      border-radius: 50%;
      border: 1px solid transparent;
    }
  }
`;

export default swatchStyles;

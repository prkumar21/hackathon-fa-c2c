import React, { useState, useEffect } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import Router from 'next/router';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import Button from '@/ui/Button';
import ContainerWrap from '@/ui/Container';
import layOutStyle from './UploadImages.style';
import { WithLabels } from 'contexts/LabelContext';
import { WithDevice } from 'contexts/DeviceContext';
import { useRouter } from 'next/router';
import FormInput from '@/ui/FormInput';
import { WithAppCtx } from 'contexts/AppContext';
import { API_HOST } from 'utils/httpService';
import CircularProgress from '@material-ui/core/CircularProgress';

const ProductDetailForm = ({ postSKUDetails }) => {
  const [files, setfiles] = useState([]);
  const [open, setOpen] = useState(false);
  const [showSpinner, setShowSpinner] = useState(false);
  const router = useRouter();
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };
  async function uploadImage() {
    postSKUDetails(files);
  }
  const onChange = (e, index) => {
    try {
      let reader = new FileReader();
      reader.onload = async function (e) {
        setShowSpinner(true)
        const url = `${API_HOST}image/upload`;
        const response = await axios({
          method: 'post',
          url: url,
          data: {
            skuId: '123',
            imageData: reader.result
          }
        });
        const newFiles = [...files];
        newFiles.splice(index, 1, response.data);
        setfiles([...newFiles]);
        setShowSpinner(false)
        setOpen(true);
      };
      reader.readAsDataURL(e.target.files[0]);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    router.prefetch('/dashboard');
  }, []);
  return (
    <ContainerWrap direction="column">
      {showSpinner ? <div className="spinner"><CircularProgress /> </div>: null}

      <div className="add-product">
        <form>
          <FormInput
            label="Image 1"
            onChangeHandler={(event) => onChange(event, '0')}
            name="File 1"
            id="file1"
            type="file"
          />
          {files.length > 0 && (
            <div className="image-details">
              <img src={files[0].image_url} />
              <p>
                Predicted Category: <span>{files[0].predicted_category}</span>
              </p>
              <p>
                Predicted Product Condition:{' '}
                <span
                  style={{
                    color:
                      files[0].predicted_class === 'Damaged' ? 'red' : 'green'
                  }}
                >
                  {files[0].predicted_class === 'Not Damaged'
                    ? 'Good'
                    : files[0].predicted_class}
                </span>
              </p>
              <p>
                Intensity: <span>{files[0].confidence}</span>
              </p>
            </div>
          )}
          <FormInput
            label="Image 2"
            onChangeHandler={(event) => {
              onChange(event, '1');
            }}
            name="File 2"
            id="file2"
            type="file"
          />
          {files.length > 1 && (
            <div className="image-details">
              <img src={files[1].image_url} />
              <p>
                Predicted Category: <span>{files[1].predicted_category}</span>
              </p>
              <p>
                Predicted Product Condition:{' '}
                <span
                  style={{
                    color:
                      files[1].predicted_class === 'Damaged' ? 'red' : 'green'
                  }}
                >
                  {files[1].predicted_class === 'Damaged'
                    ? files[1].predicted_class
                    : 'Good'}
                </span>
              </p>
              <p>
                Intensity: <span>{files[1].confidence}</span>
              </p>
            </div>
          )}
          <FormInput
            label="Image 3"
            onChangeHandler={(event) => onChange(event, '2')}
            name="File 3"
            id="file3"
            type="file"
          />
          {files.length > 2 && (
            <div className="image-details">
              <img src={files[2].image_url} />
              <p>
                Predicted Category: <span>{files[2].predicted_category}</span>
              </p>
              <p>
                Predicted Product Condition:{' '}
                <span
                  style={{
                    color:
                      files[2].predicted_class === 'Damaged' ? 'red' : 'green'
                  }}
                >
                  {files[2].predicted_class === 'Damaged'
                    ? files[1].predicted_class
                    : 'Good'}
                </span>
              </p>
              <p>
                Intensity: <span>{files[2].confidence}</span>
              </p>
            </div>
          )}
          <FormInput
            label="Image 4"
            onChangeHandler={(event) => onChange(event, '3')}
            name="File 4"
            id="file4"
            type="file"
          />
          {files.length > 3 && (
            <div className="image-details">
              <img src={files[3].image_url} />
              <p>
                Predicted Category: <span>{files[3].predicted_category}</span>
              </p>
              <p>
                Predicted Product Condition:{' '}
                <span
                  style={{
                    color:
                      files[3].predicted_class === 'Damaged' ? 'red' : 'green'
                  }}
                >
                  {files[3].predicted_class === 'Damaged'
                    ? files[1].predicted_class
                    : 'Good'}
                </span>
              </p>
              <p>
                Intensity: <span>{files[3].confidence}</span>
              </p>
            </div>
          )}
          <FormInput
            label="Image 5"
            onChangeHandler={(event) => onChange(event, '4')}
            id="file5"
            name="File 5"
            type="file"
          />
          {files.length > 4 && (
            <div className="image-details">
              <img src={files[4].image_url} />
              <p>
                Predicted Category: <span>{files[4].predicted_category}</span>
              </p>
              <p>
                Predicted Product Condition:{' '}
                <span
                  style={{
                    color:
                      files[4].predicted_class === 'Damaged' ? 'red' : 'green'
                  }}
                >
                  {files[4].predicted_class === 'Damaged'
                    ? files[4].predicted_class
                    : 'Good'}
                </span>
              </p>
              <p>
                Intensity: <span>{files[4].confidence}</span>
              </p>
            </div>
          )}
          <div className="btn-container">
            <Button
              key="login-btn"
              id="loginButton"
              label="Submit"
              strech={'auto'}
              size="medium"
              onClick={uploadImage}
            />
          </div>
        </form>
      </div>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left'
        }}
        open={!!open}
        autoHideDuration={6000}
        onClose={handleClose}
        message={<p className="snack-bar">Image Uploaded</p>}
        action={
          <React.Fragment>
            {/* <Button color="secondary" size="small" onClick={handleClose}>
              
            </Button> */}
            <IconButton
              size="small"
              aria-label="close"
              color="inherit"
              onClick={handleClose}
            >
              {/* <CloseIcon fontSize="small" /> */}
            </IconButton>
          </React.Fragment>
        }
      />
      <style jsx>{layOutStyle}</style>
    </ContainerWrap>
  );
};

ProductDetailForm.defaultProps = {
  labels: {}
};

ProductDetailForm.propTypes = {
  labels: PropTypes.object
};

export default WithLabels(WithAppCtx(ProductDetailForm));
export { ProductDetailForm };

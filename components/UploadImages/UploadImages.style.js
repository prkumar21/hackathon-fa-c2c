import css from 'styled-jsx/css';

const layOutStyle = css`
  .spinner {
    position: absolute;
    margin-left: auto;
    margin-right: auto;
    left: 0;
    right: 0;
    text-align: center;
    top: 50%;
  }
  .add-product {
    @mixin flexFromStart {
      margin: 0 auto;
      width: 100%;
      max-width: 1280px;
      padding: 5px 40px;
      flex-direction: column;
      overflow-y: scroll;
      h1 {
        font-size: 18px;
        font-weight: 600;
        margin: 30px 0;
      }
      @mixin smallMobileOnly {
        width: 100%;
        height: 100vh;
        margin: 0 auto;
      }
    }
  }
  form {
    img {
      width: 70px;
    }
    .image-details {
      display: flex;
      align-items: center;
      p {
        margin-right: 60px;
        margin-left: 20px;
        span {
          font-size: 14px;
        }
      }
    }
    @mixin flexFromStart {
      flex-flow: column;
      gap: 1ch;
      width: 100%;

      .btn-container {
        width: 100%;
        margin: 20px auto;
      }
    }
  }

  label {
    font-weight: 600;
  }
  .error {
    margin: 0.5rem 0 0;
    color: brown;
  }
  .snack-bar {
    font-size: 14px;
  }
`;

export default layOutStyle;

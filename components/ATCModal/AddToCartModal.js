import React, { useState } from 'react';
import Dialog from '@mui/material/Dialog';
import styles from './AddToCartModal.style';

function AddToCartModal({ handleClickOpen, handleClose, open }) {
  return (
    <div className="cartModal">
      <Dialog open={open} onClose={handleClose} maxWidth={false}>
        <div className="cartItems">
          <div className="cartItems-heading-container">
              <h2 className="cartItems-heading">Lo que llevas en tu Carro</h2>
          </div>
        </div>
      </Dialog>
      <style jsx>{styles}</style>
    </div>
  );
}

export default AddToCartModal;

import css from 'styled-jsx/css';

const styles = css`
    .cartModal {
        width: 720px;
        height: 300px;
    }
    .cartItems {
        display: flex;
        width: 720px;
        height: 300px;
        padding: 0 27px;
    }

    .cartItems-heading-container {
        width: 100%;
        display: flex;
        height: 50px;
        display: flex;
        align-items: center;
        border-bottom: 1px solid rgb(240, 240, 240);

        .cartItems-heading {
            color: #333;
            font-size: 1.8rem;
            font-weight: 400;
            letter-spacing: -0.07px;
            line-height: 34px;
        }
    }
`;

export default styles;
import css from 'styled-jsx/css';

const styles = css`
    .carousel-container {
        width: 100%;
        height: 500px;
    }

    .carousel-body {
        width: 100%;
        height: 100%;
        display: flex;
    }

    .button-container {
        width: 50px;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .button-left {
        background: #fff;
        width: 100%;
        height: 40px;
    }
    .button-right {
        background: #fff;
        width: 100%;
        height: 40px;
    }
`;

export default styles;
import React, { useState, useEffect } from 'react'
import styles from './Carousel.style';
import getVariant from '../../utils/variant';
import { guessFirstImage } from './CarouselHelper';
import _ from 'lodash';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

function Carousel({
    product
}) {
    //const { variants } = product;
    const variantId = product.sku_id;
    //const currentVariant = getVariant(variants, product.currentVariant);
    const sameImage = false;
    const firstImageGuess = guessFirstImage(product, variantId, sameImage);
    
    const [images, setImages] = useState(firstImageGuess);
    const [activeImageIndex, setActiveImageIndex] = useState(0);

    useEffect(() => {
        // const variantObj = product.variants.find(
        //   (variant) => variant.id === currentVariant.id
        // );
        if (!sameImage && _.get(product, 'medialist', []).length > 0) {
          setImages([...product.medialist]);
          return;
        }
        // if (sameImage && _.get(product, 'medias', []).length > 0) {
        //   setImages(product.medias);
        //   return;
        // }
    }, [])

    useEffect(() => {
        
    }, [activeImageIndex])

    const goLeft = (e) => {
        e.preventDefault();
        let idx = activeImageIndex - 1;
        if (idx < 0) {
            idx = 0;
        }
        setActiveImageIndex(idx);
    }

    const goRight = (e) => {
        e.preventDefault();
        let idx = activeImageIndex + 1;
        if (idx >= images.length) {
            idx = images.length - 1;
        }
        setActiveImageIndex(idx);
    }

    return (
        <div className="carousel-container">
            <div className="carousel-body">
                <div className="button-container">
                    <button className="button-left"
                        onClick={goLeft}
                    >
                        <ArrowBackIosIcon/>
                    </button>
                </div>
                <div className="image-container">
                    <img 
                        src={images[activeImageIndex]}
                        width={'100%'}
                        height={'100%'}
                    />
                </div>
                <div className="button-container">
                    <button className="button-right"
                        onClick={goRight}
                    >
                        <ArrowForwardIosIcon/>
                    </button>
                </div>
            </div>
            <style jsx>{styles}</style>
        </div>
    )
}

export default Carousel;

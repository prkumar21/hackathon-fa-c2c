import _ from 'lodash';

const guessFirstImage = (
  product,
  variantId,
  sameImage,
  imageGalleryType = 'hardline'
) => {
    // const currentVariant = product.variants.find(
    //   (v) => v.id === variantId
    // );
    
    let mediaList;  

    if (!sameImage && _.get(product, 'medialist.length', 0) > 0) {
       mediaList = product.medialist;
    }
    // if (sameImage && _.get(product, 'medias.length', 0) > 0) {
    //   mediaList = product.medias;
    // }

    const firstImageFallback = `https://falabella.scene7.com/is/image/Falabella/1569_1?wid=480&hei=480&qlt=70`;
    //const secondImageFallback = `Falabella/${variantId}_2`;

     const firstImage = _.get(mediaList, '[0]', firstImageFallback);

    // if (imageGalleryType === 'softline') {
    //   const secondImage = _.get(mediaList, '[1]', secondImageFallback);
    //   return [firstImage, secondImage];
    // }
    return [firstImage];
};

export {
    guessFirstImage
}

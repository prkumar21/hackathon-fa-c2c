import css from 'styled-jsx/css';
import { themeColors } from 'dev-configs/style-config/colors';

const buttonStyles = css`
  .button {
    @mixin flexBoxCenter {
      padding: 12px;
      background: #ff6200;
      color: #fff;
      border-radius: 40px;
      font-weight: 400;
      cursor: pointer;
      transition: 0.3s;
      padding: 10px 30px;

      &.small {
        font-size: 13px;
      }

      &.medium {
        font-size: 18px;
      }

      &.strech-full {
        width: 100%;
      }

      &.strech-half {
        width: 50%;
      }

      &.strech-auto {
        width: auto;
      }

      &:hover {
        opacity: 0.8;
      }
      &.search-btn {
        height: 40px;
        min-height: 35px;
        background-color: #495867;
        min-width: 35px;
        padding: 0.45rem 30px;
        margin: 0px 10px;
        text-align: center;
        border: 1px solid #aab7b8;
        border-radius: 0;
        margin-top: 3px;
        border-radius: 5px;
        color: #fff;
        font-size: 15px;
      }
    }
  }
`;

export default buttonStyles;

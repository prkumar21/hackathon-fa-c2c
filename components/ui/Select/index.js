import React from 'react';
import inputStyles from './input.style';

const Select = ({ onChangeHandler, value, name, id, label, disabled, options = [] }) => {
  return (
    <React.Fragment>
      <label htmlFor={name}>{label}</label>
      <select
        className="form-input"
        id={id}
        disabled={disabled}
        name={name}
        value={value}
        onChange={onChangeHandler}
      >
        {
          options.map(op => (
            <option value={op}>{op}</option>
          ))
        }
      </select>
      <style jsx>{inputStyles}</style>
    </React.Fragment>
  );
};

export default Select;

import React from 'react';
// import Image from 'next/image';
import csStyle from './image.module.scss';

const PodImage = ({ url, name, width, height }) => {
  return (
    <div>
      <img
        className={csStyle.podImage}
        src={url || "https://falabella.scene7.com/is/image/Falabella/1569_1?wid=480&hei=480&qlt=70"}
        width={width}
        height={height}
        alt={name}
      />
    </div>
  );
};

export default PodImage;

import React from 'react';
import PropTypes from 'prop-types';
import { StepperStyles } from './Stepper.style';
import noop from '../../../utils/noop';

const Stepper = ({
  count,
  onIncrement,
  onDecrement,
  minimum,
  maximum,
  type
}) => {
  return (
    <div className={`product-count ${type}`}>
      <button
        type="button"
        className={`decrement btn-${type}`}
        onClick={onDecrement}
        disabled={count <= minimum}
      >
        <span>&#8722;</span>
      </button>
      <div className={`product-count-value value-${type}`}>{count}</div>
      <button
        type="button"
        className={`increment btn-${type}`}
        onClick={onIncrement}
        disabled={count >= maximum}
      >
        <span>&#43;</span>
      </button>
      <style jsx>{StepperStyles}</style>
    </div>
  );
};

Stepper.defaultProps = {
  count: 0,
  onIncrement: noop,
  onDecrement: noop,
  minimum: 1,
  maximum: 10,
  type: 'primary'
};

Stepper.propTypes = {
  count: PropTypes.number,
  onIncrement: PropTypes.func,
  onDecrement: PropTypes.func,
  minimum: PropTypes.number,
  maximum: PropTypes.number,
  type: PropTypes.oneOf(['primary', 'secondary'])
};

export default Stepper;

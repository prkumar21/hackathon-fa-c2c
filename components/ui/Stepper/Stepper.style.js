import css from 'styled-jsx/css';

const StepperStyles = css`
  .product-count {
    display: flex;
    position: relative;

    &.primary {
      width: 88px;
      height: 30px;
    }

    &.secondary {
      width: 250px;
      height: 48px;
      border: 2px solid #f0f0f0;
      border-radius: 4px;
    }

    &-value {
      margin: auto;
      width: 100%;
      display: flex;
      justify-content: center;
      padding-bottom: 7px;

      &.value-primary {
        border-bottom: 1.3px solid #bbb;
        color: #bbb;
        font-size: 1.4rem;
        line-height: 17px;
      }
      &.value-secondary {
        font-family: Lato, sans-serif;
        font-style: normal;
        font-weight: bold;
        font-size: 16px;
        line-height: 19px;
        text-align: center;
        color: #333;
        padding-bottom: 0;
      }
    }

    .increment,
    .decrement {
      position: absolute;

      &.btn-primary {
        font-size: 1.6rem;
        bottom: 10px;
        color: #333;
      }
      &.btn-secondary {
        width: 46px;
        height: 44px;
        background: #f0f0f0;
        border-radius: 2px 0 0 2px;
        color: #495867;
        text-align: center;
        font-family: Lato, sans-serif;
        font-style: normal;
        font-weight: bold;
      }
    }

    .increment {
      &.btn-primary {
        right: 4px;
      }
      &.btn-secondary {
        right: 0;
        font-size: 22px;
      }
    }
    .decrement {
      &.btn-primary {
        left: 4px;
      }
      &.btn-secondary {
        left: 0;
        font-size: 22px;
      }
    }

    .increment:disabled,
    .decrement:disabled {
      color: #bbb;
    }
  }
`;

export { StepperStyles };

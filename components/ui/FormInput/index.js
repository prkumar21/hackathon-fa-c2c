import React from 'react';
import inputStyles from './input.style';

const FormInput = ({
  onChangeHandler,
  value,
  name,
  id,
  type,
  label,
  placeholder,
  disabled = false,
  checked = '',
  style = {},
  onClick
}) => {
  return (
    <React.Fragment>
      <label htmlFor={name}>{label}</label>
      <input
        required
        className="form-input"
        type={type}
        style={style}
        placeholder={placeholder}
        onClick={onClick}
        id={id}
        checked={checked}
        name={name}
        disabled={disabled}
        value={value}
        onChange={onChangeHandler}
      />
      <style jsx>{inputStyles}</style>
    </React.Fragment>
  );
};

export default FormInput;

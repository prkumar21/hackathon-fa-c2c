import css from 'styled-jsx/css';

const layOutStyle = css`
  .login {
    @mixin flexFromStart {
      background: #fff;
      position: fixed;
      margin: 30vh auto;
      width: 440px;
      padding: 20px 40px;
      border: 1px solid #ccc;
      border-radius: 8px;
      box-shadow: 0 2px 7px #dfdfdf;
      flex-direction: column;

      h1 {
        font-size: 18px;
        font-weight: 600;
        margin: 30px 0;
      }
      @mixin smallMobileOnly {
        width: 100%;
        height: 100vh;
        margin: 0 auto;
      }
    }
  }
  form {
    @mixin flexFromStart {
      flex-flow: column;
      gap: 1ch;
      width: 100%;

      .btn-container {
        width: 100%;
        margin: 20px auto;
      }
    }
  }
  .registration-separation {
    text-align: center;
    position: relative;
    top: 2px;
    padding-top: 1px;
    margin-bottom: 14px;
    line-height: 0;
    width: 100%;
    p {
      line-height: 1;
      font-size: 12px;
      color: #767676;
      font-weight: 400;
      z-index: 2;
      position: relative;
      display: inline-block;
      background-color: #fff;
      padding: 0 8px 0 7px;
    }
    &::after {
      content: '';
      width: 100%;
      background-color: transparent;
      display: block;
      height: 1px;
      border-top: 1px solid #e7e7e7;
      position: absolute;
      top: 50%;
      margin-top: -1px;
      z-index: 1;
    }
  }
  .registration-go-to {
    background: #495867;
    border-radius: 10px;
    width: 100%;
    font-size: 15px;
    padding: 0% 4%;
    color: #fff;
    height: 35px;
  }
  label {
    font-weight: 600;
  }
  .error {
    margin: 0.5rem 0 0;
    color: brown;
  }
`;

export default layOutStyle;

import React, { useState, useEffect } from 'react';
import Router from 'next/router';
import axios from 'axios';
import styles from './ProductDisplayPage.style';
import _, { get } from 'lodash';
import { WithAppCtx } from 'contexts/AppContext';
import Carousel from '../../components/Carousel/Carousel';
import getVariant from 'utils/variant';
import { httpFetchService, API_HOST } from 'utils/httpService';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';

const ProductDisplayPage = ({ appCtx, product = {}, customerId }) => {
  const [bidValue, setBidValue] = useState('');
  const [bids, setBids] = useState([]);
  const [offerWarning, setOfferWarning] = useState(false);
  const [bidWarning, setBidWarning] = useState(false);
  const [isSeller, setIsSeller] = useState(false);

  const {
    bid,
    name,
    price,
    sku_id,
    is_used,
    quantity,
    condition,
    seller_id,
    brand_name,
    categories,
    product_id,
    description,
    seller_rating
  } = product;
  const { basePrice = '', actualPrice } = price || {};

  useEffect(() => {
    if (Router.query.isSeller && Router.query.isSeller === 'true') {
      setIsSeller(true);
    }
  }, []);

  const handleBidValueChange = (evt) => {
    const re = /^[0-9\b]+$/;
    if (evt.target.value === '' || re.test(evt.target.value)) {
      setBidValue(evt.target.value);
    }
  };

  const getBids = async (newBid = {}) => {
    const response = await axios.get(
      `${API_HOST}bid/getTopBids/${sku_id}?limit=${
        Router.query.isSeller ? '10' : '3'
      }`
    );
    const data =
      response.statusText === 'OK' ? get(response, 'data.bids', []) : '';
    setBids(data);
  };

  useEffect(() => {
    if (bid.enabled) {
      getBids();
    }
  }, []);

  const handleBidSubmit = async () => {
    if (!bidValue) {
      return;
    }
    const bidDifference = Number(bidValue) - basePrice;
    if (bidDifference >= bid.range) {
      const response = await axios({
        url: `${API_HOST}bid`,
        method: 'post',
        data: {
          cust_id: customerId,
          sku_id: sku_id,
          bid_price: bidValue,
          currency: '$',
          isaccepted: true
        }
      });
      if (response.statusText === 'OK') {
        console.log('Bid Placed');
        await getBids();
        setBidWarning(false);
      }
    } else {
      setBidWarning(true);
    }
  };

  const handleOfferSubmit = async () => {
    if (!bidValue) {
      return;
    }
    debugger;
    if (Number(bidValue) < basePrice) {
      const response = await axios.post(`${API_HOST}bid`, {
        cust_id: customerId,
        sku_id: sku_id,
        bid_price: bidValue,
        currency: '$',
        isaccepted: true
      });
      const data = response.success ? get(response, 'data.data', {}) : '';
      if (data && data.success === 'OK') {
        console.log('Offer Placed');
        setOfferWarning(false);
        await getBids();
      }
      //setOfferWarning(false);
    } else {
      setOfferWarning(true);
    }
  };

  const handleAddToCart = () => {};

  const getMinBidToEnter = () => {
    if (bids.length > 0) {
      Number(bids[0].bid_price);
    }
    return Number(basePrice) + bid.range;
  };

  const bidSection = () => {
    return (
      <React.Fragment>
        <div className="product-specifications-column">
          <div className="make-a-bid-section">
            <div className="bid-price-section">
              <div className="bid-price-wrapper">
                <span>{bids.length > 0 ? 'Current Bid' : 'Price'} : </span>
                <span className="price-value">
                  ${bids.length > 0 ? bids[0].bid_price : basePrice}
                </span>
              </div>
              <div className="number-of-bids">
                <h2>
                  [{' '}
                  {bids.length <= 1
                    ? bids.length.toString() + ' bid'
                    : bids.length.toString() + ' bids'}{' '}
                  ]{' '}
                </h2>
              </div>
            </div>
            <div className="bid-wrapper">
              <div className="input-wrapper">
                <input
                  className="input-box"
                  placeholder="bid price"
                  type="tel"
                  value={bidValue}
                  id="testId-input-calculator-input"
                  min="0"
                  maxLength="10"
                  onChange={(evt) => handleBidValueChange(evt)}
                />
              </div>
              <div className="bid-button-wrapper">
                {bidWarning && (
                  <div className="warning">
                    <span className="warning-icon-alert">
                      <i className="csicon-warning-alert"></i>
                    </span>
                    <span className="warning-label">
                      Enter a valid bid price
                    </span>
                  </div>
                )}
                <button
                  className="make-a-bid"
                  onClick={(e) => handleBidSubmit()}
                >
                  {'Place your bid'}
                </button>
              </div>
            </div>
            <p className="min-bid-heading">
              {' '}
              Enter <span className="strong">${getMinBidToEnter()}</span> or
              more
            </p>
          </div>
        </div>
        <style jsx>{styles}</style>
      </React.Fragment>
    );
  };

  const offerSection = () => {
    return (
      <React.Fragment>
        <div className="product-specifications-column">
          <div className="atc-section">
            <div className="pdp-price-section">
              <span className="strong">Price : </span>
              <span className="price-value strong">${basePrice}</span>
            </div>
            <div className="atc-button">
              <button
                className="add-to-cart-button"
                onClick={(e) => handleAddToCart()}
              >
                {'Add to cart'}
              </button>
            </div>
          </div>

          <div className="make-an-offer-section">
            <div className="offer-description"></div>
            <div className="offer-wrapper">
              <div className="input-wrapper">
                <span className="offer-input-heading">$</span>
                <input
                  className="input-box"
                  placeholder="enter your offer"
                  type="tel"
                  value={bidValue}
                  id="testId-input-calculator-input"
                  min="0"
                  maxLength="10"
                  onChange={(evt) => handleBidValueChange(evt)}
                />
              </div>
              <div className="offer-button-wrapper">
                {offerWarning && (
                  <div className="warning">
                    <span className="warning-icon-alert">
                      <i className="csicon-warning-alert"></i>
                    </span>
                    <span className="warning-label">
                      Enter a valid offer price
                    </span>
                  </div>
                )}
                <button
                  className="make-an-offer"
                  onClick={(e) => handleOfferSubmit()}
                >
                  {'Make an offer'}
                </button>
              </div>
            </div>
          </div>
        </div>
        <style jsx>{styles}</style>
      </React.Fragment>
    );
  };

  const addToCartSection = () => {
    return (
      <React.Fragment>
        <div className="product-specifications-column">
          <div className="atc-section">
            <div className="pdp-price-section">
              <span className="strong">Price : </span>
              <span className="price-value strong">${basePrice}</span>
            </div>
            <div className="atc-button">
              <button
                className="add-to-cart-button"
                onClick={(e) => handleAddToCart()}
              >
                {'Add to cart'}
              </button>
            </div>
          </div>
        </div>
        <style jsx>{styles}</style>
      </React.Fragment>
    );
  };

  const sellerBidsSection = () => {
    return (
      <React.Fragment>
        <div className="seller-bids-container">
          <div className="seller-top-bid">
            <div className="seller-bid-price-section">
              {bids.length > 0 && (
                <div>
                  <span className="strong">
                    {bid.type === 'bargain' ? 'Top Offer' : 'Top Bid'} :{' '}
                  </span>
                  <span className="price-value strong">
                    ${bids[0].bid_price}
                  </span>
                  <div className="atc-button">
                    <button
                      className="sell-now-button"
                      onClick={(e) => handleAddToCart()}
                    >
                      {'Confirm Order For Top Bid'}
                    </button>
                  </div>
                </div>
              )}
            </div>
          </div>
          {bids.length ? (
            <div className="seller-product-characteristics">
              <div className="characteristics-heading">
                <h3>{bid.type === 'bargain' ? 'Offers' : 'Bids'}</h3>
              </div>
              <div className="characteristic-list">
                <ul>
                  {bids.map((bid) => {
                    return (
                      <li className="bid-list" key={bid.id}>
                        <span className="Strong">${bid.bid_price}</span>
                        <button
                          className="accept-a-bid"
                          onClick={(e) => handleBidSubmit()}
                        >
                          {'Accept'}
                        </button>
                      </li>
                    );
                  })}
                </ul>
              </div>
            </div>
          ) : null}
        </div>
        <style jsx>{styles}</style>
      </React.Fragment>
    );
  };

  const bidOrOfferSection = () => {
    if (isSeller) {
      return sellerBidsSection();
    }
    return bid.type === 'bargain' ? offerSection() : bidSection();
  };

  const getTime = () => {
    dayjs.extend(relativeTime);
    const time = dayjs(bid.endDate);
    return `${time.fromNow(true)} | ${time.toString()}`;
  };

  return (
    <React.Fragment>
      <img className="header" src="../../../static/header-img.png" />
      <div className="pdp-body">
        <div className="container">
          <div className="pdp-container">
            <div className="pdp-image-section">
              <Carousel product={product} />
            </div>
            <div className="pdp-detail-section">
              <div className="pdp-basic-details">
                <div className="product-heading">
                  <h2>{name}</h2>
                </div>
                <div className="product-details">
                  <div className="product-condition">
                    {!isSeller && (
                      <h2>
                        <span className="strong">Condition :</span> {condition}
                      </h2>
                    )}
                    {bid.enabled && bid.type !== 'bargain' && (
                      <span className="time-left">
                        <span className="time-left-label strong">
                          Time Left:
                        </span>{' '}
                        {getTime()}
                      </span>
                    )}
                  </div>
                  {isSeller && (
                    <div className="seller-price">
                      <h3>Base Price : ${basePrice}</h3>
                    </div>
                  )}
                  {!isSeller &&
                  bid.enabled &&
                  bid.type !== 'bargain' &&
                  bids.length ? (
                    <div className="product-characteristics">
                      <div className="characteristics-heading">
                        <h3>{'Top 3 bids'}</h3>
                      </div>
                      <div className="characteristic-list">
                        <ul>
                          {bids.map((bid) => {
                            return (
                              <li key={bid.id}>
                                {bid.currency}
                                <span className="Strong">{bid.bid_price}</span>
                              </li>
                            );
                          })}
                        </ul>
                      </div>
                    </div>
                  ) : null}
                </div>
              </div>
              <div className="pdp-product-specifications">
                {!isSeller && !bid.enabled
                  ? addToCartSection()
                  : bidOrOfferSection()}
              </div>
            </div>
          </div>
        </div>
      </div>
      <style jsx>{styles}</style>
    </React.Fragment>
  );
};

export default WithAppCtx(ProductDisplayPage);
export { ProductDisplayPage };

import css from 'styled-jsx/css';

const svgToDataUri = (svgStr) => {
    const encoded = encodeURIComponent(svgStr)
      .replace(/'/g, '%27')
      .replace(/"/g, '%22');
  
    const header = 'data:image/svg+xml;charset=utf8,';
    const dataUrl = header + encoded;
  
    return dataUrl;
};

const floorCalcWarningIcon = `<svg width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" clip-rule="evenodd" d="M0.25 9.71638C0.25 4.57997 4.4125 0.377441 9.5 0.377441C14.5875 0.377441 18.75 4.57997 18.75 9.71638C18.75 14.8528 14.5875 19.0553 9.5 19.0553C4.4125 19.0553 0.25 14.8528 0.25 9.71638ZM17.5167 9.71641C17.5167 5.20259 13.9709 1.62266 9.50006 1.62266C5.02923 1.62266 1.4834 5.20259 1.4834 9.71641C1.4834 14.2302 5.02923 17.8102 9.50006 17.8102C13.9709 17.8102 17.5167 14.2302 17.5167 9.71641ZM8.88281 4.26867H10.1161V12.0511H8.88281V4.26867ZM9.5002 15.3197C10.0111 15.3197 10.4252 14.9016 10.4252 14.3858C10.4252 13.87 10.0111 13.4519 9.5002 13.4519C8.98933 13.4519 8.5752 13.87 8.5752 14.3858C8.5752 14.9016 8.98933 15.3197 9.5002 15.3197Z" fill="#F7B500"/>
  </svg>`
const styles = css`
    .header {
        height: 70px;
    }
    .pdp-body {
        background-color: #eee;
        padding: 20px;
    }
    .container {
        margin: 0 auto;
        max-width: 1280px;
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
    }
    .pdp-container {
        width: 100%;
        display: flex;
        justify-content: flex-start;
        padding-bottom: 20px;
        padding-top: 20px;
        background-color: #fff;
    }
    .pdp-image-section {
        width: 40%;
        height: 100%;
    }

    .pdp-detail-section {
        width: 60%;
        display: flex;
        padding: 20px;
        flex-direction: column;
        margin-left: 30px;
    }

    .pdp-basic-details {
        width: 100%;
        border-bottom: 1px solid rgb(240, 240, 240);
        padding-bottom: 10px;
        display: flex;
        flex-direction: column;
        
        .product-heading {
            color: #333;
            font-size: 28px;
            font-weight: 300;
            letter-spacing: -0.07px;
            line-height: 34px;
            margin-right: 15px;
            padding-bottom: 10px;
        }

        .product-details {
            display: flex;
            width: 100%;
        }

        .seller-price {
            width: 50%;
            font-size: 25px;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .product-condition {
            width: 50%;
            display: flex;
            flex-direction: column;
            color: #333;
            height: 100px;
            font-size: 18px;
            line-height: 20px;
            letter-spacing: -0.07px;
            border-top: 1px solid rgb(240, 240, 240);
            justify-content: center;
        }
    }


    .pdp-product-specifications {
        display: flex;
        height: 100%;
        flex-direction: column;

            .product-specifications-column {
                display: flex;
                width: 100%;
                height: 100%;
                flex-direction: column;

                .atc-section {
                    display: flex;
                    width: 100%;
                    border-bottom: 1px solid #f0f0f0;
                }
                .pdp-price-section {
                    width: 50%;
                    margin-top: 30px;
                    color: #333;
                    font-size: 22px;
                    font-weight: 300;
                    letter-spacing: -0.07px;
                    line-height: 34px;
                
                    .price-value {
                        margin-left: 5px;
                    }
                }

                .bid-price-section {
                    width: 100%;
                    display: flex;
                    
                    .bid-price-wrapper {
                        width: 50%;
                        color: #333;
                        font-size: 18px;
                        font-weight: 300;
                        letter-spacing: -0.07px;
                        line-height: 34px;
                        margin-bottom: 30px;
                        font-weight: 400;
                        .price-value {
                            margin-left: 5px;
                            font-size: 25px;
                        }
                    }  
                }

                .make-an-offer-section {
                    width: 100%;
                    margin-top: 20px;
                    display: flex;
                }

                .make-a-bid-section {
                    width: 100%;
                    margin-top: 20px;
                }

                .offer-description {
                    width: 50%;
                    display: flex;
                }

                .offer-wrapper {
                    width: 50%;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    flex-direction: column;
                }

                .bid-wrapper {
                    display: flex;
                    justify-content: center;
                    align-items: center;
                }

                .min-bid-heading {
                    margin-top: 10px;
                    margin-left: 5px;
                    color: #333;
                    font-size: 15px;
                    letter-spacing: 2px;
                }

                .offer-input-heading {
                    font-size: 30px;
                    margin-right: 5px;
                }

                .input-wrapper {
                    width: 100%;
                    padding-top: 5px;
                }

                .button-wrapper {
                    width: 100%;
                }

                .offer-button-wrapper {
                    width: 100%;
                    margin-left: 40px;
                }

                .bid-button-wrapper {
                    width: 100%;
                    display: flex;
                    flex-direction: column;
                    justify-content: flex-start;
                }
    
                .input-box {
                    outline: none;
                    width: 200px;
                    height: 100%;
                    padding: 0 3px;
                    color: #4a4a4a;
                    text-align: center;
                    height: 48px;
                    margin-right: 10px;
                    border-radius: 8px;
                    border: 1px solid #333;
                }

        }

        .pdp-add-to-cart-section {
            width: 100%;
            margin-top: 30px;
            display: flex;
            flex-direction: column;

            .bid-price {
                margin-bottom: 20px;
            }
        }
        .add-to-cart-button {
            height: 56px;
            width: 295px;
            font-size: 1.9rem;
            font-weight: bold;
            line-height: 23px;
            letter-spacing: 0.5px;
            color: #fff;
            border-radius: 30px;
            background-color: #FF6200;
            margin-top: 20px;
            text-transform: none;
            margin-bottom: 40px;
        }

        .sell-now-button {
            height: 56px;
            width: 145px;
            font-size: 1.6rem;
            font-weight: bold;
            line-height: 23px;
            letter-spacing: 0.5px;
            color: #fff;
            border-radius: 30px;
            background-color: #FF6200;
            margin-top: 20px;
            text-transform: none;
            margin-bottom: 40px;
        }

        .make-an-offer {
            margin-top: 5px;
            padding: 12px;
            width: 200px;
            background: #495867;
            color: #fff;
            font-size: 21px;
            border-radius: 40px;
            font-weight: 400;
            cursor: pointer;
            transition: 0.3s;
            margin-top: 10px;
            &:hover {
              opacity: 0.8;
            }
        }

        .make-a-bid {
            margin-top: 5px;
            padding: 12px;
            width: 295px;
            background: #495867;
            color: #fff;
            font-size: 21px;
            border-radius: 40px;
            font-weight: 400;
            cursor: pointer;
            transition: 0.3s;

            &:hover {
              opacity: 0.8;
            }
        }

        .accept-a-bid {
            margin-top: 5px;
            padding: 10px;
            width: 95px;
            background: #495867;
            color: #fff;
            margin-left: 30px;
            font-size: 16px;
            border-radius: 40px;
            font-weight: 400;
            cursor: pointer;
            transition: 0.3s;

            &:hover {
              opacity: 0.8;
            }
        }
    }

    .product-characteristics {
        background: #fafafa;
        padding: 18px 27px;
        display: flex;
        width: 50%;
        flex-direction: column;
        margin-top: 30px;
        margin-left: 20px;
        max-width: 305px;

        .characteristics-heading {
            font-size: 18px;
            margin-bottom: 10px;
        }

        .characteristic-list ul {
            list-style: none;
            font-size: 15px;
        }

        .characteristic-list li span.strong{
            font-weight: bold;
        }
        .characteristic-list li::before {
            content: '◇';
            left: 0;
            color: #333;
            font-size: 1.1rem;
            margin-right: 5px;
            line-height: 20px;
        }
    }

    .seller-bids-container {
        display: flex;

        .seller-bid-price-section {
            margin-top: 30px;
            font-size: 25px;
        }
    }

    .seller-top-bid {
        width: 50%;
        display: flex;
    }

    .seller-product-characteristics {
        position: relative;
        background: #fafafa;
        padding: 18px 47px;
        display: flex;
        width: 50%;
        height: 334px;
        flex-direction: column;
        margin-top: 30px;
        border-radius: 10px;

        .characteristics-heading {
            font-size: 25px;
            margin-bottom: 10px;
            color: #495867;
            display: flex;
            justify-content: center;
        }

        .characteristic-list {
            overflow-y: scroll;
        }

        .characteristic-list ul {
            list-style: none;
            font-size: 15px;
        }

        .bid-list {
            background-color: white;
            height: 70px;
            margin-top: 10px;
            border-radius: 10px;
            display: flex;
            align-items: center;
            width: 100%;
            justify-content: center;
        }

        .bid-list span {
            font-size: 20px;
        }
        .characteristic-list li span.strong{
            font-weight: bold;
        }
        .characteristic-list li::before {
            content: '◇';
            margin-left: 20px;
            color: #333;
            font-size: 1.1rem;
            margin-right: 5px;
            line-height: 20px;
        }
    }

    .warning {
        display: flex;
        background-color : #fff9e9;
        padding: 5px;
        width: 295px;
    }
    .warning-label {
        margin-left: 5px;
        font-size: 15px;
        margin-top: 2px;
    }
    .csicon-warning-alert:before {
        content: url('${svgToDataUri(floorCalcWarningIcon)}');
      }

      .strong {
        font-weight: bold;
        } 

        .time-left-label {
            font-size: 17px;
        }
      .time-left {
          margin-top: 20px;
          font-size: 15px;
          letter-spacing: -0.07px;
      }

`;

export default styles;
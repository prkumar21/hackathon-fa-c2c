import css from 'styled-jsx/css';

const styles = css`
  .listing-wrapper {
    height: 100%;
    width: 100%;
    .top-section {
      padding: 5rem;
      max-width: 1280px;
      margin: auto;
    }
    .listing-divider {
      border: 1px solid #dadada;
    }
    p.no-result {
        text-align: center;
        font-size: 16px;
    }
    .product-list {
      max-width: 1280px;
      margin: auto;
      .product-slab {
        display: flex;
        align-items: start;
        height: 156px;
        padding: 20px;
        border: 1px solid #dadada;
        border-top: none;
        justify-content: space-between;
      }
      p, button {
          font-size: 14px;
          color: #333;
          margin-top: 5px;
      }
    }
  }
`;

export default styles;

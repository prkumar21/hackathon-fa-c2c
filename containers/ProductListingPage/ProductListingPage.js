import React, { useState } from 'react';
import Router from 'next/router';
import { WithLabels } from 'contexts/LabelContext';
import LayoutContainer from 'containers/LayoutContainer/LayoutContainer';
import styles from './ProductListingPage.style';
import _ from 'lodash';
import { WithAppCtx } from 'contexts/AppContext';

const VariantsSection = ({ product, setAppCtx, appCtx }) => {
  const [showVariations, setShowVariations] = useState(false);
  const imageUrl = _.get(product, 'mediaUrls[0]', '');
  return (
    <div className="product-slab">
      <img width="116" height="116" src={imageUrl || "https://via.placeholder.com/116x116"} alt="product image" />
      <button>{product.name}</button>
      <p>{product.brand}</p>
      <button
        onClick={() => {
          setAppCtx({
            ...appCtx,
            productDetails: {
              product_id: product.product_id,
              brand_name: product.brand_name,
              description: product.description
            }
          })
          Router.push(`/productOnboaring/${product.product_id}`);
        }}
        style={{ textDecoration: 'underline' }}
      >
        Add this product
      </button>
      <style jsx>{styles}</style>
    </div>
  );
};
const ListingPage = (props) => {
  const results = _.get(props, 'appCtx.productList', []);
  return (
    <div className="listing-wrapper">
      <section className="top-section">
        <LayoutContainer {...props} isProductsPage />
      </section>
      <div className="listing-divider" />
      <section className="product-list">
        {results.length ? (
          <React.Fragment>
            {results.map((product) => {
              return <VariantsSection {...props} key={product.id} product={product} />;
            })}
          </React.Fragment>
        ) : (
          <p className="no-result">No results found</p>
        )}
      </section>
      <style jsx>{styles}</style>
    </div>
  );
};

export default WithLabels(WithAppCtx(ListingPage));
export { ListingPage };

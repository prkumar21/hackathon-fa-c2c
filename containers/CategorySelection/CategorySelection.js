import React, { useState, useEffect } from 'react';
import taxonomy from 'data/taxonomy';
import styles from './CategorySelection.style';

const CategorySelection = ({ categoryHandler }) => {
  const [parentCategory, setParentcategory] = useState('');
  const [subCategory, setSubcategory] = useState('');
  const [subCategoryList, setSubcategoryList] = useState([]);

  const onCategorySelect = (cat) => {
    setSubcategory('');
    setParentcategory(cat.link);
    setSubcategoryList(cat.subCategories);
  };

  const onSubCategorySelect = (cat) => {
    const subcategoryID = cat.link.split('/category/')[1].split('/')[0];
    setSubcategory(subcategoryID);
  };

  return (
    <div className="categoryWrapper">
      <div className="parent-category">
        <ul>
          {taxonomy.map((item) => (
            <li
              className={`${item.link === parentCategory ? 'active' : ''}`}
              onClick={() => onCategorySelect(item)}
            >
              {item.label}
            </li>
          ))}
        </ul>
      </div>
      <div className="sub-category">
        <ul>
          {subCategoryList.map(
            (item) =>
              item.link.includes('/category/') && (
                <li
                  className={`${
                    subCategory && item.link.includes(subCategory)
                      ? 'active'
                      : ''
                  }`}
                  onClick={() => onSubCategorySelect(item)}
                >
                  {item.label}

                  {subCategory && item.link.includes(subCategory) ? (
                    <button
                      className="ctg-btn"
                      onClick={() =>
                        categoryHandler(parentCategory, subCategory)
                      }
                    >
                      Select
                    </button>
                  ) : (
                    ''
                  )}
                </li>
              )
          )}
        </ul>
      </div>
      <style jsx>{styles}</style>
    </div>
  );
};

export default CategorySelection;

import css from 'styled-jsx/css';

const styles = css`
  .categoryWrapper {
    @mixin flexFromStart {
      li {
        @mixin flexBoxCenter {
          position: relative;
          justify-content: flex-start;
          padding: 5px;
          height: 45px;
          width: 250px;
          margin: 5px;
          cursor: pointer;
          border: 1px solid #aaa;
          border-radius: 4px;
          &:hover {
            transition: 0.1s;
            border-left: 4px solid #ef5600;
            border-radius: 2px;
          }
          &.active {
            border-left: 4px solid #ef5600;
            border-radius: 2px;
          }

          .ctg-btn {
            position: absolute;
            right: -130px;
            padding: 8px 30px;
            background: #ef5600;
            color: #fff;
            border-radius: 8px;
            font-weight: 500;
          }
        }
      }
    }

    .parent-category {
    }
    .sub-category {
      margin-left: 20px;
    }
  }
`;

export default styles;

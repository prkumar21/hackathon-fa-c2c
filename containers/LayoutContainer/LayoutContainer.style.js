import css from 'styled-jsx/css';

const backgroundColor = '#eee';

const layOutStyle = css`
  $color: red;
  button.my-products-button {
    position: absolute;
    right: 50px;
    top: 30px;
    font-size: 17px;
    text-decoration: underline;
    color: #000;
  }
  .container {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    -ms-flex-direction: row;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    width: 100%;
    min-width: 36rem;
    width: 50%;
    max-width: 50rem;
    padding-top: 10rem;
    padding-bottom: 10rem;
    height: 100vh;
    @mixin desktop {
      max-width: 1280px;
    }
    .input {
      margin: 10px 0;
    }
  }
  .flex-search {
    display: flex;
    width: 100%;
  }
  .small-title {
    font-size: 14px;
    font-weight: bold;
    align-self: flex-start;
    margin-bottom: 10px;
  }
  .big-title {
    align-self: flex-start;
    font-size: 44px;
    margin-bottom: 18px;
  }

  .additional-links {
    width: 100%;
    display: flex;
    height: 30px;
    .create-new-redirect {
      width: 100%;
      border-left: 1px solid #d3d3d3;
      border-right: 1px solid #d3d3d3;
      &:hover {
        text-decoration: underline;
      }
    }
  }
`;

export default layOutStyle;

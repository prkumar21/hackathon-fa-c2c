import React from 'react';
import layOutStyle from './LayoutContainer.style';
import ContainerWrap from '@/ui/Container';
import Router from 'next/router';
import { useRouter } from 'next/router';
import axios from 'axios';
import { WithAppCtx } from 'contexts/AppContext';
import FormInput from '@/ui/FormInput';
import Button from '@/ui/Button';
import { API_HOST } from 'utils/httpService';

const LayoutContainer = ({ isProductsPage, setAppCtx, appCtx }) => {
  const [searchTerm, setSearchTerm] = React.useState('');
  const searchProducts = async () => {
    axios({
      method: 'get',
      url: `${API_HOST}catalog/product`
    }).then(
      (res) => {
        const filteredResults = res.data.products.filter((data) => {
          if (data.name.toLowerCase().includes(searchTerm.toLowerCase())) {
            return data;
          }
        });
        setAppCtx({
          ...appCtx,
          productList: filteredResults
        });
        Router.push('/products');
      },
      () => {}
    );
  };
  const router = useRouter();
  React.useEffect(() => {
    router.prefetch('/products');
  }, []);
  return (
    <React.Fragment>
      <button
        onClick={() => Router.push('/dashboard/myproducts')}
        className="my-products-button"
      >
        Go to My products
      </button>
      <div className={isProductsPage ? '' : 'container'}>
        {!isProductsPage && (
          <h5 className="small-title">To begin adding products</h5>
        )}
        <h2
          style={{ fontSize: isProductsPage ? '24px' : '' }}
          className="big-title"
        >
          Find existing products in Falabella
        </h2>
        <div className="flex-search">
          <FormInput
            id="search"
            name="Search bar"
            placeholder="Product name, product ID"
            onChangeHandler={(e) => setSearchTerm(e.target.value)}
            onEnter={(e) => setSearchTerm(e.target.value)}
          />
          <Button
            className="search-btn"
            label="Search"
            onClick={searchProducts}
            id="search-product"
            type="button"
          />
        </div>
        {!isProductsPage && (
          <div className="additional-links">
            <button
              onClick={() => Router.push('/productOnboaring')}
              className="create-new-redirect"
            >
              I'm adding a product not sold on Falabella
            </button>
            <button
              disabled
              onClick={() => Router.push('/productOnboaring')}
              className="create-new-redirect"
            >
              Add multiple products in bulk
            </button>
          </div>
        )}
      </div>
      <style jsx>{layOutStyle}</style>
    </React.Fragment>
  );
};

export default WithAppCtx(LayoutContainer);

import React, { useState, useEffect } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import Router from 'next/router';
import Button from '@/ui/Button';
import ContainerWrap from '@/ui/Container';
import layOutStyle from './ProductDetailForm.style';
import { WithLabels } from 'contexts/LabelContext';
import { WithDevice } from 'contexts/DeviceContext';
import { useRouter } from 'next/router';
import FormInput from '@/ui/FormInput';
import { WithAppCtx } from 'contexts/AppContext';

const register = async (email, password) => {
  // axios.post(`/api/test?email=${email}&password=${password}`)
  const body = {
    email,
    password
  };
  axios({
    method: 'post',
    url: '/api/test',
    data: body
  }).then(
    (res) => {
      Router.push('/dashboard');
    },
    () => {}
  );
};

const ProductDetailForm = ({ labels, productDetailsHandler }) => {
  const [userData, setUserData] = useState({
    name: '',
    description: '',
    brand_name: ''
  });
  const router = useRouter();

  async function handleSubmit() {
    const flag = Object.keys(userData)
      .map((key) => userData[key])
      .filter(Boolean);
    if (flag.length === 3) {
      productDetailsHandler(userData);
    } else {
      window.alert('Enter all fields');
    }
  }

  useEffect(() => {
    router.prefetch('/dashboard');
  }, []);

  return (
    <ContainerWrap direction="column">
      <div className="add-product">
        <form>
          <FormInput
            label="Product Name"
            onChangeHandler={(event) =>
              setUserData({ ...userData, name: event.target.value })
            }
            value={userData.name}
            name="name"
            id="name"
            type="text"
          />

          <FormInput
            label="Product Description"
            onChangeHandler={(event) =>
              setUserData({ ...userData, description: event.target.value })
            }
            value={userData.description}
            name="mobile"
            id="mobile"
            type="text"
          />
          <FormInput
            label="Brand Name"
            onChangeHandler={(event) =>
              setUserData({ ...userData, brand_name: event.target.value })
            }
            value={userData.brand_name}
            name="brand"
            id="brand"
            type="text"
          />
          <div className="btn-container">
            <Button
              key="login-btn"
              label="Next"
              strech={'auto'}
              size="medium"
              onClick={handleSubmit}
            />
          </div>
        </form>
      </div>
      <style jsx>{layOutStyle}</style>
    </ContainerWrap>
  );
};

ProductDetailForm.defaultProps = {
  labels: {}
};

ProductDetailForm.propTypes = {
  labels: PropTypes.object
};

export default WithLabels(WithAppCtx(ProductDetailForm));
export { ProductDetailForm };

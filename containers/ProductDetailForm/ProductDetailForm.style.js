import css from 'styled-jsx/css';

const layOutStyle = css`
  .add-product {
    @mixin flexFromStart {
      position: fixed;
      margin: 0 auto;
      width: 100%;
      max-width: 1280px;
      padding: 5px 40px;
      flex-direction: column;
      overflow-y: scroll;
      h1 {
        font-size: 18px;
        font-weight: 600;
        margin: 30px 0;
      }
      @mixin smallMobileOnly {
        width: 100%;
        height: 100vh;
        margin: 0 auto;
      }
    }
  }
  form {
    @mixin flexFromStart {
      flex-flow: column;
      gap: 1ch;
      width: 100%;

      .btn-container {
        width: 100%;
        margin: 20px auto;
      }
    }
  }

  label {
    font-weight: 600;
  }
  .error {
    margin: 0.5rem 0 0;
    color: brown;
  }
`;

export default layOutStyle;

import React, { useState } from 'react';
import _ from 'lodash';
import ContainerWrap from 'components/ui/Container';
import styles from './ProductOnboarding.style';
import CategorySelection from 'containers/CategorySelection/CategorySelection';
import ProductDetailForm from 'containers/ProductDetailForm/ProductDetailForm';
import SKUDetailForm from 'containers/SKUDetailForm/SKUDetailForm';
import { WithAppCtx } from 'contexts/AppContext';
import UploadImages from 'components/UploadImages/UploadImages';
import axios from 'axios';
import { API_HOST } from 'utils/httpService';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';

const NewProduct = ({
  appCtx,
  setAppCtx,
  initialStep = 'category',
  addSKUOnly = false
}) => {
  const [tab, setTab] = useState(initialStep);
  const [open, setOpen] = useState(false);
  const categories = _.get(appCtx, 'productDetails.categories', '');
  const [enableSKUForm, setSKUForm] = useState(false);
  const [enableImgesSection, setImgesSection] = useState(false);
  const categoryHandler = (parentCategory, subCategory) => {
    setAppCtx({
      ...appCtx,
      productDetails: {
        ...(appCtx ? appCtx.productDetails : {}),
        categories: [parentCategory, subCategory]
      }
    });
    setTab('product');
  };
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen('');
  };
  const productDetailsHandler = async (formData) => {
    const body = {
      ...formData,
      categories: _.get(appCtx, 'productDetails.categories', '')
    };
    try {
      const url = `${API_HOST}catalog/product/create`;
      const response = await axios({
        method: 'post',
        url: url,
        data: body
      });
      setAppCtx({
        ...appCtx,
        productDetails: {
          ...appCtx.productDetails,
          ...formData,
          product_id: response.data.product_id
        }
      });
      setSKUForm(true);
      setTab('sku');
      setOpen('Yay! Product created successfully');
    } catch (error) {
      console.log(error);
    }
  };
  const SKUDetailsHandler = async (formData) => {
    setAppCtx({
      ...appCtx,
      SKUDetails: {
        ...appCtx.SKUDetails,
        ...formData
      }
    });
    setImgesSection(true);
    setTab('upload');
  };
  const postSKUDetails = async (files) => {
    const medialist = files.map(fl => fl.image_url);
    const isDamaged = files.some(fl => fl.predicted_class === 'Damaged');
    const body = {
      ...appCtx.SKUDetails,
      medialist,
      evaluated_condition: !isDamaged ? 'Good': 'Damaged'
    };
    try {
      if (body.bid.type === 'bargain') {
        delete body.bid.startDate;
        delete body.bid.endDate;
      }
      const url = `${API_HOST}catalog/sku/create`;
      const response = await axios({
        method: 'post',
        url: url,
        data: body
      });
      setOpen('Yay! SKU added successfully');
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <ContainerWrap direction="column">
      <div className="header">
        <h1>{addSKUOnly ? 'Add New SKU' : 'Add New Product'}</h1>
      </div>

      <div className="tabs">
        <button
          className={`tab ${tab === 'category' ? 'active' : ''}`}
          onClick={() => setTab('category')}
          disabled={addSKUOnly}
        >
          Category Mapping
        </button>
        <button
          className={`tab ${tab === 'product' ? 'active' : ''}`}
          onClick={() => setTab('product')}
          disabled={!categories}
        >
          Product Details
        </button>
        <button
          className={`tab ${tab === 'sku' ? 'active' : ''}`}
          onClick={() => setTab('sku')}
          disabled={!enableSKUForm}
        >
          SKU Details
        </button>
        <button
          className={`tab ${tab === 'upload' ? 'active' : ''}`}
          onClick={() => setTab('upload')}
          disabled={!enableImgesSection}
        >
          Upload Images
        </button>
      </div>

      <div className="tabs-content">
        <div className={`${tab === 'category' ? 'visible' : 'hidden'}`}>
          <CategorySelection categoryHandler={categoryHandler} />
        </div>

        <div className={`${tab === 'product' ? 'visible' : 'hidden'}`}>
          <ProductDetailForm productDetailsHandler={productDetailsHandler} />
        </div>
        <div className={`${tab === 'sku' ? 'visible' : 'hidden'}`}>
          <SKUDetailForm SKUDetailsHandler={SKUDetailsHandler} />
        </div>
        <div className={`${tab === 'upload' ? 'visible' : 'hidden'}`}>
          <UploadImages postSKUDetails={postSKUDetails} />
        </div>
      </div>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left'
        }}
        open={!!open}
        autoHideDuration={6000}
        onClose={handleClose}
        message={<p className="snack-bar">{open}</p>}
        action={
          <React.Fragment>
            {/* <Button color="secondary" size="small" onClick={handleClose}>
              
            </Button> */}
            <IconButton
              size="small"
              aria-label="close"
              color="inherit"
              onClick={handleClose}
            >
              {/* <CloseIcon fontSize="small" /> */}
            </IconButton>
          </React.Fragment>
        }
      />
      <style jsx>{styles}</style>
    </ContainerWrap>
  );
};

export default WithAppCtx(NewProduct);

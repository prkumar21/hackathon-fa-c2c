import css from 'styled-jsx/css';

const layOutStyle = css`
  .add-product {
    @mixin flexFromStart {
      margin: 0 auto;
      width: 100%;
      max-width: 1280px;
      padding: 5px 40px;
      flex-direction: column;
      overflow-y: scroll;
      h1 {
        font-size: 18px;
        font-weight: 600;
        margin: 30px 0;
      }
      @mixin smallMobileOnly {
        width: 100%;
        height: 100vh;
        margin: 0 auto;
      }
    }
  }
  form {
    @mixin flexFromStart {
      flex-flow: column;
      gap: 1ch;
      width: 100%;

      .btn-container {
        width: 100%;
        margin: 20px auto;
      }
    }
    .row-wrapper {
      display: flex;
      width: 100%;
    }
    .column-wrapper {
      width: 100%;
      margin-right: 10px;
    }
    .registration-separation {
      text-align: center;
      position: relative;
      top: 2px;
      padding-top: 1px;
      margin-bottom: 14px;
      line-height: 0;
      width: 100%;
      p {
        line-height: 1;
        font-size: 12px;
        color: #767676;
        font-weight: 400;
        z-index: 2;
        position: relative;
        display: inline-block;
        background-color: #fff;
        padding: 0 8px 0 7px;
      }
      &::after {
        content: '';
        width: 100%;
        background-color: transparent;
        display: block;
        height: 1px;
        border-top: 1px solid #e7e7e7;
        position: absolute;
        top: 50%;
        margin-top: -1px;
        z-index: 1;
      }
    }
  }
  .input-field {
    height: 45px;
    margin: 13px 0;
  }
  b {
    font-weight: 700;
  }
  .error {
    margin: 0.5rem 0 0;
    color: brown;
  }
`;

export default layOutStyle;

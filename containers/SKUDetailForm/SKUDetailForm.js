import React, { useState, useEffect } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import Router from 'next/router';
import Switch from '@material-ui/core/Switch';
import Button from '@/ui/Button';
import ContainerWrap from '@/ui/Container';
import layOutStyle from './SKUDetailForm.style';
import { WithLabels } from 'contexts/LabelContext';
import { WithDevice } from 'contexts/DeviceContext';
import { useRouter } from 'next/router';
import FormInput from '@/ui/FormInput';
import { WithAppCtx } from 'contexts/AppContext';
import Select from '../../components/ui/Select';

const submit = async (email, password) => {
  // axios.post(`/api/test?email=${email}&password=${password}`)
  const body = {
    email,
    password
  };
  axios({
    method: 'post',
    url: '/api/test',
    data: body
  }).then(
    (res) => {
      Router.push('/dashboard');
    },
    () => {}
  );
};

const SKUDetailForm = ({ labels, SKUDetailsHandler, appCtx }) => {
  const [userData, setUserData] = useState({
    sellerSkuId: '',
    seller_id: '',
    price: {
      basePrice: '',
      actualPrice: '',
      minAcceptablePrice: ''
    },
    bid: {
      enabled: false,
      range: 0,
      startDate: new Date(),
      endDate: '',
      type: ''
    },
    seller_rating: 1,
    product_id: '',
    quantity: 0,
    internalShippingEnabled: false,
    condition: '',
    shipment: '',
    is_used: false
  });
  useEffect(() => {
    setUserData({
      ..._.get(appCtx, 'productDetails', {}),
      sellerSkuId: '',
      seller_id: localStorage.getItem('sellerUUIDC2c'),
      price: {
        basePrice: '',
        actualPrice: '',
        minAcceptablePrice: ''
      },
      bid: {
        enabled: false,
        range: 0,
        startDate: new Date(),
        endDate: '',
        type: ''
      },
      seller_rating: 0,
      is_used: false,
      quantity: 0,
      internalShippingEnabled: false,
      condition: '',
      shipment: '',
      medialist: []
    });
  }, [appCtx]);
  const router = useRouter();

  const { REGISTER_PAGE_HEADER } = labels;
  async function handleSubmit() {
    const flag = Object.keys(userData)
      .map((key) => userData[key])
      .filter(Boolean);
    SKUDetailsHandler({
      ...userData,
      is_used: userData.is_used === 'Used' ? true : false
    });
  }
  useEffect(() => {
    router.prefetch('/dashboard');
  }, []);

  return (
    <ContainerWrap direction="column">
      <div className="add-product">
        <form>
          <div className="row-wrapper">
            <div className="column-wrapper">
              <FormInput
                label="Seller SKU ID"
                onChangeHandler={(event) =>
                  setUserData({ ...userData, sellerSkuId: event.target.value })
                }
                value={userData.sellerSkuId}
                name="seller_sku_id"
                id="seller_sku_id"
                type="text"
              />
              <FormInput
                label="Quantity"
                onChangeHandler={(event) =>
                  setUserData({ ...userData, quantity: event.target.value })
                }
                value={userData.quantity}
                name="quantity"
                id="quantity"
                type="text"
              />
            </div>
            <div className="column-wrapper">
              <FormInput
                label="Seller ID"
                // onChangeHandler={(event) =>
                //   setUserData({ ...userData, description: event.target.value })
                // }
                value={userData.seller_id}
                disabled
                name="seller_id"
                id="seller_id"
                type="text"
              />
              <Select
                label="Is Used"
                onChangeHandler={(event) =>
                  setUserData({
                    ...userData,
                    is_used: event.target.value
                  })
                }
                value={userData.is_used}
                name="condition"
                id="condition"
                options={['Select', 'Used', 'New']}
              />
            </div>
            <div className="column-wrapper">
              <FormInput
                label="Product ID"
                // onChangeHandler={(event) =>
                //   setUserData({ ...userData, name: event.target.value })
                // }
                disabled
                value={userData.product_id}
                name="name"
                id="name"
                type="text"
              />
              <Select
                label="Selling Condition"
                onChangeHandler={(event) =>
                  setUserData({ ...userData, condition: event.target.value })
                }
                disabled={userData.is_used !== 'Used'}
                value={userData.condition}
                name="condition"
                id="condition"
                options={['Select', 'Used', 'Slightly Used', 'Damaged']}
              />
            </div>
          </div>
          <div className="registration-separation">
            <p>Prices and Offers</p>
          </div>
          <div className="row-wrapper">
            <div className="column-wrapper">
              <div className="input-field">
                <label>Enable bidding</label>
                <Switch
                  color="primary"
                  onChange={(event) => {
                    setUserData({
                      ...userData,
                      bid: {
                        ...userData.bid,
                        enabled: event.target.checked
                      }
                    });
                  }}
                  checked={userData.bid.enabled}
                  name="bidding_flag"
                  id="bidding_flag"
                  type="checkbox"
                />
              </div>
            </div>
            <div className="column-wrapper">
              <Select
                label="Offer Mode"
                onChangeHandler={(event) =>
                  setUserData({
                    ...userData,
                    bid: { ...userData.bid, type: event.target.value }
                  })
                }
                disabled={!userData.bid.enabled}
                value={userData.bid.type}
                name="offerType"
                id="offerType"
                options={['Select', 'bidding', 'bargain']}
              />
            </div>
          </div>
          <div className="row-wrapper">
            <div className="column-wrapper">
              <FormInput
                label="Base Price"
                onChangeHandler={(event) =>
                  setUserData({
                    ...userData,
                    price: {
                      ...userData.price,
                      basePrice: event.target.value
                    }
                  })
                }
                value={userData.price.basePrice}
                name="basePrice"
                id="basePrice"
                type="text"
              />
            </div>
            <div className="column-wrapper">
              <FormInput
                label="Actual Price"
                onChangeHandler={(event) =>
                  setUserData({
                    ...userData,
                    price: {
                      ...userData.price,
                      actualPrice: event.target.value
                    }
                  })
                }
                value={userData.price.actualPrice}
                name="actualPrice"
                id="actualPrice"
                type="text"
              />
            </div>
            <div className="column-wrapper">
              <FormInput
                label="Minimum Acceptable Price"
                onChangeHandler={(event) =>
                  setUserData({
                    ...userData,
                    price: {
                      ...userData.price,
                      minAcceptablePrice: event.target.value
                    }
                  })
                }
                value={userData.price.minAcceptablePrice}
                name="acceptablePrice"
                id="acceptablePrice"
                type="text"
                disabled={userData.bid.type !== 'bargain'}
              />
            </div>
          </div>
          <div className="row-wrapper">
            <div className="column-wrapper">
              <FormInput
                label="Start Date"
                disabled
                onChangeHandler={(event) => {
                  setUserData({
                    ...userData,
                    bid: { ...userData.bid, startDate: event.target.value }
                  });
                }}
                value={userData.bid.startDate}
                name="start_date"
                id="start_date"
                disabled={userData.bid.type !== 'bidding'}
                type="datetime-local"
              />
            </div>

            <div className="column-wrapper">
              <FormInput
                label="End Date"
                onChangeHandler={(event) => {
                  setUserData({
                    ...userData,
                    bid: { ...userData.bid, endDate: event.target.value }
                  });
                }}
                value={userData.bid.endDate}
                disabled={userData.bid.type !== 'bidding'}
                name="end_date"
                id="end_date"
                type="datetime-local"
              />
            </div>
          </div>

          <div className="row-wrapper">
            <div className="column-wrapper">
              <FormInput
                label="Range"
                onChangeHandler={(event) =>
                  setUserData({
                    ...userData,
                    bid: { ...userData.bid, range: event.target.value }
                  })
                }
                value={userData.bid.range}
                disabled={userData.bid.type !== 'bidding'}
                name="Range"
                id="Range"
                type="text"
              />
            </div>
          </div>

          <div className="registration-separation">
            <p>Shipping Details</p>
          </div>
          <div className="row-wrapper">
            <div className="column-wrapper">
              <div className="input-field">
                <label>Enable International Shipping</label>
                <Switch
                  color="primary"
                  style={{ width: 'auto' }}
                  onChange={(event) => {
                    setUserData({
                      ...userData,
                      internalShippingEnabled: event.target.checked
                    });
                  }}
                  checked={userData.internalShippingEnabled}
                  name="bidding_flag"
                  id="bidding_flag"
                  type="checkbox"
                />
              </div>
            </div>
            <div className="column-wrapper">
              <Select
                label="Seller Fulfillment Method"
                onChangeHandler={(event) =>
                  setUserData({ ...userData, shipment: event.target.value })
                }
                value={userData.shipment}
                name="Shipment"
                id="Shipment"
                options={[
                  '',
                  'Falabella Warehouse',
                  'Falabella Delivery System',
                  'Self'
                ]}
              />
            </div>
            <div className="column-wrapper">
              <div className="input-field"></div>
            </div>
          </div>
          <div className="btn-container">
            <Button
              key="login-btn"
              label="Next"
              strech={'auto'}
              size="medium"
              onClick={handleSubmit}
            />
          </div>
        </form>
      </div>
      <style jsx>{layOutStyle}</style>
    </ContainerWrap>
  );
};

SKUDetailForm.defaultProps = {
  labels: {}
};

SKUDetailForm.propTypes = {
  labels: PropTypes.object
};

export default WithLabels(WithAppCtx(SKUDetailForm));
export { SKUDetailForm };

import css from 'styled-jsx/css';

const backgroundColor = '#eee';

const layOutStyle = css`
  .search-page {
    display: flex;
    flex-wrap: wrap;
  }
  .header {
    height: 70px;
  }
  .pdp-pods {
    display: flex;
    flex-wrap: wrap;
    margin-left: 200px;
    justify-content: center;
  }
  $color: red;
  .container {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
    flex-wrap: wrap;
    width: 100%;
    margin: 100px auto;

    @mixin desktop {
      max-width: 1150px;
    }
  }
  .facet-container {
    height: 100%;
    width: 200px;
    position: fixed;
    z-index: 1;
    top: 70px;
    left: 0;
    background-color: #eee;
    overflow-x: hidden;
    padding-top: 20px;

  }
  button.facet-wrapper {
    display: flex;
    width: 100%;
    padding: 6px 8px 6px 16px;
    text-decoration: none;
    font-size: 22px;
    color: #818181;
    display: flex; 
    input {
      margin: 10px;
    }
  }
  .underline {
    text-decoration: underline;
    font-weight: bold
  }
  .separation {
    text-align: center;
    position: relative;
    top: 2px;
    padding-top: 1px;
    margin-bottom: 14px;
    line-height: 0;
    width: 100%;
    p {
      line-height: 1;
      font-size: 20px;
      color: #767676;
      font-weight: 400;
      z-index: 2;
      position: relative;
      display: inline-block;
      background-color: #eee;
      padding: 0 8px 0 7px;
    }
    &::after {
      content: '';
      width: 100%;
      background-color: transparent;
      display: block;
      height: 1px;
      border-top: 1px solid #c3c3c3;
      position: absolute;
      top: 50%;
      margin-top: -1px;
      z-index: 1;
    }
  }
`;

export default layOutStyle;

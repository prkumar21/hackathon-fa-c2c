import React from 'react';
import layOutStyle from './search.style';
import PodCard from 'components/Pods';
import ContainerWrap from '@/ui/Container';
import axios from 'axios';
import { API_HOST } from 'utils/httpService';
import _ from 'lodash';
import Router from 'next/router';

const Facet = ({ facetName, facet, onFacetSelection }) => {
  const [facetValue, setFacetValue] = React.useState(false);
  const handleFacetClick = () => {
    onFacetSelection(facetName, !facetValue);
    setFacetValue(!facetValue);
  };
  return (
    <React.Fragment>
      <button onClick={() => handleFacetClick()} className="facet-wrapper">
        <div style={{ textTransform: 'capitalize' }}>{facetName}</div>(
        <div>{_.get(facet, '[0].doc_count', 0)}</div>)
      </button>
      <style jsx>{layOutStyle}</style>
    </React.Fragment>
  );
};

const SearchLanding = ({ products, facets, query, type = '' }) => {
  const [productList, setProductList] = React.useState([]);
  const [facetList, setFacetList] = React.useState({});
  const [facetSelected, setFacetSelected] = React.useState(false);
  React.useEffect(() => {
    setProductList(products);
    setFacetList(facets);
  }, [products]);
  const onFacetSelection = async (facetName) => {
    setFacetSelected(!facetSelected);
    Router.push(`/search?Ntt=${query}&${facetName}=${!facetSelected}`);
  };
  return (
    <React.Fragment>
    <img className="header" src="../../../static/header-img.png" />
    <div className="search-page">
      {type !== 'seller' && (
        <div className="facet-container">
          <div className="separation">
            <p>Filters</p>
          </div>
          {Object.keys(facetList).map((key) => {
            return (
              <React.Fragment>
                <button className={`facet-wrapper`}>
                  <input
                    onClick={() => onFacetSelection(key)}
                    type="checkbox"
                    selected={facetSelected && key === 'used'}
                    value={facetSelected && key === 'used'}
                  />
                  <div style={{ textTransform: 'capitalize' }}>{key}</div>
                </button>
              </React.Fragment>
            );
          })}
        </div>
      )}
      <div style={{ marginLeft: type === 'seller' ? 0 : '' }} className="pdp-pods">
        {productList.map(
          (
            {
              brand_name,
              name,
              medialist,
              product_id,
              variants = [],
              description,
              sku_id,
              evaluated_condition,
              ...rest
            },
            index
          ) => {
            return (
              <PodCard
                key={`${product_id}-${index}`}
                badge={false}
                brandName={brand_name}
                productName={name}
                isAssured={type !== 'seller' && evaluated_condition === 'Good'}
                description={description}
                media={medialist[0]}
                promoted={false}
                variants={variants}
                price={rest['price.basePrice']}
                skuID={sku_id}
                productId={product_id}
                type={type}
              />
            );
          }
        )}
      </div>
    </div>
      <style jsx>{layOutStyle}</style>
    </React.Fragment>
  );
};

export default SearchLanding;

import css from 'styled-jsx/css';

const layOutStyle = css`
  .login {
    @mixin flexFromStart {
      background: #fff;
      margin: 30px auto;
      width: 440px;
      padding: 5px 40px;
      border: 1px solid #ccc;
      border-radius: 8px;
      box-shadow: 0 2px 7px #dfdfdf;
      flex-direction: column;
      overflow-y: scroll;
      h1 {
        font-size: 18px;
        font-weight: 600;
        margin: 30px 0;
      }
      @mixin smallMobileOnly {
        width: 100%;
        height: 100vh;
        margin: 0 auto;
      }
    }
  }
  form {
    @mixin flexFromStart {
      flex-flow: column;
      gap: 1ch;
      width: 100%;

      .btn-container {
        width: 100%;
        margin: 20px auto;
      }
    }
  }

  label {
    font-weight: 600;
  }
  .error {
    margin: 0.5rem 0 0;
    color: brown;
  }
  ul {
    line-height: 1;
    font-size: 12px;
    color: #767676;
    font-weight: 400;
    z-index: 2;
    position: relative;
    display: inline-block;
    background-color: #fff;
    padding: 0 8px 0 7px;

    li {
      list-style-type: disc;
      margin: 15px 0;
    }
    b {
      font-weight: bold;
      font-size: 12px;
    }
    p {
      margin-top: 4px;
    }
  }
  .snack-bar {
      font-size: 14px
  }
`;

export default layOutStyle;

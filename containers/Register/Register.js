import React, { useState, useEffect } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import Router from 'next/router';
import Button from '@/ui/Button';
import ContainerWrap from '@/ui/Container';
import layOutStyle from './Register.style';
import { WithLabels } from 'contexts/LabelContext';
import { WithDevice } from 'contexts/DeviceContext';
import { useRouter } from 'next/router';
import FormInput from '@/ui/FormInput';
import Select from '../../components/ui/Select';
import { Switch } from '@material-ui/core';
import { API_HOST } from 'utils/httpService';
import { setCookie } from 'utils/cookieStorage';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';

import _ from 'lodash';

const register = async (email, password) => {
  // axios.post(`/api/test?email=${email}&password=${password}`)
  const body = {
    email,
    password
  };
};

const LoginContainer = ({ labels }) => {
  const [userData, setUserData] = useState({
    email: '',
    password: '',
    name: '',
    mobile: '',
    shipmentMethod: '',
    address: ''
  });
  const [open, setOpen] = useState(false);
  // const router = useRouter();

  const openSnackBar = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };
  const { REGISTER_PAGE_HEADER } = labels;
  async function handleSubmit() {
    const flag = Object.keys(userData)
      .map((key) => userData[key])
      .filter(Boolean);
    if (flag.length === 6) {
      try {
        const url = `${API_HOST}seller/register`;
        const response = await axios({
          method: 'post',
          url: url,
          data: userData
        });
        openSnackBar();
        const sellerId = _.get(response, 'data.seller_id', '');
        localStorage.setItem('sellerUUIDC2c', sellerId);
        setCookie({name: 'sellerUUIDC2c', value: sellerId});
        Router.push('/dashboard');
      } catch (error) {
        console.error(error);
      }
    } else {
      window.alert('Enter all fields');
    }
  }

  return (
    <ContainerWrap direction="column">
      <div className="login">
        <h1> {REGISTER_PAGE_HEADER} </h1>
        <form>
          <FormInput
            label="Name"
            onChangeHandler={(event) =>
              setUserData({ ...userData, name: event.target.value })
            }
            value={userData.name}
            name="name"
            id="name"
            type="text"
          />
          <FormInput
            label="Mobile Number"
            onChangeHandler={(event) =>
              setUserData({ ...userData, mobile: event.target.value })
            }
            value={userData.mobile}
            name="mobile"
            id="mobile"
            type="text"
          />
          <FormInput
            label="Email"
            onChangeHandler={(event) =>
              setUserData({ ...userData, email: event.target.value })
            }
            value={userData.email}
            name="email"
            id="email"
            type="text"
          />
          <FormInput
            label="Password"
            onChangeHandler={(event) =>
              setUserData({ ...userData, paswword: event.target.value })
            }
            value={userData.paswword}
            name="paswword"
            id="paswword"
            type="paswword"
          />
          <FormInput
            label="Address"
            onChangeHandler={(event) =>
              setUserData({ ...userData, address: event.target.value })
            }
            value={userData.address}
            name="address"
            id="address"
            type="text"
          />
          <Select
            label="Shipment Method"
            onChangeHandler={(event) =>
              setUserData({ ...userData, shipmentMethod: event.target.value })
            }
            value={userData.shipmentMethod}
            name="Shipment"
            id="Shipment"
            options={[
              '',
              'Falabella Warehouse',
              'Falabella Delivery System',
              'Self'
            ]}
          />
          <ul>
            {/* <li>
              <b>Falabella Warehouse</b>
              <p>
                You store your product in the nearest Falabella warehouse and we
                handle the customer shipments
              </p>
            </li> */}
            <li>
              <b>Falabella Delivery System</b>
              <p>
                You store and pack orders in your location, we pick them up and
                deliver to customers.
              </p>
            </li>
            <li>
              <b>Self</b>
              <p>
                You store and pack orders in your location. Also, you need to
                deliver to customers on your own
              </p>
            </li>
          </ul>
          <div>
            <label>International Shipping</label>
            <Switch
              // onChange={(event) => {
              //   setUserData({
              //     ...userData,
              //     isBiddingEnabled: event.target.checked
              //   });
              // }}
              // checked={userData.isBiddingEnabled}
              color="primary"
              name="internationalShipping"
              id="internationalShipping"
              type="checkbox"
            />
          </div>
          <div className="btn-container">
            <Button
              key="login-btn"
              id="loginButton"
              label="Register"
              strech={'full'}
              size="medium"
              onClick={handleSubmit}
            />
          </div>
        </form>
      </div>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left'
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
        message={
          <p className="snack-bar">
            Registration Successful! Welcome to Falabella Seller App!
          </p>
        }
        action={
          <React.Fragment>
            {/* <Button color="secondary" size="small" onClick={handleClose}>
              
            </Button> */}
            <IconButton
              size="small"
              aria-label="close"
              color="inherit"
              onClick={handleClose}
            >
              {/* <CloseIcon fontSize="small" /> */}
            </IconButton>
          </React.Fragment>
        }
      />
      <style jsx>{layOutStyle}</style>
    </ContainerWrap>
  );
};

LoginContainer.defaultProps = {
  labels: {}
};

LoginContainer.propTypes = {
  labels: PropTypes.object
};

export default WithLabels(LoginContainer);
export { LoginContainer };

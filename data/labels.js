const labels = {
  LOGIN_PAGE_HEADER: 'Sign in to your Account',
  REGISTER_PAGE_HEADER: 'Create your Account',
  REGISTRATION_DIVISORY: 'New to Falabella ?',
  GO_TO_REGISTRATION_PAGE: 'Create your Falabella Seller account'
};

export default labels;

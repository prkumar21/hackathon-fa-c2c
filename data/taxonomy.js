const taxonomy = [
  {
    subCategories: [
      {
        leafCategories: [
          {
            isHighlightLink: true,
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat2018/Celulares-y-Telefonos'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat1280018/Celulares-Basicos',
            label: 'Celulares básicos',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Celulares con planes',
            link: 'https://tienda.falabella.com/falabella-cl/page/planes'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat720161/Smartphones',
            isHighlightLink: false,
            label: 'Smartphones'
          }
        ],
        label: 'Celulares y teléfonos',
        link: 'https://www.falabella.com/falabella-cl/category/cat2018/Celulares-y-Telefonos?isPLP=1'
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat720161/Smartphones',
        label: 'Marcas destacadas',
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat720161/Smartphones?facetSelected=true&f.product.brandName=apple',
            isHighlightLink: false,
            label: 'iPhone'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat720161/Smartphones?facetSelected=true&f.product.brandName=samsung',
            isHighlightLink: false,
            label: 'Samsung'
          },
          {
            label: 'Xiaomi',
            link: 'https://www.falabella.com/falabella-cl/category/cat720161/Smartphones?facetSelected=true&f.product.brandName=xiaomi',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'Motorola',
            link: 'https://www.falabella.com/falabella-cl/category/cat720161/Smartphones?facetSelected=true&f.product.brandName=motorola'
          },
          {
            isHighlightLink: false,
            label: 'Huawei',
            link: 'https://www.falabella.com/falabella-cl/category/cat720161/Smartphones?facetSelected=true&f.product.brandName=huawei'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat2018/Celulares-y-Telefonos?facetSelected=true&f.product.brandName=oppo&isPLP=1',
            isHighlightLink: false,
            label: 'Oppo'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat720161/Smartphones?facetSelected=true&f.product.brandName=vivo',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Vivo'
          }
        ]
      },
      {
        label: 'Accesorios celulares',
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat70014/Accesorios-Celulares',
            label: 'Ver todo',
            isHighlightLink: true
          },
          {
            isHighlightLink: false,
            label: 'Audífonos',
            link: 'https://www.falabella.com/falabella-cl/category/cat1640002/Audifonos'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat7230057/Baterias-Externas',
            isHighlightLink: false,
            label: 'Baterias externas'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat1820030/Carcasas',
            label: 'Carcasas'
          },
          {
            isHighlightLink: false,
            label: 'Cargadores y cables',
            link: 'https://www.falabella.com/falabella-cl/category/cat170030/Cargadores-y-cables'
          },
          {
            label: 'Otros accesorios',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat13750040/Otros-Accesorios-Celulares'
          },
          {
            label: 'Protector de pantalla',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat1920016/Protector-de-Pantalla'
          },
          {
            label: 'Tarjetas de memoria',
            link: 'https://www.falabella.com/falabella-cl/category/cat70037/Tarjetas-de-Memoria',
            isHighlightLink: false
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/category/cat70014/Accesorios-Celulares'
      },
      {
        leafCategories: [
          {
            label: 'Ver todo',
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat7190053/Wearables'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat7190053/Wearables?&facetSelected=true&f.product.attribute.Tipo=Accesorios+smartwatch',
            label: 'Accesorios'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat4290064/Smartband',
            isHighlightLink: false,
            label: 'Smartband'
          },
          {
            isHighlightLink: false,
            label: 'Smartwatches',
            link: 'https://www.falabella.com/falabella-cl/category/cat4290063/SmartWatch'
          },
          {
            label: 'Smartwatches niños',
            link: 'https://www.falabella.com/falabella-cl/category/cat5550008/Smartwatch-Kids',
            meatStickerOption: 0,
            isHighlightLink: false
          }
        ],
        label: 'Wearables',
        link: 'https://www.falabella.com/falabella-cl/category/cat7190053/Wearables'
      }
    ],
    link: 'cat16400010',
    label: 'Celulares y planes'
  },
  {
    subCategories: [
      {
        label: 'TV',
        link: 'https://www.falabella.com/falabella-cl/category/cat1012/TV-y-Video',
        verTodoActive: true,
        leafCategories: [
          {
            isHighlightLink: true,
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat1012/TV-y-Video'
          },
          {
            label: 'Accesorios',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat70050/Accesorios-TV'
          },
          {
            meatStickerOption: 0,
            label: 'LEDs y Smart TV',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat7190148/Televisores-LED'
          },
          {
            label: 'LEDs menores a 50"',
            link: 'https://www.falabella.com/falabella-cl/category/cat7190148/Televisores-LED?facetSelected=true&f.product.attribute.Tama%C3%B1o_de_la_pantalla=22+pulgadas%3A%3A24+pulgadas%3A%3A32+pulgadas%3A%3A40+pulgadas%3A%3A42+pulgadas%3A%3A43+pulgadas%3A%3A49+pulgadas',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'LEDs entre 50" - 55"',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat7190148/Televisores-LED?facetSelected=true&f.product.attribute.Tama%C3%B1o_de_la_pantalla=50+pulgadas%3A%3A55+pulgadas'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat7190148/Televisores-LED?facetSelected=true&f.product.attribute.Tama%C3%B1o_de_la_pantalla=58+pulgadas%3A%3A60+pulgadas%3A%3A65+pulgadas%3A%3A70+pulgadas%3A%3A75+pulgadas%3A%3A77+pulgadas%3A%3A82+pulgadas%3A%3A85+pulgadas',
            isHighlightLink: false,
            label: 'LEDs sobre 55"',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat2070/Proyectores',
            label: 'Proyectores'
          },
          {
            label: 'Soportes',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat70052/Soportes'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat6260040/Streaming',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Streaming'
          }
        ]
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat2005/Audio?isLanding=true',
        leafCategories: [
          {
            label: 'Ver todo',
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat2005/Audio'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat1640002/Audifonos',
            isHighlightLink: false,
            label: 'Audífonos'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat590091/Audio-profesional-y-DJ',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Audio profesional y DJ'
          },
          {
            label: 'Equipos de música y karaokes',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3091/Equipos-de-musica-y-karaokes',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3203/Hi-Fi',
            label: 'Hi-Fi',
            meatStickerOption: 0
          },
          {
            label: 'Instrumentos musicales',
            link: 'https://www.falabella.com/falabella-cl/category/cat3117/Instrumentos-Musicales',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3171/Parlantes-bluetooth',
            label: 'Parlantes Bluetooth'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Soundbar y Home Theater',
            link: 'https://www.falabella.com/falabella-cl/category/cat2045/Soundbar-y-Home-Theater'
          }
        ],
        label: 'Audio'
      },
      {
        label: 'Wearables',
        verTodoActive: true,
        link: 'https://www.falabella.com/falabella-cl/category/cat7190053/Wearables',
        leafCategories: [
          {
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat7190053/Wearables',
            isHighlightLink: true
          },
          {
            label: 'Smartband',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat4290064/Smartband'
          },
          {
            label: 'Smartwatches',
            link: 'https://www.falabella.com/falabella-cl/category/cat4290063/SmartWatch',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'Smartwatches niños',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat5550008/Smartwatch-Kids'
          }
        ]
      },
      {
        label: 'Computación',
        leafCategories: [
          {
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat40052/Computadores',
            isHighlightLink: true,
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2001/Accesorios-Computacion',
            label: 'Accesorios'
          },
          {
            isHighlightLink: false,
            label: 'All In One',
            link: 'https://www.falabella.com/falabella-cl/category/cat40051/All-in-one'
          },
          {
            label: 'Almacenamiento',
            link: 'https://www.falabella.com/falabella-cl/category/cat2003/Almacenamiento',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat7350043/Desktops',
            isHighlightLink: false,
            label: 'Desktops'
          },
          {
            label: 'Impresoras',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2049/Impresoras'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat7230007/Tablets?facetSelected=true&f.product.attribute.Tipo=Ebooks',
            label: 'Kindles & eReaders',
            meatStickerOption: 0
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat2062/Monitores',
            isHighlightLink: false,
            label: 'Monitores'
          },
          {
            isHighlightLink: false,
            label: 'Notebooks',
            link: 'https://www.falabella.com/falabella-cl/category/cat70057/Notebooks'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2082/Routers-y-Conectividad',
            meatStickerOption: 0,
            label: 'Routers y repetidor WiFi'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat2076/Softwares',
            isHighlightLink: false,
            label: 'Softwares'
          },
          {
            label: 'Tablets',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat7230007/Tablets'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat2380002/WebCams',
            isHighlightLink: false,
            label: 'Webcams'
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/category/cat40052/Computadores?isLanding=true'
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat4850013/Computacion-gamer',
        label: 'Computación gamer',
        verTodoActive: true,
        leafCategories: [
          {
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat4850013/Computacion-gamer',
            label: 'Ver todo'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Accesorios',
            link: 'https://www.falabella.com/falabella-cl/category/cat13520029/Accesorios-Gamers'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Audífonos',
            link: 'https://www.falabella.com/falabella-cl/category/cat4930009/Audifonos-Gamers'
          },
          {
            label: 'Desktops',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat10700011/Desktops-Gamers',
            meatStickerOption: 0
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat70057/Notebooks?facetSelected=true&f.product.attribute.Tipo=Gamers',
            isHighlightLink: false,
            label: 'Notebooks',
            meatStickerOption: 0
          }
        ]
      },
      {
        label: 'Videojuegos',
        link: 'https://www.falabella.com/falabella-cl/category/cat2023/Videojuegos?isLanding=true',
        verTodoActive: true,
        leafCategories: [
          {
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat2023/Videojuegos',
            label: 'Ver todo'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3190/Accesorios-Videojuegos',
            label: 'Accesorios',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'Consolas',
            link: 'https://www.falabella.com/falabella-cl/category/cat2023/Videojuegos?facetSelected=true&f.product.attribute.Tipo=3DS%3A%3ANES%3A%3ANintendo+3DS%3A%3ANintendo+Switch%3A%3AOtras+Consolas%3A%3APS3%3A%3APS4%3A%3AXbox+360%3A%3AXbox+One%3A%3APS5',
            meatStickerOption: 0
          },
          {
            label: 'Juegos',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2023/Videojuegos?facetSelected=true&f.product.attribute.Tipo=Videojuegos',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            label: 'Nintendo',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat70003/Nintendo'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat4440005/Playstation',
            label: 'PlayStation'
          },
          {
            label: 'Suscripciones y tarjetas',
            link: 'https://www.falabella.com/falabella-cl/category/cat12530007/Suscripiones-y-tarjetas',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Xbox',
            link: 'https://www.falabella.com/falabella-cl/category/cat4440004/Xbox'
          }
        ]
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat2038/Fotografia?isLanding=true',
        leafCategories: [
          {
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat2038/Fotografia',
            isHighlightLink: true
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Accesorios',
            link: 'https://www.falabella.com/falabella-cl/category/cat7190259/Accesorios-de-fotografia'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Cámaras compactas',
            link: 'https://www.falabella.com/falabella-cl/category/cat70028/Camaras-compactas'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Cámaras de video',
            link: 'https://www.falabella.com/falabella-cl/category/cat2014/Camaras-de-video'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat7230012/Camaras-deportivas',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Cámaras deportivas'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2038/Fotografia?facetSelected=true&f.product.attribute.Tipo=C%C3%A1maras+instant%C3%A1neas',
            label: 'Cámaras instantáneas'
          },
          {
            label: 'Cámaras profesionales',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat18230032/Camaras-Profesionales'
          },
          {
            meatStickerOption: 0,
            label: 'Cámaras semiprofesionales',
            link: 'https://www.falabella.com/falabella-cl/category/cat7230011/Camaras-Semiprofesionales',
            isHighlightLink: false
          },
          {
            label: 'Drones',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat7230062/Drones',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat70040/Lentes--Filtros-y-Flash',
            meatStickerOption: 0,
            label: 'Lentes, filtros y flash',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'Tarjetas de memoria',
            link: 'https://www.falabella.com/falabella-cl/category/cat70037/Tarjetas-de-Memoria'
          }
        ],
        label: 'Fotografía'
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat7190093/Smart-Home',
        label: 'Smart Home',
        leafCategories: [
          {
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat7190093/Smart-Home',
            isHighlightLink: true
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat13490034/Asistentes-por-Voz',
            label: 'Asistentes de voz',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat8600022/Aspiradoras-Robot',
            isHighlightLink: false,
            label: 'Aspiradoras robot'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat7190100/Gadgets',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Gadgets y novedades'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat7190096/Iluminacion-inteligente',
            isHighlightLink: false,
            label: 'Iluminación inteligente',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Proyectores',
            link: 'https://www.falabella.com/falabella-cl/category/cat14710001/Proyectores'
          },
          {
            meatStickerOption: 0,
            label: 'Seguridad y vigilancia',
            link: 'https://www.falabella.com/falabella-cl/category/cat7190094/Camaras-de-Seguridad-y-Vigilancia',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            label: 'Streaming',
            link: 'https://www.falabella.com/falabella-cl/category/cat6260040/Streaming',
            isHighlightLink: false
          }
        ]
      }
    ],
    label: 'Tecnología',
    link: 'cat7090034'
  },
  {
    link: 'cat700256',
    label: 'Zapatos',
    bannerLocation: 'urlToImage',
    subCategories: [
      {
        leafCategories: [
          {
            label: 'Ver todo',
            link: 'https://tienda.falabella.com/falabella-cl/page/zapatos',
            isHighlightLink: true
          },
          {
            isHighlightLink: false,
            label: 'SNEAKER CORNER',
            link: 'https://tienda.falabella.com/falabella-cl/page/zapatillas',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            label: 'INSPÍRATE EN MODA F',
            meatStickerOption: 0,
            link: 'https://tienda.falabella.com/falabella-cl/page/zapatos'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/collection/zapatos-diseno-chileno',
            isHighlightLink: false,
            label: 'Diseño chileno',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/Zapatos-Plataforma',
            label: 'Zapatos plataforma',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/collection/zapatos-comodos-mujer',
            label: 'Zapatos cómodos',
            meatStickerOption: 0,
            isHighlightLink: false
          }
        ],
        verTodoActive: true,
        label: 'Tendencias - Inspiración',
        link: 'https://tienda.falabella.com/falabella-cl/page/zapatos'
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat12440001/Zapatos',
        leafCategories: [
          {
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat12440001/Zapatos',
            isHighlightLink: true
          },
          {
            link: 'https://www.falabella.com/falabella-cl/collection/zapatos-mujer-nueva-temporada',
            isHighlightLink: false,
            label: 'NUEVA TEMPORADA',
            meatStickerOption: 0
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat690245/Botines',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Botines'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat12440001/Zapatos?facetSelected=true&f.product.attribute.Tipo=Hawaianas',
            label: 'Hawaianas'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat16630007/Pantuflas-mujer',
            label: 'Pantuflas',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            label: 'Sandalias',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat690247/Sandalias'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat850068/Zapatillas',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Zapatillas'
          },
          {
            meatStickerOption: 0,
            label: 'Zapatillas outdoor',
            link: 'https://www.falabella.com/falabella-cl/category/cat7500147/Zapatillas-outdoor-mujer',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Zapatillas running',
            link: 'https://www.falabella.com/falabella-cl/category/cat7500146/Zapatillas-deportivas-mujer?f.product.attribute.Disciplina=Cross+training%3A%3ARunning&facetSelected=true'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat690239/Zapatillas-urbanas-mujer',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Zapatillas urbanas'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat12440001/Zapatos?f.product.attribute.Tipo=Alpargatas%3A%3ABallerinas%3A%3AMocasines%3A%3AZapatos+casuales%3A%3AZapatos+formales&facetSelected=true&facetSelected=true%2Ctrue%2Ctrue%2Ctrue&isPLP=1',
            isHighlightLink: false,
            label: 'Zapatos'
          }
        ],
        label: 'Mujer',
        verTodoActive: true
      },
      {
        verTodoActive: true,
        label: 'Hombre',
        leafCategories: [
          {
            label: 'Ver todo',
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat1720006/Zapatos'
          },
          {
            meatStickerOption: 0,
            label: 'NUEVA TEMPORADA',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/zapatos-hombre-nueva-coleccion'
          },
          {
            isHighlightLink: false,
            label: 'Botines',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat1720008/Zapatos-hombre?f.product.attribute.Tipo=Botas%3A%3ABotines'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat1720006/Zapatos?&facetSelected=true&f.product.attribute.Tipo=Hawaianas',
            label: 'Hawaianas'
          },
          {
            isHighlightLink: false,
            label: 'Mocasines',
            link: 'https://www.falabella.com/falabella-cl/category/cat1720006/Zapatos?facetSelected=true&f.product.attribute.Tipo=Mocasines',
            meatStickerOption: 0
          },
          {
            label: 'Pantuflas',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat1720006/Zapatos?facetSelected=true&f.product.attribute.Tipo=Pantuflas'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Sandalias',
            link: 'https://www.falabella.com/falabella-cl/category/cat1720006/Zapatos?&f.product.attribute.Tipo=Sandalias'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Zapatillas',
            link: 'https://www.falabella.com/falabella-cl/category/cat850064/Zapatillas'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Zapatillas de fútbol',
            link: 'https://www.falabella.com/falabella-cl/category/cat6930355/Zapatillas-deportivas-hombre?f.product.attribute.Disciplina=Baby+f%C3%BAtbol%3A%3AF%C3%BAtbol&facetSelected=true'
          },
          {
            meatStickerOption: 0,
            label: 'Zapatillas outdoor',
            link: 'https://www.falabella.com/falabella-cl/category/cat6930356/Zapatillas-outdoor-hombre',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat6930355/Zapatillas-deportivas-hombre?f.product.attribute.Disciplina=Cross+training%3A%3ARunning&facetSelected=true',
            label: 'Zapatillas running',
            isHighlightLink: false
          },
          {
            label: 'Zapatillas urbanas',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat7500133/Zapatillas-urbanas-hombre',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'Zapatos casual',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat1720006/Zapatos?f.product.attribute.Tipo=Mocasines%3A%3AZapatos+casuales'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Zapatos de seguridad',
            link: 'https://www.falabella.com/falabella-cl/category/CATG10787/Zapatos-de-Seguridad'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat1720006/Zapatos?facetSelected=true&f.product.attribute.Tipo=Zapatos+formales',
            label: 'Zapatos formal',
            meatStickerOption: 0,
            isHighlightLink: false
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/category/cat1720006/Zapatos'
      },
      {
        link: 'https://www.falabella.com/falabella-cl/collection/Ver-Todo-Zapatos-Ninos',
        leafCategories: [
          {
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/collection/Ver-Todo-Zapatos-Ninos',
            label: 'Ver todo'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/zapatos-ninos-nueva-coleccion',
            label: 'NUEVA TEMPORADA'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/Ver-Todo-Zapatos-Ninos?f.product.attribute.Tipo=Botas%3A%3ABotas+de+lluvia%3A%3ABotines',
            isHighlightLink: false,
            label: 'Botas y botines'
          },
          {
            label: 'Sandalias',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/Ver-Todo-Zapatos-Ninos?f.product.attribute.Tipo=Sandalias'
          },
          {
            label: 'Zapatillas niñas',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat4740101/Zapatillas-nina'
          },
          {
            label: 'Zapatillas niños',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat4740100/Zapatillas-nino',
            isHighlightLink: false
          },
          {
            label: 'Zapatos escolares niña',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/zapatos-y-zapatillas-escolares?facetSelected=true&f.product.attribute.G%C3%A9nero=Mujer%3A%3ANi%C3%B1a%3A%3AUnisex'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/zapatos-y-zapatillas-escolares?facetSelected=true&f.product.attribute.G%C3%A9nero=Hombre%3A%3ANi%C3%B1o%3A%3AUnisex',
            isHighlightLink: false,
            label: 'Zapatos escolares niños'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/Ver-Todo-Zapatos-Nina',
            isHighlightLink: false,
            label: 'Zapatos niñas'
          },
          {
            label: 'Zapatos niños',
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-zapatos-nino',
            meatStickerOption: 0,
            isHighlightLink: false
          }
        ],
        label: 'Infantil',
        verTodoActive: true
      },
      {
        verTodoActive: true,
        link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-zapatos',
        leafCategories: [
          {
            label: 'Adidas',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2083/Zapatillas?facetSelected=true&f.product.brandName=adidas%3A%3Aadidas+originals'
          },
          {
            isHighlightLink: false,
            label: 'Aldo',
            meatStickerOption: 0,
            link: 'https://tienda.falabella.com/falabella-cl/page/aldo'
          },
          {
            label: 'Americanino',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-zapatos?f.product.brandName=americanino',
            isHighlightLink: false
          },
          {
            label: 'Basement',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-zapatos?f.product.brandName=basement'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-zapatos?f.product.brandName=call+it+spring',
            isHighlightLink: false,
            label: 'Call it Spring'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Cat',
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-zapatos?facetSelected=true&f.product.brandName=cat'
          },
          {
            meatStickerOption: 0,
            label: 'Champion',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2083/Zapatillas?&facetSelected=true&f.product.brandName=champion'
          },
          {
            isHighlightLink: false,
            label: 'Clarks',
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-zapatos?f.product.brandName=clarks',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            label: 'Converse',
            link: 'https://www.falabella.com/falabella-cl/category/cat2083/Zapatillas?facetSelected=true&f.product.brandName=converse',
            meatStickerOption: 0
          },
          {
            label: 'Dr Martens',
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-zapatos?f.product.brandName=dr+martens',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat2083/Zapatillas?facetSelected=true&f.product.brandName=fila',
            isHighlightLink: false,
            label: 'Fila',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            label: 'Hunter',
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-zapatos?f.product.brandName=hunter',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2083/Zapatillas?f.product.brandName=lippi',
            meatStickerOption: 0,
            label: 'Lippi'
          },
          {
            label: 'Michael Kors',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat12440001/Zapatos?facetSelected=true&f.product.brandName=michael+kors'
          },
          {
            isHighlightLink: false,
            label: 'New Balance',
            link: 'https://www.falabella.com/falabella-cl/category/cat2083/Zapatillas?facetSelected=true&f.product.brandName=new+balance',
            meatStickerOption: 0
          },
          {
            label: 'Nike',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat2083/Zapatillas?mkid=HU_GE_VER_170003492&facetSelected=true&f.product.brandName=nike',
            isHighlightLink: false
          },
          {
            label: 'Panamajack',
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-zapatos?&facetSelected=true&f.product.brandName=panama+jack%3A%3Apanamajack',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat2083/Zapatillas?facetSelected=true&f.product.brandName=puma',
            label: 'Puma',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            label: 'Skechers',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/Ver-Todo-Zapatillas?&facetSelected=true&f.product.brandName=skechers'
          },
          {
            meatStickerOption: 0,
            label: 'Sybilla',
            link: 'https://www.falabella.com/falabella-cl/category/cat12440001/Zapatos?facetSelected=true&f.product.brandName=sybilla',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2083/Zapatillas?facetSelected=true&f.product.brandName=vans',
            label: 'Vans',
            meatStickerOption: 0
          }
        ],
        label: 'Marcas destacadas'
      }
    ]
  },
  {
    subCategories: [
      {
        label: 'Electrodomésticos cocina',
        link: 'https://www.falabella.com/falabella-cl/category/cat2034/Electrodomesticos-Cocina',
        leafCategories: [
          {
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat2034/Electrodomesticos-Cocina',
            label: 'Ver todo '
          },
          {
            label: 'Batidoras',
            link: 'https://www.falabella.com/falabella-cl/category/cat9890052/Batidoras',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3045/Cafeteras-electricas',
            label: 'Cafeteras eléctricas'
          },
          {
            label: 'Freidoras',
            link: 'https://www.falabella.com/falabella-cl/category/cat3099/Freidoras',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'Hervidores',
            link: 'https://www.falabella.com/falabella-cl/category/cat3110/Hervidores'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3114/Hornos-Electricos',
            label: 'Hornos eléctricos',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3141/Licuadoras',
            label: 'Licuadoras'
          },
          {
            isHighlightLink: false,
            label: 'Microondas',
            link: 'https://www.falabella.com/falabella-cl/category/cat3151/Microondas'
          },
          {
            isHighlightLink: false,
            label: 'Ollas multifuncionales',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat7230035/Ollas-multiuso-y-arroceras'
          },
          {
            isHighlightLink: false,
            label: 'Robot de cocina',
            link: 'https://www.falabella.com/falabella-cl/category/cat8410002/Robot-de-Cocina'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat6740089/Cocina-Entretenida',
            label: 'Cocina entretenida',
            isHighlightLink: false,
            meatStickerOption: 0
          }
        ]
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat7190001/Aspirado-y-Limpieza',
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat7190001/Aspirado-y-Limpieza',
            label: 'Ver todo',
            isHighlightLink: true
          },
          {
            label: 'Aspiradoras de arrastre',
            link: 'https://www.falabella.com/falabella-cl/category/cat3025/Aspiradoras',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat8600026/Aspiradoras-Portatiles',
            label: 'Aspiradoras portátiles'
          },
          {
            isHighlightLink: false,
            label: 'Aspiradoras robot',
            link: 'https://www.falabella.com/falabella-cl/category/cat8600022/Aspiradoras-Robot'
          }
        ],
        label: 'Aspirado y limpieza'
      },
      {
        label: 'Refrigeradores',
        leafCategories: [
          {
            isHighlightLink: true,
            meatStickerOption: 0,
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat3205/Refrigeradores'
          },
          {
            isHighlightLink: false,
            label: 'Freezers',
            link: 'https://www.falabella.com/falabella-cl/category/cat4048/Freezers'
          },
          {
            isHighlightLink: false,
            label: 'Frigobares',
            link: 'https://www.falabella.com/falabella-cl/category/cat4049/Frigobar'
          },
          {
            label: 'Línea industrial',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat10896522/Refrigeracion-Industrial'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3205/Refrigeradores?f.product.attribute.Tipo=Bottom+freezer%3A%3AMonopuerta%3A%3ATop+mount&isPLP=1%2C1%2C1%2C1&isPLP=1',
            label: 'Refrigeradores',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            label: 'Side by Side',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat4091/Side-by-Side'
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/category/cat3205/Refrigeradores'
      },
      {
        leafCategories: [
          {
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat3065/Cocina',
            isHighlightLink: true
          },
          {
            label: 'Campanas',
            link: 'https://www.falabella.com/falabella-cl/category/cat4026/Campanas',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'Cocinas a gas',
            link: 'https://www.falabella.com/falabella-cl/category/cat70012/Cocinas-a-gas'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat4045/Encimeras',
            isHighlightLink: false,
            label: 'Encimeras'
          },
          {
            label: 'Hornos empotrables',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat4054/Hornos-Empotrables'
          },
          {
            meatStickerOption: 0,
            label: 'Kits empotrados',
            link: 'https://www.falabella.com/falabella-cl/category/cat10700008/Kit-Empotrados',
            isHighlightLink: false
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/category/cat3065/Cocina',
        label: 'Cocina'
      },
      {
        label: 'Lavado y planchado',
        link: 'https://www.falabella.com/falabella-cl/category/cat3136/Lavado',
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3136/Lavado',
            label: 'Ver todo',
            isHighlightLink: true
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat4060/Lavadoras',
            isHighlightLink: false,
            label: 'Lavadoras'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat1700002/Lavadoras-Secadoras',
            isHighlightLink: false,
            label: 'Lavadoras secadoras'
          },
          {
            isHighlightLink: false,
            label: 'Lavavajillas',
            link: 'https://www.falabella.com/falabella-cl/category/cat4061/Lavavajillas'
          },
          {
            isHighlightLink: false,
            label: 'Planchas convencionales',
            link: 'https://www.falabella.com/falabella-cl/category/cat8530004/Planchas-Convencionales'
          },
          {
            isHighlightLink: false,
            label: 'Planchas verticales',
            link: 'https://www.falabella.com/falabella-cl/category/cat8530006/Planchas-Verticales'
          },
          {
            label: 'Secadoras',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat4088/Secadoras'
          }
        ]
      },
      {
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/page/costuras',
            label: 'Ver todo',
            isHighlightLink: true
          },
          {
            meatStickerOption: 0,
            label: 'Accesorios y complementos',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat8540013/Accesorios-y-Complementos'
          },
          {
            isHighlightLink: false,
            label: 'Bordadoras',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat8540011/Bordadoras'
          },
          {
            label: 'Cortadoras',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat14790018/Cortadoras'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat2024/Maquinas-de-Coser',
            label: 'Máquinas de coser'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat13620018/Overlock-y-Quilting',
            meatStickerOption: 0,
            label: 'Overlock y quilting'
          }
        ],
        label: 'Máquinas de coser',
        verTodoActive: true,
        link: 'https://www.falabella.com/falabella-cl/page/costuras'
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat19110021/Equipamiento-Industrial',
        label: 'Equipamiento industrial',
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat19110021/Equipamiento-Industrial',
            label: 'Ver todo ',
            isHighlightLink: true
          },
          {
            label: 'Accesorios comerciales',
            link: 'https://www.falabella.com/falabella-cl/category/cat10897314/Accesorios-Comerciales',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat10897842/Gastronomia',
            isHighlightLink: false,
            label: 'Gastronomía'
          },
          {
            label: 'Mueblería y grifería',
            link: 'https://www.falabella.com/falabella-cl/category/cat11264206/Muebleria-y-Griferia',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat10896522/Refrigeracion-Industrial',
            label: 'Refrigeración industrial'
          }
        ]
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat2019/Aire-Acondicionado',
        leafCategories: [
          {
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat2019/Aire-Acondicionado',
            label: 'Ver todo'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat2019/Aire-Acondicionado',
            label: 'Aire acondicionado',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            label: 'Aire acondicionado split',
            link: 'https://www.falabella.com/falabella-cl/category/cat7830014/Split',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Aire acondicionado portátil',
            link: 'https://www.falabella.com/falabella-cl/category/cat7830015/Portatiles'
          },
          {
            label: 'Calefonts y termos',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2013/Calefont-y-Termos'
          },
          {
            meatStickerOption: 0,
            label: 'Calientacamas',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3046/Calientacamas'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat170042/Enfriadores-de-aire?mkid=PL_HU_VER_1000008583',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Enfriadores'
          },
          {
            label: 'Estufas',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat7170003/Calefaccion'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3197/Purificadores',
            isHighlightLink: false,
            label: 'Purificadores'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Ventiladores',
            link: 'https://www.falabella.com/falabella-cl/category/cat3254/Ventiladores'
          }
        ],
        label: 'Climatización'
      },
      {
        leafCategories: [
          {
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat2025/Tecnologia-para-la-Belleza',
            isHighlightLink: true
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Afeitadoras y cortapelos',
            link: 'https://www.falabella.com/falabella-cl/category/cat8950035/Afeitadoras-y-Cortapelos'
          },
          {
            meatStickerOption: 0,
            label: 'Alisadores de pelo',
            link: 'https://www.falabella.com/falabella-cl/category/cat3018/Alisadores-de-pelo',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3085/Depiladoras',
            label: 'Depiladoras'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3170/Onduladores-de-pelo',
            isHighlightLink: false,
            label: 'Onduladores de pelo'
          },
          {
            isHighlightLink: false,
            label: 'Secadores de pelo',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3223/Secadores-de-pelo'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat7660011/Tecnologia-para-la-belleza',
            label: 'Tecnología para la belleza'
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/category/cat2025/Tecnologia-para-la-Belleza',
        label: 'Cuidado personal',
        verTodoActive: true
      }
    ],
    link: 'cat16510006',
    label: 'Electrohogar'
  },
  {
    label: 'Muebles y organización',
    link: 'cat16510005',
    subCategories: [
      {
        leafCategories: [
          {
            isHighlightLink: true,
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat1008/Muebles-y-Organizacion'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat1008/Muebles?&f.product.brandName=amoble%3A%3Aanthropologie%3A%3Aanthropologie+home%3A%3Acrate+%26+barrel%3A%3Ajohn+lewis%3A%3Amilk%3A%3Asauder%3A%3Asur+dise%C3%B1o%3A%3Athe+popular+design&facetSelected=true',
            isHighlightLink: false,
            label: 'MARCAS PREMIUM'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Banquetas y pouffs',
            link: 'https://www.falabella.com/falabella-cl/category/cat10600007/Banquetas-y-poufs'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3032/Bar',
            label: 'Muebles de bar',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'Berger y reclinables',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3036/Bergers-y-Reclinables'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3180021/Camas-infantiles',
            isHighlightLink: false,
            label: 'Camas infantiles'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3063/Closet',
            meatStickerOption: 0,
            label: 'Clóset'
          },
          {
            isHighlightLink: false,
            label: 'Cómodas y cajoneras',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3074/Comodas-y-Cajoneras'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat10170015/Cunas-de-Madera',
            isHighlightLink: false,
            label: 'Cunas'
          },
          {
            label: 'Escritorios y sillas',
            link: 'https://www.falabella.com/falabella-cl/category/cat2046/Oficina-y-escritorio',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat1810012/Estantes--repisas-y-soportes',
            label: 'Estantes y repisas'
          },
          {
            label: 'Juegos de comedor',
            link: 'https://www.falabella.com/falabella-cl/category/cat3123/Juegos-de-Comedor',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            label: 'Mesas centro y arrimos',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2940012/Mesas-Centro--Laterales-y-Arrimos'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3149/Mesas-de-Comedor',
            isHighlightLink: false,
            label: 'Mesas de comedor'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat5140014/Muebles-de-TV-y-Racks',
            isHighlightLink: false,
            label: 'Muebles de TV y racks'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3212/Respaldos-y-Veladores',
            label: 'Respaldos y veladores',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            label: 'Sillas de comedor',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3229/Sillas-de-Comedor'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat8960005/Sofas-y-Sillones',
            label: 'Sofás y sillones'
          },
          {
            label: 'Terrazas',
            link: 'https://www.falabella.com/falabella-cl/category/cat1180024/Muebles-de-terraza',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            label: 'Vitrinas y buffets',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat840027/Vitrinas-y-Buffet'
          }
        ],
        label: 'Muebles por tipo',
        link: 'https://www.falabella.com/falabella-cl/category/cat1008/Muebles-y-Organizacion'
      },
      {
        leafCategories: [
          {
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat16610015/Living---Sala-de-Estar',
            isHighlightLink: true
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat1008/Muebles?&f.product.brandName=amoble%3A%3Aanthropologie%3A%3Aanthropologie+home%3A%3Acrate+%26+barrel%3A%3Ajohn+lewis%3A%3Amilk%3A%3Asauder%3A%3Asur+dise%C3%B1o%3A%3Athe+popular+design&facetSelected=true',
            isHighlightLink: false,
            label: 'MARCAS PREMIUM'
          },
          {
            isHighlightLink: false,
            label: 'Bergers y reclinables',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3036/Bergers-y-Reclinables'
          },
          {
            label: 'Estantes y repisas',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat1810012/Estantes--repisas-y-soportes',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            label: 'Mesas centro y arrimos',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2940012/Mesas-Centro--Laterales-y-Arrimos'
          },
          {
            label: 'Muebles de TV y racks',
            link: 'https://www.falabella.com/falabella-cl/category/cat5140014/Muebles-de-TV-y-Racks',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            label: 'Sofás y sillones',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat8960005/Sofas-y-Sillones'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3107/Sofas-cama-y-futones',
            meatStickerOption: 0,
            label: 'Sofás Camas y Futones'
          },
          {
            isHighlightLink: false,
            label: 'Sitiales y Poltronas',
            link: 'https://www.falabella.com/falabella-cl/category/cat3234/Sitiales-y-poltronas',
            meatStickerOption: 0
          },
          {
            label: 'Banquetas y pouffs',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat10600007/Banquetas-y-poufs',
            isHighlightLink: false
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/category/cat16610015/Living---Sala-de-Estar',
        verTodoActive: true,
        label: 'Living y sala de estar'
      },
      {
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat2021/Muebles-de-comedor',
            label: 'Ver todo',
            isHighlightLink: true
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'MARCAS PREMIUM',
            link: 'https://www.falabella.com/falabella-cl/category/cat2021/Muebles-de-comedor?facetSelected=true&f.product.brandName=crate+%26+barrel%3A%3Ajohn+lewis%3A%3Amilk%3A%3Asauder%3A%3Asur+dise%C3%B1o%3A%3Athe+popular+design'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Muebles de bar',
            link: 'https://www.falabella.com/falabella-cl/category/cat3032/Bar'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3123/Juegos-de-Comedor',
            label: 'Juegos de comedor',
            isHighlightLink: false
          },
          {
            label: 'Mesas de comedor',
            link: 'https://www.falabella.com/falabella-cl/category/cat3149/Mesas-de-Comedor',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3229/Sillas-de-Comedor',
            label: 'Sillas de comedor',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            label: 'Vitrinas y buffet',
            link: 'https://www.falabella.com/falabella-cl/category/cat840027/Vitrinas-y-Buffet',
            isHighlightLink: false
          }
        ],
        label: 'Comedor',
        link: 'https://www.falabella.com/falabella-cl/category/cat2021/Muebles-de-comedor'
      },
      {
        verTodoActive: true,
        link: 'https://www.falabella.com/falabella-cl/category/cat2058/Maleteria-y-viajes',
        leafCategories: [
          {
            label: 'Ver todo',
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat2058/Maleteria-y-viajes'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat1870002/Accesorios-de-Viaje',
            meatStickerOption: 0,
            label: 'Accesorios de Viaje'
          },
          {
            meatStickerOption: 0,
            label: 'Maletas',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3143/Maletas'
          }
        ],
        label: 'Maletas y viajes'
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat3180022/Muebles-Infantiles',
        leafCategories: [
          {
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat3180022/Muebles-Infantiles',
            isHighlightLink: true
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3180021/Camas-infantiles',
            meatStickerOption: 0,
            label: 'Camas infantiles',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'Mesas y escritorios',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat16080001/Escritorios-Infantiles'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Sillas Infantiles',
            link: 'https://www.falabella.com/falabella-cl/category/cat12830003/Sillas--Sofas-y-Mesas-infantiles'
          }
        ],
        verTodoActive: true,
        label: 'Muebles de niños'
      },
      {
        leafCategories: [
          {
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat5870049/Muebles-de-Dormitorio',
            label: 'Ver todo'
          },
          {
            meatStickerOption: 0,
            label: 'Clóset',
            link: 'https://www.falabella.com/falabella-cl/category/cat3063/Closet',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'Cómodas y cajoneras',
            link: 'https://www.falabella.com/falabella-cl/category/cat3074/Comodas-y-Cajoneras'
          },
          {
            isHighlightLink: false,
            label: 'Dormitorio Infantil',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3180022/Muebles-Infantiles'
          },
          {
            isHighlightLink: false,
            label: 'Respaldos y veladores',
            link: 'https://www.falabella.com/falabella-cl/category/cat3212/Respaldos-y-Veladores'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat11640018/Zapateros',
            label: 'Zapateros'
          }
        ],
        label: 'Dormitorio',
        link: 'https://www.falabella.com/falabella-cl/category/cat5870049/Muebles-de-Dormitorio'
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat2046/Oficina-y-escritorio',
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat2046/Oficina-y-escritorio',
            label: 'Ver todo',
            isHighlightLink: true
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat9130008/Sillas-de-Escritorio',
            label: 'Sillas de escritorio'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Escritorios',
            link: 'https://www.falabella.com/falabella-cl/category/cat3158/Escritorios'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/CATG10143/Lockers',
            label: 'Lockers'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/CATG10144/Gabinetes-y-kardex',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Gabinetes y kardex'
          }
        ],
        verTodoActive: true,
        label: 'Oficina y escritorio'
      },
      {
        verTodoActive: true,
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/CATG10139/Organizacion',
            label: 'Ver todo',
            isHighlightLink: true
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Cajas y canastos',
            link: 'https://www.falabella.com/falabella-cl/category/cat6210052/Cajas-organizadoras-y-canastos'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/CATG10165/Mudanza',
            label: 'Mudanza'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Organización baño',
            link: 'https://www.falabella.com/falabella-cl/category/CATG10169/Organizacion-de-Bano'
          },
          {
            meatStickerOption: 0,
            label: 'Organización cocina',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat8950026/Organizacion-de-Cocina'
          },
          {
            label: 'Organización dormitorio',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat8950028/Organizacion-de-Dormitorio'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/CATG10159/Organizacion-de-Escritorio',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Organización escritorio'
          },
          {
            meatStickerOption: 0,
            label: 'Organización infantil',
            link: 'https://www.falabella.com/falabella-cl/category/cat16260008/Organizacion-Infantil',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat11640016/Perchas--percheros-y-ganchos',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Perchas y ganchos'
          },
          {
            label: 'Soportes, rieles y escuadras',
            link: 'https://www.falabella.com/falabella-cl/category/CATG10142/Soportes--Rieles-y-Escuadras',
            meatStickerOption: 0,
            isHighlightLink: false
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/category/CATG10139/Organizacion',
        label: 'Organización y almacenamiento'
      },
      {
        link: 'https://tienda.falabella.com/falabella-cl/page/crateandbarrel',
        leafCategories: [
          {
            link: 'https://tienda.falabella.com/falabella-cl/page/crateandbarrel',
            label: 'Ver todo',
            isHighlightLink: true
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/alfombras-y-textil-crate--26-barrel',
            label: 'Alfombras y textil'
          },
          {
            label: 'Comedor y cocina',
            link: 'https://www.falabella.com/falabella-cl/collection/comedor-y-cocina-crate-barrel',
            isHighlightLink: false
          },
          {
            label: 'Decoración',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat2026/Decoracion-e-iluminacion?facetSelected=true&f.product.brandName=crate+%26+barrel%3A%3Acurrey+and+company%3A%3Akrosno'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Muebles',
            link: 'https://www.falabella.com/falabella-cl/category/cat1008/Muebles?facetSelected=true&f.product.brandName=crate+%26+barrel'
          },
          {
            label: 'Ofertas',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/sale-crate--26-barrel',
            meatStickerOption: 0
          }
        ],
        label: 'Crate&Barrel'
      },
      {
        label: 'Tienda virtual',
        link: 'https://www.falabella.com/falabella-cl/collection/realidad-aumentada',
        verTodoActive: true,
        leafCategories: [
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Realidad aumentada',
            link: 'https://www.falabella.com/falabella-cl/collection/realidad-aumentada'
          }
        ]
      }
    ]
  },
  {
    label: 'Dormitorio',
    link: 'cat1005',
    subCategories: [
      {
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat13720010/Camas',
            isHighlightLink: true,
            label: 'Ver todo'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat1005/Dormitorio?facetSelected=true&f.product.brandName=cannon%3A%3Acelta%3A%3Acic%3A%3Adrimkip%3A%3Adr%C3%B6m%3A%3Aflex%3A%3Arosen%3A%3Atempur',
            label: 'MARCAS PREMIUM'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat13720015/Camas-1-Plaza',
            label: '1 plaza'
          },
          {
            label: '1,5 plazas',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat13720017/Camas-1-5-Plazas'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat13720016/Camas-2-Plazas',
            isHighlightLink: false,
            label: '2 plazas'
          },
          {
            isHighlightLink: false,
            label: 'King',
            link: 'https://www.falabella.com/falabella-cl/category/cat19370004/Camas-King'
          },
          {
            label: 'Súper king',
            link: 'https://www.falabella.com/falabella-cl/category/cat19370016/Camas-Super-King',
            isHighlightLink: false
          }
        ],
        label: 'Camas',
        link: 'https://www.falabella.com/falabella-cl/category/cat13720010/Camas'
      },
      {
        label: 'Tipos de cama',
        leafCategories: [
          {
            label: 'Ver todo',
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat670091/Tipos-de-Cama'
          },
          {
            isHighlightLink: false,
            label: 'Box spring',
            link: 'https://www.falabella.com/falabella-cl/category/cat13720010/Camas?facetSelected=true&f.product.attribute.Tipo=Box+Spring',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            label: 'Camarotes',
            link: 'https://www.falabella.com/falabella-cl/category/cat3049/Camarotes'
          },
          {
            label: 'Camas americanas',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat13720010/Camas?facetSelected=true&f.product.attribute.Tipo=Cama+Americana%3A%3ACama+Americanas%3A%3ACama+americana%3A%3ACamas+Americana%3A%3ACamas+Americanas%3A%3ACamas+americanas'
          },
          {
            label: 'Camas europeas',
            link: 'https://www.falabella.com/falabella-cl/category/cat13720010/Camas?facetSelected=true&f.product.attribute.Tipo=Cama+Europea%3A%3ACama+europea%3A%3ACamas+Europea%3A%3ACamas+Europeas',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2029/Camas-Nido',
            label: 'Camas nido'
          },
          {
            isHighlightLink: false,
            label: 'Marquesas y bases',
            link: 'https://www.falabella.com/falabella-cl/category/cat8690021/Marquesas-y-Bases-de-Cama'
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/category/cat670091/Tipos-de-Cama'
      },
      {
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat2020/Colchones',
            label: 'Ver todo',
            isHighlightLink: true
          },
          {
            isHighlightLink: false,
            label: '1 plaza',
            link: 'https://www.falabella.com/falabella-cl/category/cat90008/Colchon-1-Plaza'
          },
          {
            label: '1,5 plazas',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat90013/Colchon-1-5-Plazas'
          },
          {
            label: '2 plazas',
            link: 'https://www.falabella.com/falabella-cl/category/cat90010/Colchon-2-Plazas',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat19370014/Colchon-King',
            label: 'King'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat19370017/Colchon-Super-King',
            isHighlightLink: false,
            label: 'Súper king'
          }
        ],
        label: 'Colchones',
        link: 'https://www.falabella.com/falabella-cl/category/cat2020/Colchones'
      },
      {
        label: 'Ropa de cama',
        link: 'https://www.falabella.com/falabella-cl/category/cat2073/Ropa-de-Cama',
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat2073/Ropa-de-Cama',
            isHighlightLink: true,
            label: 'Ver todo'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat13790009/Almohadas-y-fundas',
            label: 'Almohadas y fundas'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3067/Cojines-y-Pieceras',
            isHighlightLink: false,
            label: 'Cojines y pieceras'
          },
          {
            isHighlightLink: false,
            label: 'Cubrecamas y quilts',
            link: 'https://www.falabella.com/falabella-cl/category/cat3080/Cubrecamas-y-Quilts'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3194/Cubrecolchon',
            label: 'Cubre colchón y toppers',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            label: 'Faldones',
            link: 'https://www.falabella.com/falabella-cl/category/cat2073/Ropa-de-Cama?facetSelected=true&f.product.attribute.Tipo=Faldones',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            label: 'Frazadas y mantas',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3098/Frazadas-y-Mantas'
          },
          {
            label: 'Fundas plumón',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3104/Funda-plumon'
          },
          {
            label: 'Juegos de cama',
            link: 'https://www.falabella.com/falabella-cl/category/cat3122/Juegos-de-Cama',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3185/Plumones',
            label: 'Plumones',
            isHighlightLink: false
          },
          {
            label: 'Ropa de cama infantil',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat4210002/Ropa-de-cama-infantil'
          },
          {
            isHighlightLink: false,
            label: 'Sábanas',
            link: 'https://www.falabella.com/falabella-cl/category/cat3215/Sabanas'
          }
        ]
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat5870049/Muebles-de-Dormitorio',
        leafCategories: [
          {
            isHighlightLink: true,
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat5870049/Muebles-de-Dormitorio'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3063/Closet',
            label: 'Clóset'
          },
          {
            label: 'Cómodas y cajoneras',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3074/Comodas-y-Cajoneras'
          },
          {
            meatStickerOption: 0,
            label: 'Organización dormitorio',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat8950028/Organizacion-de-Dormitorio'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3212/Respaldos-y-Veladores',
            isHighlightLink: false,
            label: 'Respaldos y veladores'
          }
        ],
        label: 'Muebles de dormitorio'
      },
      {
        leafCategories: [
          {
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat3180019/Dormitorio-Infantil',
            label: 'Ver todo'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3180021/Camas-infantiles',
            isHighlightLink: false,
            label: 'Camas infantiles'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat4034/Comodas-y-mudadores',
            label: 'Cómodas y mudadores'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat4038/Cunas',
            label: 'Cunas',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3180019/Dormitorio-Infantil',
            label: 'Dormitorio bebé'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat4210002/Ropa-de-cama-infantil',
            label: 'Ropa de cama infantil',
            isHighlightLink: false
          },
          {
            label: 'Ropa de cuna ',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat4086/Ropa-de-cuna'
          }
        ],
        label: 'Dormitorio de niños',
        link: 'https://www.falabella.com/falabella-cl/category/cat3180019/Dormitorio-Infantil'
      },
      {
        verTodoActive: true,
        label: 'Complementos cama',
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/collection/complementos-cama',
            isHighlightLink: true,
            label: 'Ver todo'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat13790009/Almohadas-y-fundas',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Almohadas y fundas'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3194/Cubrecolchon',
            label: 'Cubre colchón y toppers',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3185/Plumones',
            label: 'Plumones',
            isHighlightLink: false
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/collection/complementos-cama'
      },
      {
        label: 'Combos de dormitorio',
        leafCategories: [
          {
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat10546441/Combos-de-Dormitorio',
            label: 'Ver todo'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat10546441/Combos-de-Dormitorio?f.product.attribute.Tama%C3%B1o=1+plaza',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: '1 plaza'
          },
          {
            isHighlightLink: false,
            label: '1,5 plazas',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat10546441/Combos-de-Dormitorio?f.product.attribute.Tama%C3%B1o=1.5+plazas'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat10546441/Combos-de-Dormitorio?f.product.attribute.Tama%C3%B1o=2+plazas',
            isHighlightLink: false,
            label: '2 plazas'
          },
          {
            isHighlightLink: false,
            label: 'King',
            link: 'https://www.falabella.com/falabella-cl/category/cat10546441/Combos-de-Dormitorio?f.product.attribute.Tama%C3%B1o=King',
            meatStickerOption: 0
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat670091/Tipos-de-Cama?f.product.attribute.Tama%C3%B1o=Super+King',
            isHighlightLink: false,
            label: 'Súper king',
            meatStickerOption: 0
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/category/cat10546441/Combos-de-Dormitorio',
        verTodoActive: true
      },
      {
        label: 'Marcas destacadas',
        verTodoActive: true,
        leafCategories: [
          {
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat1005/Dormitorio?facetSelected=true&f.product.brandName=cannon%3A%3Acelta%3A%3Acic%3A%3Adrimkip%3A%3Adr%C3%B6m%3A%3Aflex%3A%3Arosen%3A%3Atempur',
            label: 'Ver todo'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=&f.product.brandName=CANNON',
            label: 'Cannon',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Celta',
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=&f.product.brandName=CELTA'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'CIC',
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=&f.product.brandName=CIC'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=&f.product.brandName=DRIMKIP',
            meatStickerOption: 0,
            label: 'Drimkip'
          },
          {
            isHighlightLink: false,
            label: 'Dröm',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=&f.product.brandName=Dr%C3%B6m'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=&f.product.brandName=FLEX',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Flex'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=&f.product.brandName=ROSEN',
            label: 'Rosen'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=&f.product.brandName=SIMMONS',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Simmons'
          },
          {
            label: 'Tempur',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=&f.product.brandName=TEMPUR'
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/category/cat1005/Dormitorio?facetSelected=true&f.product.brandName=cannon%3A%3Acelta%3A%3Acic%3A%3Adrimkip%3A%3Adr%C3%B6m%3A%3Aflex%3A%3Arosen%3A%3Atempur'
      }
    ]
  },
  {
    subCategories: [
      {
        label: 'Cocina',
        leafCategories: [
          {
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/CATG10000/Cocina',
            isHighlightLink: true
          },
          {
            label: 'Muebles de cocina',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/CATG10014/Muebles-de-Cocina',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/CATG10013/Lavaplatos',
            label: 'Lavaplatos',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            label: 'Grifería cocina',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/CATG10017/Griferia-de-Cocina-y-Lavadero?&f.product.L2_category_paths=cat01%7C%7CCocina+y+Ba%C3%B1o%2FCATG10002%7C%7CGrifer%C3%ADas%2FCATG10017%7C%7CGrifer%C3%ADa+de+Cocina+y+Lavadero'
          },
          {
            label: 'Hornos empotrables',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat4054/Hornos-Empotrables',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'Cocinas a gas',
            link: 'https://www.falabella.com/falabella-cl/category/cat70012/Cocinas-a-gas',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            label: 'Campanas',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat4026/Campanas'
          },
          {
            label: 'Encimeras',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat4045/Encimeras'
          },
          {
            label: 'Lavavajillas',
            link: 'https://www.falabella.com/falabella-cl/category/cat4061/Lavavajillas',
            meatStickerOption: 0,
            isHighlightLink: false
          }
        ],
        verTodoActive: true,
        link: 'https://www.falabella.com/falabella-cl/category/CATG10000/Cocina'
      },
      {
        verTodoActive: true,
        label: 'Electrodomésticos cocina',
        link: 'https://www.falabella.com/falabella-cl/category/cat2034/Electrodomesticos-Cocina?isLanding=true',
        leafCategories: [
          {
            isHighlightLink: true,
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat2034/Electrodomesticos-Cocina'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat9890052/Batidoras',
            meatStickerOption: 0,
            label: 'Batidoras'
          },
          {
            label: 'Cafeteras eléctricas',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3045/Cafeteras-electricas',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat6740089/Cocina-Entretenida',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Cocina entretenida'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3099/Freidoras',
            label: 'Freidoras',
            meatStickerOption: 0
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3110/Hervidores',
            label: 'Hervidores',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Hornos eléctricos',
            link: 'https://www.falabella.com/falabella-cl/category/cat3114/Hornos-Electricos'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3141/Licuadoras',
            isHighlightLink: false,
            label: 'Licuadoras',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3151/Microondas',
            meatStickerOption: 0,
            label: 'Microondas'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Ollas multifuncionales',
            link: 'https://www.falabella.com/falabella-cl/category/cat7230035/Ollas-multiuso-y-arroceras'
          },
          {
            label: 'Robot de cocina',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat8410002/Robot-de-Cocina'
          }
        ]
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat2060/Menaje-Cocina',
        label: 'Menaje cocina',
        verTodoActive: true,
        leafCategories: [
          {
            isHighlightLink: true,
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat2060/Menaje-Cocina'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat2060/Menaje-Cocina?facetSelected=true&f.product.brandName=3+claveles%3A%3Aberlinger+haus%3A%3Acrate+%26+barrel%3A%3Ajohn+lewis%3A%3Ajoseph+joseph%3A%3Akorkmaz%3A%3Ale+creuset%3A%3Aoxo%3A%3Arachael+ray%3A%3Aswiss+nature+labs',
            isHighlightLink: false,
            label: 'MARCAS PREMIUM',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Baterías de cocina',
            link: 'https://www.falabella.com/falabella-cl/category/cat3034/Baterias-de-Cocina'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat5430047/Contenedores',
            label: 'Conservadores y frascos',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            label: 'Cuchillos y tablas',
            link: 'https://www.falabella.com/falabella-cl/category/cat7280093/Cuchillos-y-tablas',
            meatStickerOption: 0
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3169/Ollas',
            meatStickerOption: 0,
            label: 'Ollas',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat8950026/Organizacion-de-Cocina',
            meatStickerOption: 0,
            label: 'Organización'
          },
          {
            label: 'Repostería',
            link: 'https://www.falabella.com/falabella-cl/category/cat7280016/Reposteria',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3220/Sartenes-y-Wok',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Sartenes y wok'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat970004/Termos-y-Picnic',
            label: 'Termos y picnic',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            label: 'Textil cocina',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat14600003/Textil-Cocina'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3024/Utensilios-de-cocina',
            label: 'Utensilios de cocina',
            meatStickerOption: 0
          }
        ]
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat2061/Menaje-Comedor',
        verTodoActive: true,
        label: 'Menaje comedor',
        leafCategories: [
          {
            isHighlightLink: true,
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat2061/Menaje-Comedor'
          },
          {
            label: 'MARCAS PREMIUM',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2061/Menaje-Comedor?facetSelected=true&f.product.brandName=3+claveles%3A%3Aanthropologie+home%3A%3Abordallo%3A%3Acosta+nova%3A%3Acrate+%26+barrel%3A%3Adalper%3A%3Aidurgo%3A%3Ajohn+lewis%3A%3Ajoseph+joseph%3A%3Aoxo%3A%3Apip+studio%3A%3Aqdigital%3A%3Awmf%3A%3Awrzesniak',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            label: 'Accesorios de mesa',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3012/Accesorios-de-mesa'
          },
          {
            isHighlightLink: false,
            label: 'Aperitivo y bar',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat7280098/Aperitivo-y-bar'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3081/Cuchilleria',
            label: 'Cuchillería',
            meatStickerOption: 0
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3100/Fuentes-y-Bandejas',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Fuentes y bandejas'
          },
          {
            label: 'Mantelería',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat7280099/Manteleria'
          },
          {
            label: 'Té y café',
            link: 'https://www.falabella.com/falabella-cl/category/cat7280097/Te-y-cafe',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Vajilla',
            link: 'https://www.falabella.com/falabella-cl/category/cat7280100/Vajilla'
          },
          {
            isHighlightLink: false,
            label: 'Vasos y copas',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat6590032/Vasos-y-copas'
          }
        ]
      },
      {
        label: 'Refrigeradores',
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3205/Refrigeradores',
            isHighlightLink: true,
            label: 'Ver todo'
          },
          {
            label: 'Cavas',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat1840004/Cavas'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3205/Refrigeradores?f.product.attribute.Tipo=Bottom+freezer%3A%3AMonopuerta%3A%3ATop+mount&isPLP=1%2C1%2C1%2C1',
            meatStickerOption: 0,
            label: 'Convencionales'
          },
          {
            label: 'Freezers',
            link: 'https://www.falabella.com/falabella-cl/category/cat4048/Freezers',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat4049/Frigobar',
            label: 'Frigobar'
          },
          {
            label: 'Side by Side',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat4091/Side-by-Side'
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/category/cat3205/Refrigeradores',
        verTodoActive: true
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/CATG10001/Bano',
        label: 'Baño',
        verTodoActive: true,
        leafCategories: [
          {
            label: 'Ver todo',
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/CATG10001/Bano'
          },
          {
            isHighlightLink: false,
            label: 'Duchas y cabinas',
            link: 'https://www.falabella.com/falabella-cl/category/CATG10023/Duchas-y-Cabinas',
            meatStickerOption: 0
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/CATG10018/Griferia-para-Bano',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Grifería Baño'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Lavamanos y vanitorios',
            link: 'https://www.falabella.com/falabella-cl/category/CATG10022/Lavamanos-y-Vanitorios'
          },
          {
            isHighlightLink: false,
            label: 'Seguridad y mov. reducida',
            link: 'https://www.falabella.com/falabella-cl/category/CATG10028/Seguridad-Bano-y-Movilidad-Reducida',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            label: 'Tinas e hidromasaje',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/CATG10024/Tinas-e-Hidromasajes'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/CATG10020/WC---Sanitarios',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'WC - Sanitarios'
          }
        ]
      },
      {
        verTodoActive: true,
        leafCategories: [
          {
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat2013/Calefont-y-Termos',
            isHighlightLink: true
          },
          {
            label: 'Accesorios',
            link: 'https://www.falabella.com/falabella-cl/category/CATG10182/Accesorios-Calefont?sortBy=derived.price.search%2Casc&facetSelected=true&f.product.L3_category_paths=cat16510006%7C%7CElectrohogar%2Fcat7170003%7C%7CCalefacci%C3%B3n%2Fcat2013%7C%7CCalefont+y+Termos%2FCATG10182%7C%7CAccesorios+Calefont',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Calefont tiro forzado',
            link: 'https://www.falabella.com/falabella-cl/category/cat2013/Calefont-y-Termos?facetSelected=true&f.product.attribute.Tiro=Forzado'
          },
          {
            meatStickerOption: 0,
            label: 'Calefont tiro natural',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2013/Calefont-y-Termos?facetSelected=true&f.product.attribute.Tiro=Natural'
          },
          {
            meatStickerOption: 0,
            label: 'Termos y calderas',
            link: 'https://www.falabella.com/falabella-cl/category/CATG10181/Termos-y-Calderas?facetSelected=true&f.product.L3_category_paths=cat16510006%7C%7CElectrohogar%2Fcat7170003%7C%7CCalefacci%C3%B3n%2Fcat2013%7C%7CCalefont+y+Termos%2FCATG10181%7C%7CTermos+y+Calderas&f.product.attribute.Tipo=Caldera%3A%3ACaldera+Pre-Mix+Monoblock%3A%3ACalderas%3A%3ATermo%3A%3ATermos%3A%3ATermotanques',
            isHighlightLink: false
          }
        ],
        label: 'Calefont y termos',
        link: 'https://www.falabella.com/falabella-cl/category/cat2013/Calefont-y-Termos'
      },
      {
        verTodoActive: true,
        link: 'https://www.falabella.com/falabella-cl/category/cat2006/Menaje-Bano',
        label: 'Complementos de baño',
        leafCategories: [
          {
            isHighlightLink: true,
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat2006/Menaje-Bano'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3010/Accesorios-de-Bano',
            isHighlightLink: false,
            label: 'Accesorios',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            label: 'Basureros y papeleros',
            link: 'https://www.falabella.com/falabella-cl/category/cat11390001/Basureros-y-Papeleros',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            label: 'Tapas y asientos WC',
            link: 'https://www.falabella.com/falabella-cl/category/CATG10027/Tapas-y-asientos-para-WC',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Organización',
            link: 'https://www.falabella.com/falabella-cl/category/CATG10169/Organizacion-de-Bano'
          },
          {
            meatStickerOption: 0,
            label: 'Textil baño',
            link: 'https://www.falabella.com/falabella-cl/category/CATG10030/Textil-bano',
            isHighlightLink: false
          }
        ]
      }
    ],
    label: 'Cocina y baño',
    link: 'cat01',
    bannerLocation: 'urlToImage'
  },
  {
    link: 'cat7330051',
    label: 'Mujer',
    subCategories: [
      {
        leafCategories: [
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'INSPÍRATE CON MODA F',
            link: 'https://tienda.falabella.com/falabella-cl/page/moda-f'
          },
          {
            link: 'https://tienda.falabella.com/falabella-cl/page/marcas-premium',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Marcas premium'
          },
          {
            isHighlightLink: false,
            label: 'SNEAKER CORNER',
            meatStickerOption: 0,
            link: 'https://tienda.falabella.com/falabella-cl/page/zapatillas'
          }
        ],
        link: 'https://tienda.falabella.com/falabella-cl/page/moda-f',
        label: 'Tendencias - Inspiración',
        verTodoActive: true
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat11670003/Especiales',
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/collection/mundo-senora',
            label: 'Mundo señora',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/Mundo-Surf',
            label: 'Mundo surf',
            meatStickerOption: 0
          },
          {
            label: 'Ropa de fiesta',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat14330023/Vestidos-y-enteritos?&f.product.attribute.Estilo=Fiesta',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat7350021/Ropa-Maternal',
            label: 'Ropa maternal'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/collection/Tallas-Grandes-Mujer',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Tallas grandes'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Diseño local',
            link: 'https://www.falabella.com/falabella-cl/collection/diseno-local'
          },
          {
            label: 'Ropa + sustentable',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat17790018/Mujer-mas-sustentable',
            isHighlightLink: false
          }
        ],
        label: 'Segmentos y ocasiones',
        verTodoActive: true
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat12440001/Zapatos',
        label: 'Zapatos',
        leafCategories: [
          {
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat12440001/Zapatos',
            isHighlightLink: true
          },
          {
            meatStickerOption: 0,
            label: 'NUEVA TEMPORADA',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/zapatos-mujer-nueva-temporada'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat16630007/Pantuflas-mujer',
            meatStickerOption: 0,
            label: 'Pantuflas',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat690247/Sandalias',
            label: 'Sandalias'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat850068/Zapatillas',
            label: 'Zapatillas',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'Zapatos',
            link: 'https://www.falabella.com/falabella-cl/category/cat12440001/Zapatos?f.product.attribute.Tipo=Alpargatas%3A%3ABallerinas%3A%3AMocasines%3A%3AZapatos+casuales%3A%3AZapatos+formales&facetSelected=true&facetSelected=true%2Ctrue%2Ctrue%2Ctrue',
            meatStickerOption: 0
          }
        ],
        verTodoActive: true
      },
      {
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat20002/Moda-Mujer',
            label: 'Ver todo',
            isHighlightLink: true
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/Nueva-Temporada-Moda-Mujer',
            isHighlightLink: false,
            label: 'NUEVA TEMPORADA'
          },
          {
            label: 'Blusas y poleras',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat850030/Blusas-y-Poleras-Mujer',
            isHighlightLink: false
          },
          {
            label: 'Buzos y calzas',
            link: 'https://www.falabella.com/falabella-cl/category/cat20002/Moda-Mujer?facetSelected=true&f.product.attribute.Tipo=Buzos%3A%3ACalzas%3A%3ALeggins',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat850036/Abrigos-y-Chaquetas',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Chaquetas y parkas'
          },
          {
            label: 'Faldas',
            link: 'https://www.falabella.com/falabella-cl/category/cat850042/Faldas',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat850040/Pantalones?facetSelected=true&f.product.attribute.Tipo=Jeans',
            isHighlightLink: false,
            label: 'Jeans'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat850036/Abrigos-y-Chaquetas?facetSelected=true&f.product.attribute.Tipo=Kimonos',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Kimonos'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/collection/packs-mujer',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Packs'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Pantalones',
            link: 'https://www.falabella.com/falabella-cl/category/cat850040/Pantalones'
          },
          {
            meatStickerOption: 0,
            label: 'Polerones y chiporros',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat7350055/Polerones-Mujer'
          },
          {
            isHighlightLink: false,
            label: 'Shorts',
            link: 'https://www.falabella.com/falabella-cl/category/cat850044/Shorts',
            meatStickerOption: 0
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat850034/Sweaters-y-chalecos',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Sweaters y chalecos'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Trajes de baño',
            link: 'https://www.falabella.com/falabella-cl/category/cat11610008/Trajes-de-bano-y-bikinis'
          },
          {
            label: 'Vestidos y enteritos',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat14330023/Vestidos-y-enteritos'
          }
        ],
        label: 'Ropa',
        verTodoActive: true,
        link: 'https://www.falabella.com/falabella-cl/category/cat20002/Moda-Mujer'
      },
      {
        leafCategories: [
          {
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-ropa-interior-mujer',
            isHighlightLink: true
          },
          {
            label: 'NUEVA TEMPORADA',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/lo-nuevo-en-ropa-interior-mujer'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat7350018/Calzones',
            label: 'Calzones',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat7350012/Fajas-y-Modeladores',
            meatStickerOption: 0,
            label: 'Fajas y modeladores',
            isHighlightLink: false
          },
          {
            label: 'Pack ropa interior',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat13990009/Pack-Ropa-Interior'
          },
          {
            label: 'Pantuflas y accesorios',
            link: 'https://www.falabella.com/falabella-cl/category/cat16630007/Pantuflas-mujer',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            label: 'Pantys y calcetines',
            link: 'https://www.falabella.com/falabella-cl/category/cat7350013/Pantys-y-Calcetines',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Pijamas',
            link: 'https://www.falabella.com/falabella-cl/collection/pijamas-mujer'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat7350019/Sostenes',
            meatStickerOption: 0,
            label: 'Sostenes',
            isHighlightLink: false
          }
        ],
        verTodoActive: true,
        link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-ropa-interior-mujer',
        label: 'Ropa interior y pijamas'
      },
      {
        link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-accesorios?facetSelected=true&f.product.attribute.G%C3%A9nero=Mujer%3A%3AUnisex',
        label: 'Accesorios',
        verTodoActive: true,
        leafCategories: [
          {
            label: 'Ver todo',
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-accesorios?facetSelected=true&f.product.attribute.G%C3%A9nero=Mujer%3A%3AUnisex'
          },
          {
            label: 'Billeteras',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3038/Billeteras',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat7480104/Carteras-y-mochilas',
            meatStickerOption: 0,
            label: 'Carteras'
          },
          {
            isHighlightLink: false,
            label: 'Cinturones',
            link: 'https://www.falabella.com/falabella-cl/category/cat3062/Cinturones',
            meatStickerOption: 0
          },
          {
            label: 'Joyas',
            link: 'https://www.falabella.com/falabella-cl/category/cat590028/Joyas',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            label: 'Lentes',
            link: 'https://www.falabella.com/falabella-cl/category/cat7480074/Anteojos?facetSelected=true&f.product.attribute.G%C3%A9nero=Mujer%3A%3AUnisex&facetSelected=true',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            label: 'Lentes ópticos',
            link: 'https://www.falabella.com/falabella-cl/category/cat14864567/Anteojos-Opticos?facetSelected=true&f.product.attribute.G%C3%A9nero=Mujer%3A%3AUnisex',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            label: 'Mascarillas',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/mascarillas-falabella',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat5880085/Mochilas',
            label: 'Mochilas'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Pañuelos y bufandas',
            link: 'https://www.falabella.com/falabella-cl/category/cat2017/Accesorios-Mujer?mkid=PL_HU_VER_1000012412&facetSelected=true&f.product.attribute.Tipo=Bufandas::Pa%C3%B1uelos'
          },
          {
            meatStickerOption: 0,
            label: 'Relojes',
            link: 'https://www.falabella.com/falabella-cl/category/cat12160001/Relojes-Mujer',
            isHighlightLink: false
          }
        ]
      },
      {
        link: 'https://tienda.falabella.com/falabella-cl/page/marcas-destacadas',
        leafCategories: [
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Marcas de la A a la Z',
            link: 'https://tienda.falabella.com/falabella-cl/page/marcas-destacadas'
          },
          {
            meatStickerOption: 0,
            label: 'Aldo',
            link: 'https://tienda.falabella.com/falabella-cl/page/aldo',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/collection/Americanino-Mujer',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Americanino'
          },
          {
            meatStickerOption: 0,
            link: 'https://tienda.falabella.com/falabella-cl/page/basement',
            isHighlightLink: false,
            label: 'Basement'
          },
          {
            link: 'https://tienda.falabella.com/falabella-cl/page/calzedonia',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Calzedonia'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/desigual',
            label: 'Desigual'
          },
          {
            label: 'ELLE',
            link: 'https://www.falabella.com/falabella-cl/collection/Elle-mujer',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/Esprit-mujer',
            meatStickerOption: 0,
            label: 'Esprit'
          },
          {
            meatStickerOption: 0,
            label: 'Etam',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/etam'
          },
          {
            meatStickerOption: 0,
            label: 'GAP',
            link: 'https://www.falabella.com/falabella-cl/collection/Gap-mujer',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'Guess',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/guess'
          },
          {
            meatStickerOption: 0,
            label: 'Lounge',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/landing-lounge?sred=lounge'
          },
          {
            meatStickerOption: 0,
            label: 'Mango',
            isHighlightLink: false,
            link: 'https://tienda.falabella.com/falabella-cl/page/mango'
          },
          {
            label: 'Mossimo',
            link: 'https://www.falabella.com/falabella-cl/collection/Mossimo-Mujer',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Pandora',
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-pandora?sred=pandora'
          },
          {
            meatStickerOption: 0,
            label: 'Rapsodia',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/Rapsodia-mujer'
          },
          {
            isHighlightLink: false,
            link: 'https://tienda.falabella.com/falabella-cl/page/superdry',
            meatStickerOption: 0,
            label: 'Superdry'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/sybilla-moda-mujer',
            meatStickerOption: 0,
            label: 'Sybilla'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/tous?sred=tous',
            isHighlightLink: false,
            label: 'Tous'
          },
          {
            isHighlightLink: false,
            label: 'University Club',
            link: 'https://www.falabella.com/falabella-cl/collection/University-Club-mujer',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/vero-moda',
            isHighlightLink: false,
            label: 'Vero Moda'
          }
        ],
        verTodoActive: true,
        label: 'Marcas destacadas'
      }
    ]
  },
  {
    link: 'cat7450065',
    subCategories: [
      {
        label: 'Tendencias - Inspiración',
        link: 'https://tienda.falabella.com/falabella-cl/page/moda-hombre',
        verTodoActive: true,
        leafCategories: [
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'INSPÍRATE CON MODA F',
            link: 'https://tienda.falabella.com/falabella-cl/page/moda-hombre'
          },
          {
            isHighlightLink: false,
            link: 'https://tienda.falabella.com/falabella-cl/page/urbano-hombre',
            meatStickerOption: 0,
            label: 'URBAN STORE'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/marcas-surferas',
            meatStickerOption: 0,
            label: 'Mundo surf'
          },
          {
            meatStickerOption: 0,
            label: 'Tallas grandes',
            link: 'https://www.falabella.com/falabella-cl/category/cat1320008/Moda-Hombre?f.variant.attribute.size=xxl%3A%3Axxxl',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/-2bverde-hombre',
            label: 'Ropa + sustentable',
            meatStickerOption: 0
          },
          {
            link: 'https://tienda.falabella.com/falabella-cl/page/zapatillas',
            isHighlightLink: false,
            label: 'SNEAKER CORNER',
            meatStickerOption: 0
          }
        ]
      },
      {
        verTodoActive: true,
        label: 'Zapatos',
        link: 'https://www.falabella.com/falabella-cl/category/cat1720006/Zapatos',
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat1720006/Zapatos',
            isHighlightLink: true,
            label: 'Ver todo'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/zapatos-hombre-nueva-coleccion',
            label: 'NUEVA TEMPORADA'
          },
          {
            label: 'Pantuflas',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat7450089/Pantuflas-hombre',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat1720006/Zapatos?facetSelected=true&f.product.attribute.Tipo=Sandalias',
            label: 'Sandalias y hawaianas'
          },
          {
            isHighlightLink: false,
            label: 'Zapatillas',
            link: 'https://www.falabella.com/falabella-cl/category/cat850064/Zapatillas',
            meatStickerOption: 0
          },
          {
            label: 'Zapatos',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat1720008/Zapatos-hombre',
            meatStickerOption: 0
          }
        ]
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat5260002/Ropa-interior',
        label: 'Ropa interior y pijamas',
        leafCategories: [
          {
            label: 'Ver todo',
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat5260002/Ropa-interior'
          },
          {
            label: 'Boxers y calzoncillos',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat7450083/Boxers',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat7450088/Calcetines',
            meatStickerOption: 0,
            label: 'Calcetines',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'Pijamas y camisetas',
            link: 'https://www.falabella.com/falabella-cl/category/cat7450086/Pijamas',
            meatStickerOption: 0
          }
        ],
        verTodoActive: true
      },
      {
        verTodoActive: true,
        leafCategories: [
          {
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat1320008/Moda-Hombre',
            isHighlightLink: true
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/Nueva-temporada-Moda-Hombre',
            isHighlightLink: false,
            label: 'NUEVA TEMPORADA'
          },
          {
            label: 'Camisas formales',
            link: 'https://www.falabella.com/falabella-cl/category/cat7450067/Camisas-de-Vestir',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            label: 'Camisas y guayaberas',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat18380042/Camisas'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat1320012/Chalecos-y-Sweaters',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Chalecos y sweaters'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat16580002/Chaquetas-y-abrigos',
            isHighlightLink: false,
            label: 'Chaquetas y parkas',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat7450073/Jeans-Hombre',
            label: 'Jeans'
          },
          {
            label: 'Pantalones y buzos',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat17980009/Pantalones-Hombre?f.product.attribute.G%C3%A9nero=Hombre&facetSelected=true'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat1320010/Poleras?f.product.attribute.G%C3%A9nero=Hombre&facetSelected=true',
            label: 'Poleras'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Polerones y polar',
            link: 'https://www.falabella.com/falabella-cl/category/cat7500027/Polerones-y-Polar'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat13620025/Shorts-y-Bermudas',
            label: 'Shorts y bermudas',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat7450062/Trajes-de-Bano',
            label: 'Trajes de baño',
            isHighlightLink: false
          },
          {
            label: 'Trajes formales',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat1640033/Trajes-formales'
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/category/cat1320008/Moda-Hombre',
        label: 'Ropa'
      },
      {
        label: 'Ropa deportiva',
        link: 'https://www.falabella.com/falabella-cl/category/cat6930003/Ropa-deportiva-hombre',
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat6930003/Ropa-deportiva-hombre',
            label: 'Ver todo',
            isHighlightLink: true
          },
          {
            label: 'Accesorios',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/Accesorios-Deportivos?f.product.attribute.G%C3%A9nero=Hombre%3A%3AUnisex',
            meatStickerOption: 0
          },
          {
            label: 'Bolsos deportivos',
            link: 'https://www.falabella.com/falabella-cl/category/cat6930147/Bolsos-deportivos',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat190004/Camisetas-Oficiales',
            label: 'Camisetas de fútbol',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'Pantalones y joggers',
            link: 'https://www.falabella.com/falabella-cl/category/cat6930019/Pantalones-y-Joggers',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat12650004/Parkas-y-Cortavientos',
            label: 'Parkas y cortavientos'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat6930011/Poleras',
            label: 'Poleras',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat6930014/Polerones',
            label: 'Polerones'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat6930018/Shorts',
            meatStickerOption: 0,
            label: 'Shorts',
            isHighlightLink: false
          }
        ],
        verTodoActive: true
      },
      {
        leafCategories: [
          {
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-accesorios?facetSelected=true&f.product.attribute.G%C3%A9nero=Hombre%3A%3AUnisex',
            label: 'Ver todo'
          },
          {
            label: 'Billeteras',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat6050006/Billeteras',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            label: 'Bufandas y Pañuelos',
            link: 'https://www.falabella.com/falabella-cl/category/cat6050009/Bufandas-y-Panuelos',
            isHighlightLink: false
          },
          {
            label: 'Cinturones',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat6050005/Cinturones',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat1320018/Corbatas-y-Humitas',
            label: 'Corbatas y humitas'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Jockeys y gorros',
            link: 'https://www.falabella.com/falabella-cl/category/cat6050008/Jockeys-y-Gorros'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat7480074/Anteojos?facetSelected=true&f.product.attribute.G%C3%A9nero=Hombre%3A%3AUnisex&facetSelected=true%2Ctrue',
            isHighlightLink: false,
            label: 'Lentes'
          },
          {
            isHighlightLink: false,
            label: 'Mochilas y bolsos',
            link: 'https://www.falabella.com/falabella-cl/category/cat6050007/Bolsos-y-Mochilas',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3207/Relojes-hombre',
            isHighlightLink: false,
            label: 'Relojes'
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-accesorios?facetSelected=true&f.product.attribute.G%C3%A9nero=Hombre%3A%3AUnisex',
        verTodoActive: true,
        label: 'Accesorios'
      },
      {
        label: 'Cuidado personal',
        link: 'https://www.falabella.com/falabella-cl/category/cat7660002/Belleza?facetSelected=true&f.product.attribute.G%C3%A9nero=Hombre',
        leafCategories: [
          {
            isHighlightLink: true,
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat7660002/Belleza?facetSelected=true&f.product.attribute.G%C3%A9nero=Hombre'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Afeitadoras y cortapelos',
            link: 'https://www.falabella.com/falabella-cl/category/CATG11731/Afeitadoras-y-Cortapelos'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat7660002/Belleza?facetSelected=true&f.product.attribute.G%C3%A9nero=Hombre&f.product.attribute.Tipo=Aceites+capilares%3A%3AAcondicionadores%3A%3AAftershave%3A%3AControl+ca%C3%ADda%3A%3ACremas+de+masaje+capilar%3A%3ACremas+para+peinar%3A%3AOtros+accesorios+de+belleza%3A%3APreparados+de+afeitar%3A%3ASet+de+tratamiento+capilar%3A%3AShampoos%3A%3ASprays+capilares%3A%3ATratamientos+capilares',
            label: 'Cuidado capilar'
          },
          {
            isHighlightLink: false,
            label: 'Perfumes hombre',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3112/Perfumes-Hombre'
          },
          {
            isHighlightLink: false,
            label: 'Tratamiento',
            link: 'https://www.falabella.com/falabella-cl/category/cat3214/Cuidado-del-Rostro?facetSelected=true&f.product.attribute.G%C3%A9nero=Hombre',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            label: 'Vitaminas y suplementos',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat11342105/Vitaminas-y-suplementos'
          }
        ],
        verTodoActive: true
      },
      {
        label: 'Marcas destacadas',
        link: 'https://www.falabella.com/falabella-cl/category/cat5420004/Marcas--Destacadas',
        verTodoActive: true,
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/collection/nba-hombre',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'NBA'
          },
          {
            label: 'Adidas',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/adidas'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/collection/Americanino-Hombre',
            label: 'Americanino',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat1320008/Moda-Hombre?f.product.brandName=basement',
            label: 'Basement',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat5260002/Ropa-interior?facetSelected=true&f.product.brandName=benetton',
            label: 'Benetton',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Calvin Klein',
            link: 'https://www.falabella.com/falabella-cl/category/cat1320008/Moda-Hombre?f.product.brandName=calvin+klein'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3208/Relojes?mkid=PL_HU_VER_1000010543&f.product.brandName=casio',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Casio'
          },
          {
            label: 'Festina',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3208/Relojes?mkid=PL_HU_VER_1000010545&&f.product.brandName=festina',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            label: 'G Shock',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3208/Relojes?facetSelected=true&f.product.brandName=g-shock'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Lacoste',
            link: 'https://www.falabella.com/falabella-cl/category/cat1320008/Moda-Hombre?f.product.brandName=lacoste'
          },
          {
            label: 'La Martina',
            link: 'https://www.falabella.com/falabella-cl/collection/la-martina',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            label: "Levi's",
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat1320008/Moda-Hombre?f.product.brandName=levis%3A%3Alevis+negocio+especial',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/lippi',
            isHighlightLink: false,
            label: 'Lippi'
          },
          {
            label: 'Mango Man',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/mango-man',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            label: 'Marmot',
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=&f.product.brandName=Marmot',
            isHighlightLink: false
          },
          {
            label: 'Mossimo',
            link: 'https://www.falabella.com/falabella-cl/category/cat1320008/Moda-Hombre?facetSelected=true&f.product.brandName=mossimo',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            label: 'Nike',
            link: 'https://www.falabella.com/falabella-cl/category/cat6930003/Ropa-deportiva-hombre?facetSelected=true&f.product.brandName=nike',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            label: 'Tommy Hilfiger',
            link: 'https://www.falabella.com/falabella-cl/collection/tommy-hilfiger-hombre',
            isHighlightLink: false
          },
          {
            label: 'Ray Ban',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat7480074/Anteojos?facetSelected=true&f.product.brandName=ray+ban',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/superdry?facetSelected=true&f.product.attribute.G%C3%A9nero=Hombre',
            label: 'Superdry',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://tienda.falabella.com/falabella-cl/page/wolf-hank',
            label: 'Wolf&Hank'
          }
        ]
      }
    ],
    label: 'Hombre'
  },
  {
    subCategories: [
      {
        leafCategories: [
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/ropa-bebe-nina-0-a-24',
            label: '0-24 meses',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: '2-8 años',
            link: 'https://www.falabella.com/falabella-cl/collection/ropa-nina-2-a-8'
          },
          {
            label: '8-16 años',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/ropa-nina-8-a16'
          }
        ],
        verTodoActive: true,
        label: 'Ropa de niñas por edad',
        link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-ropa-nina'
      },
      {
        label: 'Ropa de niñas por tipo',
        link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-ropa-nina',
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-ropa-nina',
            label: 'Ver todo',
            isHighlightLink: true
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Calcetines y ropa interior',
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-pijamas-nina?facetSelected=true&f.product.attribute.Tipo=Calcetines%3A%3ACalcetines+de+vestir%3A%3ACalzones%3A%3ACamisetas%3A%3APack%3A%3APantys%3A%3APetos%3A%3ASostenes'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat14370004/Calzas-Ninas',
            isHighlightLink: false,
            label: 'Buzos, calzas y conjuntos',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat16400013/Sweaters-y-Polerones-Ninas',
            isHighlightLink: false,
            label: 'Chalecos y polerones'
          },
          {
            label: 'Chaquetas y parkas',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat17010032/Chaquetas--Parkas-y-Abrigos-Ninas',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat16400014/Jeans-y-Pantalones-Ninas',
            meatStickerOption: 0,
            label: 'Jeans y pantalones'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Pijamas y pantuflas',
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-pijamas-nina?facetSelected=true&f.product.attribute.Tipo=Pantuflas%3A%3APijamas'
          },
          {
            label: 'Poleras y blusas',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-ropa-nina?facetSelected=true&f.product.attribute.Tipo=Blusas%3A%3APoleras'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Ropa deportiva',
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-ropa-deportiva-nina'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat19280007/Shorts-Ninas',
            label: 'Shorts y jardineras'
          },
          {
            meatStickerOption: 0,
            label: 'Trajes de baño y bikinis',
            link: 'https://www.falabella.com/falabella-cl/category/cat19290017/Trajes-de-Bano-y-Bikinis-Ninas',
            isHighlightLink: false
          },
          {
            label: 'Vestidos y enteritos',
            link: 'https://www.falabella.com/falabella-cl/category/cat16400015/Faldas-y-Vestidos-Ninas',
            isHighlightLink: false,
            meatStickerOption: 0
          }
        ],
        verTodoActive: true
      },
      {
        leafCategories: [
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: '0-24 meses',
            link: 'https://www.falabella.com/falabella-cl/collection/ropa-bebe-nino-0-a-24'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/ropa-nino-2-a-8',
            label: '2-8 años',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/ropa-nino-8-a-16',
            label: '8-16 años'
          }
        ],
        verTodoActive: true,
        label: 'Ropa de niños por edad',
        link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-ropa-nino'
      },
      {
        leafCategories: [
          {
            isHighlightLink: true,
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-ropa-nino'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat16400020/Buzos-y-Conjuntos-Ninos',
            isHighlightLink: false,
            label: 'Buzos y conjuntos'
          },
          {
            label: 'Calcetines y ropa interior',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-ropa-interior-y-pijamas-nino?facetSelected=true&f.product.attribute.Tipo=Bodys%3A%3ABoxers%3A%3ACalcetines%3A%3ACalcetines+de+vestir%3A%3ACalcetines+deportivos%3A%3ACalzoncillos%3A%3ACamisetas%3A%3APack'
          },
          {
            label: 'Chalecos y polerones',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat16400018/Sweaters-y-Polerones-Ninos',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            label: 'Chaquetas y parkas',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat17010033/Chaquetas--Parkas-y-Abrigos-Ninos'
          },
          {
            label: 'Jeans y pantalones',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat16400019/Jeans-y-Pantalones-Ninos',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-ropa-interior-y-pijamas-nino?facetSelected=true&f.product.attribute.Tipo=Pantuflas%3A%3APijamas',
            label: 'Pijamas y pantuflas'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-ropa-nino?facetSelected=true&f.product.attribute.Tipo=Camisas%3A%3APoleras',
            meatStickerOption: 0,
            label: 'Poleras y camisas'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Ropa deportiva',
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-ropa-deportiva-nino'
          },
          {
            isHighlightLink: false,
            label: 'Shorts',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat19280010/Shorts-Ninos'
          },
          {
            label: 'Trajes de baño',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat19290018/Trajes-de-Bano-Ninos'
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-ropa-nino',
        verTodoActive: true,
        label: 'Ropa de niños por tipo'
      },
      {
        link: 'https://www.falabella.com/falabella-cl/collection/Ver-Todo-Zapatos-Ninos',
        verTodoActive: true,
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/collection/Ver-Todo-Zapatos-Ninos',
            label: 'Ver todo',
            isHighlightLink: true
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/zapatos-ninos-nueva-coleccion',
            label: 'NUEVA TEMPORADA'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/collection/Ver-Todo-Zapatos-Ninos?f.product.attribute.Tipo=Botas%3A%3ABotas+de+lluvia%3A%3ABotines',
            label: 'Botas y botines',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            label: 'Zapatillas',
            link: 'https://www.falabella.com/falabella-cl/collection/Ver-Todo-Zapatillas-Ninos',
            meatStickerOption: 0
          }
        ],
        label: 'Zapatos'
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat14680031/Juguetes',
        label: 'Juguetería',
        leafCategories: [
          {
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat14680031/Juguetes',
            label: 'Ver todo'
          },
          {
            isHighlightLink: false,
            label: 'Disfraces',
            link: 'https://www.falabella.com/falabella-cl/category/cat210002/Disfraces',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Figuras coleccionables',
            link: 'https://www.falabella.com/falabella-cl/category/cat690299/Figuras-de-accion-y-coleccionables'
          },
          {
            label: 'Juegos de mesa',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat3127/Juegos-de-Mesa',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            label: 'Juegos de rol',
            link: 'https://www.falabella.com/falabella-cl/category/cat6980031/Juegos-de-Rol',
            isHighlightLink: false
          },
          {
            label: 'Juguetes bebé',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat4059/Juguetes-bebe'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat90080/Juguetes-Didacticos',
            label: 'Juguetes didácticos'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat430005/Lego-y-Armables',
            label: 'LEGO y armables',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat90074/Munecas-y-Accesorios',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Muñecas y accesorios'
          },
          {
            label: 'Peluches',
            link: 'https://www.falabella.com/falabella-cl/category/cat90075/Peluches',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat90083/Pistolas-de-Juguete',
            meatStickerOption: 0,
            label: 'Pistolas de juguete',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Puzzles',
            link: 'https://www.falabella.com/falabella-cl/category/cat8860003/Puzzles'
          },
          {
            isHighlightLink: false,
            label: 'Vehículos a control remoto',
            link: 'https://www.falabella.com/falabella-cl/category/CATG11837/Autos-y-vehiculos-a-control-remoto',
            meatStickerOption: 0
          }
        ],
        verTodoActive: true
      },
      {
        label: 'Juguetería por edad',
        link: 'https://www.falabella.com/falabella-cl/category/cat14680031/Juguetes',
        leafCategories: [
          {
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat14680031/Juguetes',
            isHighlightLink: true
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat14680031/Juguetes?facetSelected=true&f.product.attribute.Edad_m%C3%ADnima_recomendada=3+meses%3A%3A6+meses%3A%3A9+meses%3A%3AReci%C3%A9n+nacido',
            meatStickerOption: 0,
            label: '0 - 1 año'
          },
          {
            meatStickerOption: 0,
            label: '1 - 2 años',
            link: 'https://www.falabella.com/falabella-cl/category/cat14680031/Juguetes?f.product.attribute.Edad_m%C3%ADnima_recomendada=1+a%C3%B1o%3A%3A12+meses%3A%3A2+a%C3%B1os',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat14680031/Juguetes?f.product.attribute.Edad_m%C3%ADnima_recomendada=3+a%C3%B1os%3A%3A4+a%C3%B1os%3A%3A5+a%C3%B1os&isPLP=1%2C1%2C1%2C1&isPLP=1',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: '3 - 5 años'
          },
          {
            label: '6 - 8 años',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat14680031/Juguetes?f.product.attribute.Edad_m%C3%ADnima_recomendada=6+a%C3%B1os%3A%3A7+a%C3%B1os%3A%3A8+a%C3%B1os&isPLP=1%2C1%2C1%2C1%2C1&isPLP=1 '
          },
          {
            label: '9 años o más',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat14680031/Juguetes?f.product.attribute.Edad_m%C3%ADnima_recomendada=10+a%C3%B1os%3A%3A11+a%C3%B1os%3A%3A12+a%C3%B1os%3A%3A13+a%C3%B1os%3A%3A14+a%C3%B1os%3A%3A15+a%C3%B1os%3A%3A9+a%C3%B1os&isPLP=1%2C1%2C1%2C1%2C1%2C1%2C1%2C1&isPLP=1'
          }
        ],
        verTodoActive: true
      },
      {
        leafCategories: [
          {
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat690277/Juegos-de-Exterior-e-Interior',
            label: 'Ver todo'
          },
          {
            label: 'Alfombras de juego',
            link: 'https://www.falabella.com/falabella-cl/category/cat10850007/Alfombras-de-juegos',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Autos a batería',
            link: 'https://www.falabella.com/falabella-cl/category/cat90084/Autos-a-bateria'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat90024/Camas-elasticas',
            meatStickerOption: 0,
            label: 'Camas elásticas'
          },
          {
            label: 'Casas de juego y carpas',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat12270018/Casas-de-juego-y-carpas',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            label: 'Go Karts',
            link: 'https://www.falabella.com/falabella-cl/category/cat1100004/Go-kart',
            isHighlightLink: false
          },
          {
            label: 'Inflables',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat6930380/Inflables',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/CATG10509/Inflables-y-juegos-para-piscina',
            label: 'Inflables para piscina '
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat6930454/Ping-pong,-Taca-Taca-y-Pool',
            label: 'Ping pong, taca taca y pool'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat7280050/Piscinas--spa-e-inflables',
            label: 'Piscinas'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat7280035/Scooter--Skate-y-Patines',
            label: 'Scooter, skate y patines'
          },
          {
            isHighlightLink: false,
            label: 'Triciclos y correpasillos',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3078/Triciclos-y-Correpasillos'
          }
        ],
        label: 'Juegos de exterior',
        verTodoActive: true,
        link: 'https://www.falabella.com/falabella-cl/category/cat690277/Juegos-de-Exterior-e-Interior'
      }
    ],
    label: 'Niños y juguetería',
    bannerLocation: 'urlToImage',
    link: 'cat7510001'
  },
  {
    label: 'Bebés',
    subCategories: [
      {
        label: 'Ropa bebé niña por edad',
        leafCategories: [
          {
            isHighlightLink: false,
            label: '0-6 meses',
            link: 'https://www.falabella.com/falabella-cl/category/cat7280122/Ropa-de-bebe?facetSelected=true&f.product.attribute.G%C3%A9nero=Beb%C3%A9+ni%C3%B1a%3A%3ANi%C3%B1a%3A%3AUnisex&f.variant.attribute.size=0+meses%3A%3A3+meses%3A%3A6+meses%3A%3Aprematuro%3A%3Atu',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: '6-12 meses',
            link: 'https://www.falabella.com/falabella-cl/category/cat7280122/Ropa-de-bebe?facetSelected=true&f.product.attribute.G%C3%A9nero=Beb%C3%A9+ni%C3%B1a%3A%3ANi%C3%B1a%3A%3AUnisex&f.variant.attribute.size=12+meses%3A%3A6+meses%3A%3A9+meses%3A%3Atu'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat7280122/Ropa-de-bebe?facetSelected=true&f.product.attribute.G%C3%A9nero=Beb%C3%A9+ni%C3%B1a%3A%3ANi%C3%B1a%3A%3AUnisex&f.variant.attribute.size=12+meses%3A%3A18+meses%3A%3A24+meses%3A%3A36+meses',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: '12-24 meses'
          }
        ],
        verTodoActive: true,
        link: 'https://www.falabella.com/falabella-cl/category/cat7280122/Ropa-de-bebe?f.product.attribute.Género=Niña%3A%3AUnisex'
      },
      {
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat13680004/Vestuario-Bebe-Nina',
            isHighlightLink: true,
            label: 'Ver todo'
          },
          {
            isHighlightLink: false,
            label: 'Bodies y piluchos',
            link: 'https://www.falabella.com/falabella-cl/category/cat15330009/Bodies-Ninas-Bebe',
            meatStickerOption: 0
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat15330007/Buzos-y-Conjuntos-Ninas-Bebe',
            label: 'Buzos y sets',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat15330010/Ropa-Interior-Ninas-Bebe',
            isHighlightLink: false,
            label: 'Calcetines, baberos y gorros'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat15330005/Jeans-y-Pantalones-Ninas-Bebe',
            meatStickerOption: 0,
            label: 'Calzas, jeans y pantalones'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat15330030/Chaquetas,-Parkas-y-Abrigos-Ninas-Bebe',
            label: 'Chaquetas y parkas',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Ositos y enteritos',
            link: 'https://www.falabella.com/falabella-cl/category/cat15330006/Ositos-y-Enteritos-Ninas-Bebe'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Patitas y pantys',
            link: 'https://www.falabella.com/falabella-cl/category/cat13680004/Ropa-de-bebe-nina?f.product.attribute.Tipo=Pantys&isPLP=1%2C1'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Pijamas',
            link: 'https://www.falabella.com/falabella-cl/category/cat15330012/Pijamas-Ninas-Bebe'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat15330001/Poleras-y-Blusas-Ninas-Bebe',
            isHighlightLink: false,
            label: 'Poleras, blusas y camisetas',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            label: 'Polerones y chalecos',
            link: 'https://www.falabella.com/falabella-cl/category/cat15330011/Sweaters-y-Polerones-Ninas-Bebe',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat15330004/Trajes-de-Banos-y-Bikinis-Ninas-Bebe?&f.product.attribute.Género=Niña%3A%3AUnisex',
            meatStickerOption: 0,
            label: 'Trajes de baño'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Vestidos y jardineras',
            link: 'https://www.falabella.com/falabella-cl/category/cat15330003/Faldas-y-Vestidos-Ninas-Bebe'
          }
        ],
        verTodoActive: true,
        link: 'https://www.falabella.com/falabella-cl/category/cat13680004/Vestuario-Bebe-Nina',
        label: 'Ropa bebé niña por tipo'
      },
      {
        verTodoActive: true,
        leafCategories: [
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat7280122/Ropa-de-bebe?facetSelected=true&f.product.attribute.G%C3%A9nero=Beb%C3%A9+ni%C3%B1o%3A%3ANi%C3%B1o%3A%3AUnisex&f.variant.attribute.size=0+meses%3A%3A3+meses%3A%3A6+meses%3A%3Aprematuro%3A%3Atu',
            label: '0-6 meses',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat7280122/Ropa-de-bebe?facetSelected=true&f.product.attribute.G%C3%A9nero=Beb%C3%A9+ni%C3%B1o%3A%3ANi%C3%B1o%3A%3AUnisex&f.variant.attribute.size=12+meses%3A%3A6+meses%3A%3A9+meses%3A%3Atu',
            label: '6-12 meses',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            label: '12-24 meses',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat7280122/Ropa-de-bebe?facetSelected=true&f.product.attribute.G%C3%A9nero=Beb%C3%A9+ni%C3%B1o%3A%3ANi%C3%B1o%3A%3AUnisex&f.variant.attribute.size=12+meses%3A%3A18+meses%3A%3A24+meses%3A%3A36+meses',
            isHighlightLink: false
          }
        ],
        label: 'Ropa bebé niño por edad',
        link: 'https://www.falabella.com/falabella-cl/category/cat7280122/Ropa-de-bebe?&f.product.attribute.Género=Niño%3A%3AUnisex'
      },
      {
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat14730004/Vestuario-Bebe-Nino',
            isHighlightLink: true,
            label: 'Ver todo'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat15330025/Bodies-Ninos-Bebe',
            isHighlightLink: false,
            label: 'Bodies y piluchos'
          },
          {
            isHighlightLink: false,
            label: 'Buzos y sets',
            link: 'https://www.falabella.com/falabella-cl/category/cat15330022/Buzos-y-Conjuntos-Ninos-Bebe'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Calcetines, baberos y gorros',
            link: 'https://www.falabella.com/falabella-cl/category/cat15330026/Ropa-Interior-Ninos-Bebe'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat15330017/Chaquetas,-Parkas-y-Abrigos-Ninos-Bebe',
            label: 'Chaquetas y parkas'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat15330021/Jeans-y-Pantalones-Ninos-Bebe',
            label: 'Jeans y pantalones'
          },
          {
            isHighlightLink: false,
            label: 'Ositos y enteritos',
            link: 'https://www.falabella.com/falabella-cl/category/cat15330023/Ositos-y-Enteritos-Ninos-Bebe'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat15330029/Pijamas-Ninos-Bebe',
            isHighlightLink: false,
            label: 'Pijamas'
          },
          {
            label: 'Poleras, camisas y camisetas',
            link: 'https://www.falabella.com/falabella-cl/category/cat15330018/Poleras-y-Camisas-Ninos-Bebe',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat15330027/Sweaters-y-Polerones-Ninos-Bebe',
            label: 'Polerones y chalecos'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat15330019/Shorts-Ninos-Bebe',
            isHighlightLink: false,
            label: 'Shorts y jardineras'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Trajes de baño',
            link: 'https://www.falabella.com/falabella-cl/category/cat15330020/Trajes-de-Bano-Ninos-Bebe'
          }
        ],
        label: 'Ropa bebé niño por tipo',
        link: 'https://www.falabella.com/falabella-cl/category/cat14730004/Vestuario-Bebe-Nino'
      },
      {
        leafCategories: [
          {
            label: 'Ver todo',
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat690191/Coches-y-Sillas'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat10400099/Accesorios-Coches',
            isHighlightLink: false,
            label: 'Accesorios',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3064/Coches',
            label: 'Coches',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            label: 'Sillas de auto',
            link: 'https://www.falabella.com/falabella-cl/category/cat3228/Sillas-de-Auto',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Sillas nido',
            link: 'https://www.falabella.com/falabella-cl/category/cat3233/Silllas-nido'
          }
        ],
        label: 'Coches y sillas',
        link: 'https://www.falabella.com/falabella-cl/category/cat690191/Coches-y-Sillas',
        verTodoActive: true
      },
      {
        verTodoActive: true,
        link: 'https://www.falabella.com/falabella-cl/category/cat4010/Lactancia-y-alimentacion',
        label: 'Lactancia/alimentación',
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat4010/Lactancia-y-alimentacion',
            isHighlightLink: true,
            label: 'Ver todo'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat9370010/Accesorios-de-Lactancia',
            label: 'Accesorios',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            label: 'Baberos',
            link: 'https://www.falabella.com/falabella-cl/category/cat2450294/Baberos',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat2810082/Chupetes',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Chupetes'
          },
          {
            label: 'Extractores',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat2670020/Extractores-de-Leche'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat4010/Lactancia-y-alimentacion?f.product.attribute.Tipo=Bebestibles%3A%3AConservas%3A%3AC%C3%A1psulas%3A%3ADulces',
            meatStickerOption: 0,
            label: 'Leches y colados',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat2670018/Mamaderas',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Mamaderas'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/CATG10729/Menaje-bebe',
            label: 'Platos y contenedores'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3230/Sillas-de-Comer',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Sillas de comer'
          }
        ]
      },
      {
        leafCategories: [
          {
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat4089/Higiene-y-salud',
            label: 'Ver todo'
          },
          {
            meatStickerOption: 0,
            label: 'Pañales y toallas húmedas',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat10540003/Panales-y-toallas-humedas'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat13650010/Termometros',
            isHighlightLink: false,
            label: 'Termómetros',
            meatStickerOption: 0
          }
        ],
        label: 'Higiene y salud',
        verTodoActive: true,
        link: 'https://www.falabella.com/falabella-cl/category/cat4089/Higiene-y-salud'
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat70065/Seguridad-y-paseo',
        verTodoActive: true,
        label: 'Seguridad y paseo',
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat70065/Seguridad-y-paseo',
            isHighlightLink: true,
            label: 'Ver todo'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Accesorios',
            link: 'https://www.falabella.com/falabella-cl/category/cat2670034/Accesorios'
          },
          {
            label: 'Barandas y rejas',
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2670028/Barandas-y-rejas?page=1'
          },
          {
            meatStickerOption: 0,
            label: 'Bolsos maternales',
            link: 'https://www.falabella.com/falabella-cl/category/cat550035/Bolsos-maternales',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat4022/Porta-bebe',
            isHighlightLink: false,
            label: 'Porta bebé'
          }
        ]
      },
      {
        verTodoActive: true,
        link: 'https://www.falabella.com/falabella-cl/category/cat10190017/Entretencion-bebe',
        leafCategories: [
          {
            isHighlightLink: true,
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat10190017/Entretencion-bebe'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Gimnasio y actividad',
            link: 'https://www.falabella.com/falabella-cl/category/cat4051/Gimnasio-bebe'
          },
          {
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3020/Andadores',
            label: 'Andadores',
            isHighlightLink: false
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat4059/Juguetes-bebe',
            label: 'Juguetes'
          }
        ],
        label: 'Juguetes y entretención'
      },
      {
        link: 'https://www.falabella.com/falabella-cl/category/cat3180019/Dormitorio-Infantil',
        verTodoActive: true,
        leafCategories: [
          {
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat3180019/Dormitorio-Infantil',
            label: 'Ver todo Dormitorio Bebe'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Comodas y mudadores',
            link: 'https://www.falabella.com/falabella-cl/category/cat4034/Comodas-y-mudadores'
          },
          {
            isHighlightLink: false,
            label: 'Cunas',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat4038/Cunas'
          },
          {
            isHighlightLink: false,
            label: 'Ropa de cuna',
            link: 'https://www.falabella.com/falabella-cl/category/cat4086/Ropa-de-cuna',
            meatStickerOption: 0
          }
        ],
        label: 'Dormitorio Bebe'
      }
    ],
    link: 'cat13550007'
  },
  {
    label: 'Belleza y salud',
    link: 'cat7660002',
    subCategories: [
      {
        verTodoActive: true,
        leafCategories: [
          {
            isHighlightLink: true,
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat2069/Perfumes'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3112/Perfumes-Hombre',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Hombre'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3160/Perfumes-mujer',
            meatStickerOption: 0,
            label: 'Mujer',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat2069/Perfumes?facetSelected=true&f.product.attribute.Formato=Set',
            label: 'Sets'
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/category/cat2069/Perfumes',
        label: 'Perfumes'
      },
      {
        label: 'Maquillaje',
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat690199/Maquillaje',
            label: 'Ver todo',
            isHighlightLink: true
          },
          {
            label: 'Brochas y accesorios',
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat690203/Brochas-y-accesorios-de-maquillaje'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat4720024/Maquillaje-de-labios',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Labios'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat4720025/Maquillaje-de-ojos',
            meatStickerOption: 0,
            label: 'Ojos y cejas'
          },
          {
            meatStickerOption: 0,
            label: 'Rostro',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat4720026/Maquillaje-de-rostro'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Sets',
            link: 'https://www.falabella.com/falabella-cl/category/cat4720028/Sets-de-Maquillaje'
          }
        ],
        verTodoActive: true,
        link: 'https://www.falabella.com/falabella-cl/category/cat690199/Maquillaje'
      },
      {
        label: 'Manos y pies',
        link: 'https://www.falabella.com/falabella-cl/category/cat7660015/Cuidado-de-manos-y-pies',
        verTodoActive: true,
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat7660015/Cuidado-de-manos-y-pies',
            isHighlightLink: true,
            label: 'Ver todo'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Uñas',
            link: 'https://www.falabella.com/falabella-cl/category/cat4720029/Cuidado-de-unas'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3214/Cuidado-del-Rostro?facetSelected=true&f.product.attribute.Tipo=Tratamientos+para+manos%3A%3ATratamientos+para+pies',
            isHighlightLink: false,
            label: 'Cremas y exfoliantes',
            meatStickerOption: 0
          }
        ]
      },
      {
        label: 'Cuidado de la piel',
        leafCategories: [
          {
            label: 'Ver todo',
            isHighlightLink: true,
            link: 'https://www.falabella.com/falabella-cl/category/cat3214/Cuidado-del-Rostro'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat13060003/Antiedad',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Antiedad y serums'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat90035/Hidratantes-faciales',
            label: 'Hidratantes faciales',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            label: 'Limpiadores faciales',
            link: 'https://www.falabella.com/falabella-cl/category/cat90032/Limpiadores-faciales',
            meatStickerOption: 0,
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Protección solar rostro',
            link: 'https://www.falabella.com/falabella-cl/category/cat13070030/Proteccion-solar-rostro'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Sets de rostro',
            link: 'https://www.falabella.com/falabella-cl/category/cat5870078/Sets-de-Rostro'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat4960009/Cuidado-del-Cuerpo',
            label: 'Cuidado del cuerpo'
          },
          {
            label: 'Tecnología para la belleza',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat2025/Tecnologia-para-la-Belleza',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Tratamientos hombre',
            link: 'https://www.falabella.com/falabella-cl/category/cat90036/Tratamientos-faciales?facetSelected=true&f.product.attribute.G%C3%A9nero=Hombre'
          }
        ],
        link: 'https://www.falabella.com/falabella-cl/category/cat3214/Cuidado-del-Rostro'
      },
      {
        leafCategories: [
          {
            isHighlightLink: true,
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/collection/ver-todo-dermocosmetica'
          },
          {
            label: 'Antiedad',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat13060003/Antiedad'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat13060006/Cuerpo',
            label: 'Cuerpo'
          },
          {
            label: 'Hidratación y limpieza',
            link: 'https://www.falabella.com/falabella-cl/category/cat13060007/Hidratacion-y-Limpieza',
            isHighlightLink: false
          },
          {
            label: 'Protección solar',
            link: 'https://www.falabella.com/falabella-cl/category/cat690213/Bloqueadores-solares',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            label: 'Sets',
            link: 'https://www.falabella.com/falabella-cl/collection/sets-dermocosmetica'
          }
        ],
        label: 'Dermocosmética',
        link: 'https://www.falabella.com/falabella-cl/category/cat13080027/Dermocosmetica'
      },
      {
        leafCategories: [
          {
            isHighlightLink: true,
            label: 'Ver todo',
            link: 'https://www.falabella.com/falabella-cl/category/cat9440008/Cuidado-Capilar'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/CATG11731/Afeitadoras-y-Cortapelos',
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Afeitadoras'
          },
          {
            label: 'Alisadores de pelo',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat3018/Alisadores-de-pelo',
            isHighlightLink: false
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/collection/tratamiento-hombre-y-barberia',
            label: 'Barbería'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Onduladores de pelo',
            link: 'https://www.falabella.com/falabella-cl/category/cat3170/Onduladores-de-pelo'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat3223/Secadores-de-pelo',
            isHighlightLink: false,
            label: 'Secadores de pelo',
            meatStickerOption: 0
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat9440008/Cuidado-Capilar?facetSelected=true&f.product.attribute.Tipo=Set+de+tratamiento+capilar',
            isHighlightLink: false,
            label: 'Sets'
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat90039/Shampoo-y-acondicionador',
            label: 'Shampoo/acondicionador'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Tratamientos y mascarillas',
            link: 'https://www.falabella.com/falabella-cl/category/cat4960011/Tratamientos-capilares'
          }
        ],
        label: 'Cuidado capilar y barbería',
        link: 'https://www.falabella.com/falabella-cl/category/cat9440008/Cuidado-Capilar'
      },
      {
        verTodoActive: true,
        leafCategories: [
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat14090015/Salud',
            isHighlightLink: true,
            label: 'Ver todo'
          },
          {
            label: 'Accesorios médicos y monitores',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/CATG11205/Monitores-de-salud',
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            label: 'Aromaterapia y masajes',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/cat4960026/Aromaterapia-y-spa'
          },
          {
            meatStickerOption: 0,
            label: 'Bienestar sexual',
            link: 'https://www.falabella.com/falabella-cl/category/CATG11207/Bienestar-sexual',
            isHighlightLink: false
          },
          {
            link: 'https://www.falabella.com/falabella-cl/category/cat11868860/Equipos-ortopedicos',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'Eq. médico y ortopédico'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            label: 'Medicina deportiva',
            link: 'https://www.falabella.com/falabella-cl/category/CATG11203/Medicina-deportiva'
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/category/CATG11735/Primeros-auxilios',
            label: 'Primeros auxilios'
          },
          {
            label: 'Vitaminas y suplementos',
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/category/cat11342105/Vitaminas-y-suplementos',
            isHighlightLink: false
          }
        ],
        label: 'Salud y bienestar',
        link: 'https://www.falabella.com/falabella-cl/category/cat14090015/Salud'
      },
      {
        leafCategories: [
          {
            label: 'JO MALONE LONDON',
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=&f.product.brandName=JO%20MALONE%20LONDON',
            isHighlightLink: false,
            meatStickerOption: 0
          },
          {
            meatStickerOption: 0,
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=&f.product.brandName=BENEFIT',
            label: 'Benefit'
          },
          {
            meatStickerOption: 0,
            label: 'CHANEL',
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=&f.product.brandName=CHANEL'
          },
          {
            isHighlightLink: false,
            link: 'https://tienda.falabella.com/falabella-cl/page/it-cosmetics',
            meatStickerOption: 0,
            label: 'IT Cosmetics'
          },
          {
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=&f.product.brandName=la%20mer',
            isHighlightLink: false,
            meatStickerOption: 0,
            label: 'La Mer'
          },
          {
            isHighlightLink: false,
            meatStickerOption: 0,
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=mac',
            label: 'MAC'
          },
          {
            isHighlightLink: false,
            label: 'The Body Shop',
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=body+shop',
            meatStickerOption: 0
          },
          {
            isHighlightLink: false,
            link: 'https://www.falabella.com/falabella-cl/search?Ntt=&f.product.brandName=BATH%20%26%20BODY%20WORKS',
            label: 'Bath&Body Works',
            meatStickerOption: 0
          }
        ],
        link: 'https://tienda.falabella.com/falabella-cl/page/belleza-f',
        verTodoActive: true,
        label: 'Marcas destacadas'
      }
    ]
  }
];

export default taxonomy;

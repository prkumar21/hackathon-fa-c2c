const CookieSchema = {
  sellerUUIDC2c: {
    type: 'string',
    encoded: false
  }
};
const setCookie = ({ name, value, expiryDays = 1000, path = '/' }) => {
  let cookie = value;
  let expiration = '';
  const pathStr = `;path=${path}`;
  const schema = CookieSchema[name];
  if (schema) {
    if (schema.type === 'object') {
      cookie = JSON.stringify(cookie);
    }

    cookie = schema.encoded ? encode(cookie) : cookie;
  }

  if (expiryDays) {
    const date = new Date();
    const DAY = 24 * 60 * 60 * 1000;
    date.setTime(date.getTime() + expiryDays * DAY);
    expiration = `; expires=${date.toGMTString()}`;
  }

  const cookieStr = `${name}=${cookie}${pathStr}${expiration}`;
  if (
    typeof document !== 'undefined' &&
    typeof document.cookie !== 'undefined'
  ) {
    document.cookie = cookieStr;
    return cookieStr;
  }
  return false;
};

const getCookie = (cookieName) => {
  const result = document.cookie.match(
    `(^|;)\\s*${cookieName}\\s*=\\s*([^;]+)`
  );
  return result ? result.pop() : '';
};

export { setCookie, getCookie };

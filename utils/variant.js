const getVariant = (variants, variantId) => {
    return variants.find(({ id }) => id === variantId) || {};
};
  
export default getVariant;
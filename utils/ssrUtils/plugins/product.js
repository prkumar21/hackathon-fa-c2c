import { httpFetchService, API_HOST } from '../../httpService';
import get from 'lodash.get';

const getProduct = async (ctx, pageProps, next) => {
  const { params } = ctx;
  console.log({query: ctx.query });
  const customerId = get(ctx, 'query.customerId', '');
  const { productId, variantId } = params;
  const response = await httpFetchService(
    `${API_HOST}catalog/sku/${variantId}`,
    'GET'
  );
  console.log({url: `${API_HOST}catalog/sku/${variantId}`, response: response});
  const data = response.success ? get(response, 'data', {}) : '';
  // let data = {
  //   bid: {
  //     type: 'Bid',
  //     range: 50,
  //     enabled: true,
  //     endDate: '2021-11-24T12:30:39.834684+00:00',
  //     startDate: '2021-11-17T12:30:39.834388+00:00'
  //   },
  //   name: 'LED 55" 55PUD6794 Ambilight 4K Ultra HD Smart TV',
  //   price: {
  //     basePrice: 10,
  //     actualPrice: 50
  //   },
  //   sku_id: 'sku995635218',
  //   is_used: true,
  //   quantity: 0,
  //   condition: 'used',
  //   medialist: [],
  //   seller_id: 'string',
  //   brand_name: 'nike',
  //   categories: ['string'],
  //   product_id: 'prod123',
  //   description: 'string',
  //   seller_rating: 5
  // };
  pageProps.props.product = data;
  pageProps.props.customerId = customerId;
  return next();
};

export default getProduct;
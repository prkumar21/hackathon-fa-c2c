/* eslint-disable no-param-reassign */
import { API_HOST, httpFetchService } from '../../httpService';
import get from 'lodash.get';
import axios from 'axios';
import _ from 'lodash';

const getProductList = async (ctx, pageProps, next) => {
  const base = process.env.SRVERLESS_BASE;
  const resposne = await httpFetchService(`${API_HOST}catalog/product`, 'GET');
  const data = resposne.success ? get(resposne, 'data.data', {}) : '';
  pageProps.props.products = data;
  return next();
};

const facetMap = {
  used: 'is_used'
};

const search = async (ctx, pageProps, next) => {
  const queries = _.get(ctx, 'query', {});
  let reqBody = {
    query: queries.Ntt
  };
  let filters = [];
  Object.keys(queries).forEach((key) => {
    if (key !== 'Ntt') {
      filters.push({
        key: facetMap[key.toLowerCase()],
        values: [queries[key]]
      });
    }
  });
  if (filters.length) {
    reqBody = {
      ...reqBody,
      filters
    };
  }
  try {
    const response = await axios({
      method: 'post',
      url: `${API_HOST}search`,
      data: reqBody
    });
    if (response.data.status === 'OK') {
      pageProps.props.products = get(response, 'data.data.docs', {});
      pageProps.props.facets = get(response, 'data.data.aggs', {});
      pageProps.props.query = queries.Ntt;
    }
  } catch (error) {
    pageProps.props.products = [];
    pageProps.props.facets = {};
    pageProps.props.query = '';
    console.error(error);
  }

  return next();
};
const sellerProducts = async (ctx, pageProps, next) => {
  try {
    const sellerId = _.get(ctx, 'req.cookies.sellerUUIDC2c', '');
    if (sellerId) {
      const reqBody = {
        filter: [{ key: 'seller_id', value: [sellerId] }]
      };
      const response = await axios({
        method: 'post',
        url: `${API_HOST}search`,
        data: reqBody
      });
      if (response.data.status === 'OK') {
        pageProps.props.products = get(response, 'data.data.docs', {});
        pageProps.props.facets = get(response, 'data.data.aggs', {});
      }
    }
  } catch (error) {
    pageProps.props.products = [];
    pageProps.props.facets = {};
    pageProps.props.query = '';
    pageProps.props.error = 'Something went wrong!';
    console.error(error);
  }
  next();
};
export { search, sellerProducts };

export default getProductList;
